// To parse this JSON data, do
//
//     final cartListModel = cartListModelFromJson(jsonString);

import 'dart:convert';

CartListModel cartListModelFromJson(String str) => CartListModel.fromJson(json.decode(str));

String cartListModelToJson(CartListModel data) => json.encode(data.toJson());

class CartListModel {
  CartListModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  CartListResultData resultData;

  factory CartListModel.fromJson(Map<String, dynamic> json) => CartListModel(
    status: json["status"],
    message: json["message"],
    resultData: CartListResultData.fromJson(json["resultData"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": resultData.toJson(),
  };
}

class CartListResultData {
  CartListResultData({
    this.cartItems,
    this.total,
    this.user,
    this.shipping,
    this.grandTotal,
  });

  List<CartItem> cartItems;
  num total;
  User user;
  num shipping;
  num grandTotal;

  factory CartListResultData.fromJson(Map<String, dynamic> json) => CartListResultData(
    cartItems: List<CartItem>.from(json["cartItems"].map((x) => CartItem.fromJson(x))),
    total: json["total"],
    user: User.fromJson(json["user"]),
    shipping: json["shipping"],
    grandTotal: json["grandTotal"],
  );

  Map<String, dynamic> toJson() => {
    "cartItems": List<dynamic>.from(cartItems.map((x) => x.toJson())),
    "total": total,
    "user": user.toJson(),
    "shipping": shipping,
    "grandTotal": grandTotal,
  };
}

class CartItem {
  CartItem({
    this.id,
    this.product,
    this.user,
    this.v,
    this.count,
    this.createdAt,
    this.updatedAt,
    this.total,
  });

  String id;
  Product product;
  String user;
  num v;
  num count;
  DateTime createdAt;
  DateTime updatedAt;
  num total;

  factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
    id: json["_id"],
    product: Product.fromJson(json["product"]),
    user: json["user"],
    v: json["__v"],
    count: json["count"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "product": product.toJson(),
    "user": user,
    "__v": v,
    "count": count,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "total": total,
  };
}

class Product {
  Product({
    this.deliverable,
    this.id,
    this.name,
    this.userId,
    this.price,
    this.modelId,
    this.categoryId,
    this.subCategoryId,
    this.images,
    this.thumbnail,
    this.description,
    this.quantity,
    this.offerPrice,
    this.radius,
    this.isActive,
    this.sold,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  Deliverable deliverable;
  String id;
  String name;
  String userId;
  num price;
  List<String> modelId;
  String categoryId;
  String subCategoryId;
  List<String> images;
  List<String> thumbnail;
  String description;
  num quantity;
  num offerPrice;
  num radius;
  bool isActive;
  num sold;
  num status;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    deliverable: Deliverable.fromJson(json["deliverable"]),
    id: json["_id"],
    name: json["name"],
    userId: json["user_id"],
    price: json["price"],
    modelId: List<String>.from(json["model_id"].map((x) => x)),
    categoryId: json["category_id"],
    subCategoryId: json["sub_category_id"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnail: List<String>.from(json["thumbnail"].map((x) => x)),
    description: json["description"],
    quantity: json["quantity"],
    offerPrice: json["offerPrice"],
    radius: json["radius"],
    isActive: json["isActive"],
    sold: json["sold"],
    status: json["status"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "deliverable": deliverable.toJson(),
    "_id": id,
    "name": name,
    "user_id": userId,
    "price": price,
    "model_id": List<dynamic>.from(modelId.map((x) => x)),
    "category_id": categoryId,
    "sub_category_id": subCategoryId,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnail": List<dynamic>.from(thumbnail.map((x) => x)),
    "description": description,
    "quantity": quantity,
    "offerPrice": offerPrice,
    "radius": radius,
    "isActive": isActive,
    "sold": sold,
    "status": status,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}

class Deliverable {
  Deliverable({
    this.coordinates,
  });

  List<List<List<double>>> coordinates;

  factory Deliverable.fromJson(Map<String, dynamic> json) => Deliverable(
    coordinates: List<List<List<double>>>.from(json["coordinates"].map((x) => List<List<double>>.from(x.map((x) => List<double>.from(x.map((x) => x.toDouble())))))),
  );

  Map<String, dynamic> toJson() => {
    "coordinates": List<dynamic>.from(coordinates.map((x) => List<dynamic>.from(x.map((x) => List<dynamic>.from(x.map((x) => x)))))),
  };
}

class User {
  User({
    this.id,
    this.role,
    this.phone,
  });

  String id;
  List<Role> role;
  String phone;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    role: List<Role>.from(json["role"].map((x) => Role.fromJson(x))),
    phone: json["phone"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role": List<dynamic>.from(role.map((x) => x.toJson())),
    "phone": phone,
  };
}

class Role {
  Role({
    this.id,
    this.name,
  });

  String id;
  String name;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
    id: json["_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
  };
}
