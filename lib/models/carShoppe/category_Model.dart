// To parse this JSON data, do
//
//     final categoryModel = categoryModelFromJson(jsonString);

import 'dart:convert';

CategoryModel categoryModelFromJson(String str) => CategoryModel.fromJson(json.decode(str));

String categoryModelToJson(CategoryModel data) => json.encode(data.toJson());

class CategoryModel {
  CategoryModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<CategoryResultData> resultData;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
    status: json["status"],
    message: json["message"],
    resultData: List<CategoryResultData>.from(json["resultData"].map((x) => CategoryResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class CategoryResultData {
  CategoryResultData({
    this.id,
    this.name,
    this.images,
    this.thumbnail,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  List<String> images;
  List<String> thumbnail;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory CategoryResultData.fromJson(Map<String, dynamic> json) => CategoryResultData(
    id: json["_id"],
    name: json["name"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnail: List<String>.from(json["thumbnail"].map((x) => x)),
    isActive: json["isActive"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnail": List<dynamic>.from(thumbnail.map((x) => x)),
    "isActive": isActive,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}
