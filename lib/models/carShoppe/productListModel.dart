// To parse this JSON data, do
//
//     final productListModel = productListModelFromJson(jsonString);

import 'dart:convert';

ProductListModel productListModelFromJson(String str) => ProductListModel.fromJson(json.decode(str));

String productListModelToJson(ProductListModel data) => json.encode(data.toJson());

class ProductListModel {
  ProductListModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<ProductListResultData> resultData;

  factory ProductListModel.fromJson(Map<String, dynamic> json) => ProductListModel(
    status: json["status"],
    message: json["message"],
    resultData: List<ProductListResultData>.from(json["resultData"].map((x) => ProductListResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class ProductListResultData {
  ProductListResultData({
    this.deliverable,
    this.id,
    this.name,
    this.userId,
    this.price,
    this.modelId,
    this.categoryId,
    this.subCategoryId,
    this.images,
    this.thumbnail,
    this.description,
    this.quantity,
    this.offerPrice,
    this.radius,
    this.isActive,
    this.sold,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  Deliverable deliverable;
  String id;
  String name;
  String userId;
  num price;
  List<String> modelId;
  String categoryId;
  String subCategoryId;
  List<String> images;
  List<String> thumbnail;
  String description;
  num quantity;
  num offerPrice;
  num radius;
  bool isActive;
  num sold;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory ProductListResultData.fromJson(Map<String, dynamic> json) => ProductListResultData(
    deliverable: Deliverable.fromJson(json["deliverable"]),
    id: json["_id"],
    name: json["name"],
    userId: json["user_id"],
    price: json["price"],
    modelId: List<String>.from(json["model_id"].map((x) => x)),
    categoryId: json["category_id"],
    subCategoryId: json["sub_category_id"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnail: List<String>.from(json["thumbnail"].map((x) => x)),
    description: json["description"],
    quantity: json["quantity"],
    offerPrice: json["offerPrice"],
    radius: json["radius"],
    isActive: json["isActive"],
    sold: json["sold"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "deliverable": deliverable.toJson(),
    "_id": id,
    "name": name,
    "user_id": userId,
    "price": price,
    "model_id": List<dynamic>.from(modelId.map((x) => x)),
    "category_id": categoryId,
    "sub_category_id": subCategoryId,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnail": List<dynamic>.from(thumbnail.map((x) => x)),
    "description": description,
    "quantity": quantity,
    "offerPrice": offerPrice,
    "radius": radius,
    "isActive": isActive,
    "sold": sold,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}

class Deliverable {
  Deliverable({
    this.coordinates,
  });

  List<List<List<double>>> coordinates;

  factory Deliverable.fromJson(Map<String, dynamic> json) => Deliverable(
    coordinates: List<List<List<double>>>.from(json["coordinates"].map((x) => List<List<double>>.from(x.map((x) => List<double>.from(x.map((x) => x.toDouble())))))),
  );

  Map<String, dynamic> toJson() => {
    "coordinates": List<dynamic>.from(coordinates.map((x) => List<dynamic>.from(x.map((x) => List<dynamic>.from(x.map((x) => x)))))),
  };
}
