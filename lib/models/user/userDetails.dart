// To parse this JSON data, do
//
//     final userDetails = userDetailsFromJson(jsonString);

import 'dart:convert';

UserDetails userDetailsFromJson(String str) => UserDetails.fromJson(json.decode(str));

String userDetailsToJson(UserDetails data) => json.encode(data.toJson());

class UserDetails {
  UserDetails({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  ResultData resultData;

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
    status: json["status"],
    message: json["message"],
    resultData: ResultData.fromJson(json["resultData"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": resultData.toJson(),
  };
}

class ResultData {
  ResultData({
    this.id,
    this.phone,
    this.v,
    this.createdAt,
    this.isActive,
    this.isTfaEnabled,
    this.loginAttempt,
    this.loginDate,
    this.otpCode,
    this.otpCreatedDate,
    this.provides,
    this.role,
    this.rpToken,
    this.rpTokenCreatedDate,
    this.tfaOtpAuthUrl,
    this.tfaSecretBase32,
    this.tfaSecretHex,
    this.updatedAt,
    this.userToken,
    this.carType,
    this.modelId,
    this.email,
    this.name,
  });

  String id;
  String phone;
  num v;
  DateTime createdAt;
  bool isActive;
  bool isTfaEnabled;
  num loginAttempt;
  DateTime loginDate;
  String otpCode;
  DateTime otpCreatedDate;
  List<dynamic> provides;
  List<Role> role;
  dynamic rpToken;
  dynamic rpTokenCreatedDate;
  dynamic tfaOtpAuthUrl;
  dynamic tfaSecretBase32;
  dynamic tfaSecretHex;
  DateTime updatedAt;
  String userToken;
  String carType;
  String modelId;
  String email;
  String name;

  factory ResultData.fromJson(Map<String, dynamic> json) => ResultData(
    id: json["_id"],
    phone: json["phone"],
    v: json["__v"],
    createdAt: DateTime.parse(json["createdAt"]),
    isActive: json["isActive"],
    isTfaEnabled: json["isTFAEnabled"],
    loginAttempt: json["loginAttempt"],
    loginDate: DateTime.parse(json["loginDate"]),
    otpCode: json["otpCode"],
    otpCreatedDate: DateTime.parse(json["otpCreatedDate"]),
    provides: List<dynamic>.from(json["provides"].map((x) => x)),
    role: List<Role>.from(json["role"].map((x) => Role.fromJson(x))),
    rpToken: json["rpToken"],
    rpTokenCreatedDate: json["rpTokenCreatedDate"],
    tfaOtpAuthUrl: json["tfaOTPAuthUrl"],
    tfaSecretBase32: json["tfaSecretBase32"],
    tfaSecretHex: json["tfaSecretHex"],
    updatedAt: DateTime.parse(json["updatedAt"]),
    userToken: json["userToken"],
    carType: json["carType"],
    modelId: json["model_id"],
    email: json["email"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "phone": phone,
    "__v": v,
    "createdAt": createdAt.toIso8601String(),
    "isActive": isActive,
    "isTFAEnabled": isTfaEnabled,
    "loginAttempt": loginAttempt,
    "loginDate": loginDate.toIso8601String(),
    "otpCode": otpCode,
    "otpCreatedDate": otpCreatedDate.toIso8601String(),
    "provides": List<dynamic>.from(provides.map((x) => x)),
    "role": List<dynamic>.from(role.map((x) => x.toJson())),
    "rpToken": rpToken,
    "rpTokenCreatedDate": rpTokenCreatedDate,
    "tfaOTPAuthUrl": tfaOtpAuthUrl,
    "tfaSecretBase32": tfaSecretBase32,
    "tfaSecretHex": tfaSecretHex,
    "updatedAt": updatedAt.toIso8601String(),
    "userToken": userToken,
    "carType": carType,
    "model_id": modelId,
    "email": email,
    "name": name,
  };
}

class Role {
  Role({
    this.id,
    this.name,
  });

  String id;
  String name;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
    id: json["_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
  };
}
