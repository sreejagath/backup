// // To parse this JSON data, do
// //
// //     final addressListModel = addressListModelFromJson(jsonString);

// import 'dart:convert';

// AddressListModel addressListModelFromJson(String str) => AddressListModel.fromJson(json.decode(str));

// String addressListModelToJson(AddressListModel data) => json.encode(data.toJson());

// class AddressListModel {
//   AddressListModel({
//     this.status,
//     this.message,
//     this.resultData,
//   });

//   String status;
//   String message;
//   List<AddressListResultData> resultData;

//   factory AddressListModel.fromJson(Map<String, dynamic> json) => AddressListModel(
//     status: json["status"],
//     message: json["message"],
//     resultData: List<AddressListResultData>.from(json["resultData"].map((x) => AddressListResultData.fromJson(x))),
//   );

//   Map<String, dynamic> toJson() => {
//     "status": status,
//     "message": message,
//     "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
//   };
// }

// class AddressListResultData {
//   AddressListResultData({
//     this.id,
//     this.name,
//     this.mobile,
//     this.house,
//     this.street,
//     this.city,
//     this.state,
//     this.pincode,
//     this.landmark,
//     this.altPhone,
//     this.type,
//     //this.location,
//     this.user,
//     this.isDefault,
//     this.createdAt,
//     this.updatedAt,
//     this.v,
//   });

//   String id;
//   String name;
//   num mobile;
//   String house;
//   String street;
//   String city;
//   String state;
//   num pincode;
//   String landmark;
//   num altPhone;
//   String type;
//   //List<double> location;
//   String user;
//   bool isDefault;
//   DateTime createdAt;
//   DateTime updatedAt;
//   num v;

//   factory AddressListResultData.fromJson(Map<String, dynamic> json) => AddressListResultData(
//     id: json["_id"],
//     name: json["name"],
//     mobile: json["mobile"],
//     house: json["house"],
//     street: json["street"],
//     city: json["city"],
//     state: json["state"],
//     pincode: json["pincode"],
//     landmark: json["landmark"],
//     altPhone: json["altPhone"],
//     type: json["type"],
//     //location: List<double>.from(json["location"].map((x) => x.toDouble())),
//     user: json["user"],
//     isDefault: json["isDefault"],
//     createdAt: DateTime.parse(json["createdAt"]),
//     updatedAt: DateTime.parse(json["updatedAt"]),
//     v: json["__v"],
//   );

//   Map<String, dynamic> toJson() => {
//     "_id": id,
//     "name": name,
//     "mobile": mobile,
//     "house": house,
//     "street": street,
//     "city": city,
//     "state": state,
//     "pincode": pincode,
//     "landmark": landmark,
//     "altPhone": altPhone,
//     "type": type,
//     //"location": List<dynamic>.from(location.map((x) => x)),
//     "user": user,
//     "isDefault": isDefault,
//     "createdAt": createdAt.toIso8601String(),
//     "updatedAt": updatedAt.toIso8601String(),
//     "__v": v,
//   };
// }

// To parse this JSON data, do
//
//     final addressListModel = addressListModelFromJson(jsonString);

import 'dart:convert';

AddressListModel addressListModelFromJson(String str) =>
    AddressListModel.fromJson(json.decode(str));

String addressListModelToJson(AddressListModel data) =>
    json.encode(data.toJson());

class AddressListModel {
  AddressListModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<AddressListResultData> resultData;

  factory AddressListModel.fromJson(Map<String, dynamic> json) =>
      AddressListModel(
        status: json["status"],
        message: json["message"],
        resultData: List<AddressListResultData>.from(
            json["resultData"].map((x) => AddressListResultData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
      };
}

class AddressListResultData {
  AddressListResultData({
    this.id,
    this.name,
    this.mobile,
    this.place,
    this.house,
    this.street,
    this.city,
    this.state,
    this.pincode,
    this.landmark,
    this.altPhone,
    this.type,
    this.location,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.isDefault,
  });

  String id;
  String name;
  int mobile;
  String place;
  String house;
  String street;
  String city;
  String state;
  int pincode;
  String landmark;
  num altPhone;
  String type;
  List<double> location;
  dynamic user;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  bool isDefault;

  factory AddressListResultData.fromJson(Map<String, dynamic> json) =>
      AddressListResultData(
        id: json["_id"],
        name: json["name"],
        mobile: json["mobile"],
        place: json["place"],
        house: json["house"],
        street: json["street"],
        city: json["city"],
        state: json["state"],
        pincode: json["pincode"],
        landmark: json["landmark"],
        altPhone: json["altPhone"],
        type: json["type"],
        location: List<double>.from(json["location"].map((x) => x.toDouble())),
        user: json["user"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        isDefault: json["isDefault"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "mobile": mobile,
        "place": place,
        "house": house,
        "street": street,
        "city": city,
        "state": state,
        "pincode": pincode,
        "landmark": landmark,
        "altPhone": altPhone,
        "type": type,
        "location": List<dynamic>.from(location.map((x) => x)),
        "user": user,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "isDefault": isDefault,
      };
}
