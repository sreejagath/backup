// To parse this JSON data, do
//
//     final carSpaServiceModel = carSpaServiceModelFromJson(jsonString);

import 'dart:convert';

CarSpaServiceModel carSpaServiceModelFromJson(String str) => CarSpaServiceModel.fromJson(json.decode(str));

String carSpaServiceModelToJson(CarSpaServiceModel data) => json.encode(data.toJson());

class CarSpaServiceModel {
  CarSpaServiceModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<CarSpaServiceResultData> resultData;

  factory CarSpaServiceModel.fromJson(Map<String, dynamic> json) => CarSpaServiceModel(
    status: json["status"],
    message: json["message"],
    resultData: List<CarSpaServiceResultData>.from(json["resultData"].map((x) => CarSpaServiceResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class CarSpaServiceResultData {
  CarSpaServiceResultData({
    this.id,
    this.name,
    this.categoryId,
    this.carType,
    this.images,
    this.thumbnails,
    this.isActive,
    this.price,
    this.description,
    this.list,
    this.addOns,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  String categoryId;
  String carType;
  List<String> images;
  List<String> thumbnails;
  bool isActive;
  num price;
  String description;
  String list;
  List<CarSpaAddOn> addOns;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory CarSpaServiceResultData.fromJson(Map<String, dynamic> json) => CarSpaServiceResultData(
    id: json["_id"],
    name: json["name"],
    categoryId: json["categoryId"],
    carType: json["carType"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnails: List<String>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    price: json["price"],
    description: json["description"],
    list: json["list"],
    addOns: List<CarSpaAddOn>.from(json["addOns"].map((x) => CarSpaAddOn.fromJson(x))),
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "categoryId": categoryId,
    "carType": carType,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "price": price,
    "description": description,
    "list": list,
    "addOns": List<dynamic>.from(addOns.map((x) => x.toJson())),
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}

class CarSpaAddOn {
  CarSpaAddOn({
    this.name,
    this.price,
    this.id,
  });

  String name;
  num price;
  String id;

  factory CarSpaAddOn.fromJson(Map<String, dynamic> json) => CarSpaAddOn(
    name: json["name"],
    price: json["price"],
    id: json["_id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "price": price,
    "_id": id,
  };
}
