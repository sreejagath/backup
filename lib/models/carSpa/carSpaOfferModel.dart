// To parse this JSON data, do
//
//     final carSpaOfferModel = carSpaOfferModelFromJson(jsonString);

import 'dart:convert';

CarSpaOfferModel carSpaOfferModelFromJson(String str) =>
    CarSpaOfferModel.fromJson(json.decode(str));

String carSpaOfferModelToJson(CarSpaOfferModel data) =>
    json.encode(data.toJson());

class CarSpaOfferModel {
  CarSpaOfferModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<CarSpaOfferModelData> resultData;

  factory CarSpaOfferModel.fromJson(Map<String, dynamic> json) =>
      CarSpaOfferModel(
        status: json["status"],
        message: json["message"],
        resultData: List<CarSpaOfferModelData>.from(
            json["resultData"].map((x) => CarSpaOfferModelData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
      };
}

class CarSpaOfferModelData {
  CarSpaOfferModelData({
    this.id,
    this.title,
    this.description,
    this.serviceId,
    this.maxAmount,
    this.minAmount,
    this.offerType,
    this.offerAmount,
    this.status,
    this.v,
  });

  String id;
  String title;
  String description;
  List<String> serviceId;
  num maxAmount;
  num minAmount;
  String offerType;
  num offerAmount;
  String status;
  num v;

  factory CarSpaOfferModelData.fromJson(Map<String, dynamic> json) =>
      CarSpaOfferModelData(
        id: json["_id"],
        title: json["title"],
        description: json["description"],
        serviceId: List<String>.from(json["serviceId"].map((x) => x)),
        maxAmount: json["maxAmount"],
        minAmount: json["minAmount"],
        offerType: json["offerType"],
        offerAmount: json["offerAmount"],
        status: json["status"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "title": title,
        "description": description,
        "serviceId": List<dynamic>.from(serviceId.map((x) => x)),
        "maxAmount": maxAmount,
        "minAmount": minAmount,
        "offerType": offerType,
        "offerAmount": offerAmount,
        "status": status,
        "__v": v,
      };
}
