// To parse this JSON data, do
//
//     final carSpaOrderModel = carSpaOrderModelFromJson(jsonString);

import 'dart:convert';

CarSpaOrderModel carSpaOrderModelFromJson(String str) => CarSpaOrderModel.fromJson(json.decode(str));

String carSpaOrderModelToJson(CarSpaOrderModel data) => json.encode(data.toJson());

class CarSpaOrderModel {
  CarSpaOrderModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<CarSpaOrderResultData> resultData;

  factory CarSpaOrderModel.fromJson(Map<String, dynamic> json) => CarSpaOrderModel(
    status: json["status"],
    message: json["message"],
    resultData: List<CarSpaOrderResultData>.from(json["resultData"].map((x) => CarSpaOrderResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class CarSpaOrderResultData {
  CarSpaOrderResultData({
    this.id,
    this.orderId,
    this.customerId,
    this.franchiseId,
    this.serviceId,
    this.timeSlot,
    this.price,
    this.mode,
    this.status,
    this.address,
    this.addOn,
    this.date,
    this.discountAmount,
    this.grandTotal,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String orderId;
  String customerId;
  String franchiseId;
  ServiceId serviceId;
  String timeSlot;
  num price;
  String mode;
  String status;
  Address address;
  List<AddOn> addOn;
  DateTime date;
  num discountAmount;
  num grandTotal;
  DateTime createdAt;
  DateTime updatedAt;
  num v;

  factory CarSpaOrderResultData.fromJson(Map<String, dynamic> json) => CarSpaOrderResultData(
    id: json["_id"],
    orderId: json["orderId"],
    customerId: json["customerId"],
    franchiseId: json["franchiseId"],
    serviceId: ServiceId.fromJson(json["serviceId"]),
    timeSlot: json["timeSlot"],
    price: json["price"],
    mode: json["mode"],
    status: json["status"],
    address: Address.fromJson(json["address"]),
    addOn: List<AddOn>.from(json["addOn"].map((x) => AddOn.fromJson(x))),
    date: DateTime.parse(json["date"]),
    discountAmount: json["discountAmount"],
    grandTotal: json["grandTotal"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "orderId": orderId,
    "customerId": customerId,
    "franchiseId": franchiseId,
    "serviceId": serviceId.toJson(),
    "timeSlot": timeSlot,
    "price": price,
    "mode": mode,
    "status": status,
    "address": address.toJson(),
    "addOn": List<dynamic>.from(addOn.map((x) => x.toJson())),
    "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    "discountAmount": discountAmount,
    "grandTotal": grandTotal,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}

class AddOn {
  AddOn({
    this.name,
    this.price,
    this.id,
  });

  String name;
  num price;
  String id;

  factory AddOn.fromJson(Map<String, dynamic> json) => AddOn(
    name: json["name"],
    price: json["price"],
    id: json["_id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "price": price,
    "_id": id,
  };
}

class Address {
  Address({
    this.id,
    this.name,
    this.mobile,
    this.house,
    this.street,
    this.city,
    this.state,
    this.pincode,
    this.landmark,
    this.type,
    this.location,
    this.user,
    this.isDefault,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String name;
  num mobile;
  String house;
  String street;
  String city;
  String state;
  num pincode;
  String landmark;
  String type;
  List<double> location;
  String user;
  bool isDefault;
  DateTime createdAt;
  DateTime updatedAt;
  num v;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    id: json["_id"],
    name: json["name"],
    mobile: json["mobile"],
    house: json["house"],
    street: json["street"],
    city: json["city"],
    state: json["state"],
    pincode: json["pincode"],
    landmark: json["landmark"],
    type: json["type"],
    location: List<double>.from(json["location"].map((x) => x.toDouble())),
    user: json["user"],
    isDefault: json["isDefault"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "mobile": mobile,
    "house": house,
    "street": street,
    "city": city,
    "state": state,
    "pincode": pincode,
    "landmark": landmark,
    "type": type,
    "location": List<dynamic>.from(location.map((x) => x)),
    "user": user,
    "isDefault": isDefault,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}

class ServiceId {
  ServiceId({
    this.id,
    this.name,
    this.categoryId,
    this.carType,
    this.images,
    this.thumbnails,
    this.isActive,
    this.price,
    this.addOns,
    this.description,
    this.list,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  String categoryId;
  String carType;
  List<String> images;
  List<String> thumbnails;
  bool isActive;
  num price;
  List<AddOn> addOns;
  String description;
  String list;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory ServiceId.fromJson(Map<String, dynamic> json) => ServiceId(
    id: json["_id"],
    name: json["name"],
    categoryId: json["categoryId"],
    carType: json["carType"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnails: List<String>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    price: json["price"],
    addOns: List<AddOn>.from(json["addOns"].map((x) => AddOn.fromJson(x))),
    description: json["description"],
    list: json["list"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "categoryId": categoryId,
    "carType": carType,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "price": price,
    "addOns": List<dynamic>.from(addOns.map((x) => x.toJson())),
    "description": description,
    "list": list,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}
