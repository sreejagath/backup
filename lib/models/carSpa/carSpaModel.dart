// To parse this JSON data, do
//
//     final carSpaModel = carSpaModelFromJson(jsonString);

import 'dart:convert';

CarSpaModel carSpaModelFromJson(String str) => CarSpaModel.fromJson(json.decode(str));

String carSpaModelToJson(CarSpaModel data) => json.encode(data.toJson());

class CarSpaModel {
  CarSpaModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<CarSpaResultData> resultData;

  factory CarSpaModel.fromJson(Map<String, dynamic> json) => CarSpaModel(
    status: json["status"],
    message: json["message"],
    resultData: List<CarSpaResultData>.from(json["resultData"].map((x) => CarSpaResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class CarSpaResultData {
  CarSpaResultData({
    this.id,
    this.name,
    this.images,
    this.thumbnails,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  List<dynamic> images;
  List<dynamic> thumbnails;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<dynamic> thumbUrl;
  List<dynamic> imageUrl;

  factory CarSpaResultData.fromJson(Map<String, dynamic> json) => CarSpaResultData(
    id: json["_id"],
    name: json["name"],
    images: List<dynamic>.from(json["images"].map((x) => x)),
    thumbnails: List<dynamic>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<dynamic>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<dynamic>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}
