// To parse this JSON data, do
//
//     final quickHelpServiceModel = quickHelpServiceModelFromJson(jsonString);

import 'dart:convert';

QuickHelpServiceModel quickHelpServiceModelFromJson(String str) => QuickHelpServiceModel.fromJson(json.decode(str));

String quickHelpServiceModelToJson(QuickHelpServiceModel data) => json.encode(data.toJson());

class QuickHelpServiceModel {
  QuickHelpServiceModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<QuickHelpServiceResultData> resultData;

  factory QuickHelpServiceModel.fromJson(Map<String, dynamic> json) => QuickHelpServiceModel(
    status: json["status"],
    message: json["message"],
    resultData: List<QuickHelpServiceResultData>.from(json["resultData"].map((x) => QuickHelpServiceResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class QuickHelpServiceResultData {
  QuickHelpServiceResultData({
    this.id,
    this.name,
    this.categoryId,
    this.carType,
    this.images,
    this.thumbnails,
    this.isActive,
    this.price,
    this.addOns,
    this.description,
    this.list,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  String categoryId;
  String carType;
  List<String> images;
  List<String> thumbnails;
  bool isActive;
  num price;
  List<AddOn> addOns;
  String description;
  String list;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory QuickHelpServiceResultData.fromJson(Map<String, dynamic> json) => QuickHelpServiceResultData(
    id: json["_id"],
    name: json["name"],
    categoryId: json["categoryId"],
    carType: json["carType"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnails: List<String>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    price: json["price"],
    addOns: List<AddOn>.from(json["addOns"].map((x) => AddOn.fromJson(x))),
    description: json["description"],
    list: json["list"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "categoryId": categoryId,
    "carType": carType,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "price": price,
    "addOns": List<dynamic>.from(addOns.map((x) => x.toJson())),
    "description": description,
    "list": list,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}

class AddOn {
  AddOn({
    this.name,
    this.price,
    this.id,
  });

  String name;
  num price;
  String id;

  factory AddOn.fromJson(Map<String, dynamic> json) => AddOn(
    name: json["name"],
    price: json["price"],
    id: json["_id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "price": price,
    "_id": id,
  };
}
