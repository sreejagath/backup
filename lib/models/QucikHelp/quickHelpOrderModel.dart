// To parse this JSON data, do
//
//     final quickHelpOrderModel = quickHelpOrderModelFromJson(jsonString);

import 'dart:convert';

QuickHelpOrderModel quickHelpOrderModelFromJson(String str) => QuickHelpOrderModel.fromJson(json.decode(str));

String quickHelpOrderModelToJson(QuickHelpOrderModel data) => json.encode(data.toJson());

class QuickHelpOrderModel {
  QuickHelpOrderModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<QuickHelpResultData> resultData;

  factory QuickHelpOrderModel.fromJson(Map<String, dynamic> json) => QuickHelpOrderModel(
    status: json["status"],
    message: json["message"],
    resultData: List<QuickHelpResultData>.from(json["resultData"].map((x) => QuickHelpResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class QuickHelpResultData {
  QuickHelpResultData({
    this.id,
    this.orderId,
    this.customerId,
    this.franchiseId,
    this.serviceId,
    this.timeSlot,
    this.price,
    this.mode,
    this.status,
    this.address,
    this.addOn,
    this.discountAmount,
    this.grandTotal,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String orderId;
  String customerId;
  String franchiseId;
  ServiceId serviceId;
  String timeSlot;
  num price;
  String mode;
  String status;
  Address address;
  List<dynamic> addOn;
  num discountAmount;
  num grandTotal;
  DateTime createdAt;
  DateTime updatedAt;
  num v;

  factory QuickHelpResultData.fromJson(Map<String, dynamic> json) => QuickHelpResultData(
    id: json["_id"],
    orderId: json["orderId"],
    customerId: json["customerId"],
    franchiseId: json["franchiseId"],
    serviceId: ServiceId.fromJson(json["serviceId"]),
    timeSlot: json["timeSlot"],
    price: json["price"],
    mode: json["mode"],
    status: json["status"],
    address: Address.fromJson(json["address"]),
    addOn: List<dynamic>.from(json["addOn"].map((x) => x)),
    discountAmount: json["discountAmount"],
    grandTotal: json["grandTotal"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "orderId": orderId,
    "customerId": customerId,
    "franchiseId": franchiseId,
    "serviceId": serviceId.toJson(),
    "timeSlot": timeSlot,
    "price": price,
    "mode": mode,
    "status": status,
    "address": address.toJson(),
    "addOn": List<dynamic>.from(addOn.map((x) => x)),
    "discountAmount": discountAmount,
    "grandTotal": grandTotal,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}

class Address {
  Address({
    this.id,
    this.name,
    this.mobile,
    this.house,
    this.street,
    this.city,
    this.state,
    this.pincode,
    this.landmark,
    this.type,
    this.location,
    this.user,
    this.isDefault,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String name;
  num mobile;
  String house;
  String street;
  String city;
  String state;
  num pincode;
  String landmark;
  String type;
  List<double> location;
  String user;
  bool isDefault;
  DateTime createdAt;
  DateTime updatedAt;
  num v;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    id: json["_id"],
    name: json["name"],
    mobile: json["mobile"],
    house: json["house"],
    street: json["street"],
    city: json["city"],
    state: json["state"],
    pincode: json["pincode"],
    landmark: json["landmark"],
    type: json["type"],
    location: List<double>.from(json["location"].map((x) => x.toDouble())),
    user: json["user"],
    isDefault: json["isDefault"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "mobile": mobile,
    "house": house,
    "street": street,
    "city": city,
    "state": state,
    "pincode": pincode,
    "landmark": landmark,
    "type": type,
    "location": List<dynamic>.from(location.map((x) => x)),
    "user": user,
    "isDefault": isDefault,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}

class ServiceId {
  ServiceId({
    this.id,
    this.name,
    this.categoryId,
    this.carType,
    this.images,
    this.thumbnails,
    this.isActive,
    this.price,
    this.description,
    this.list,
    this.addOns,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  String categoryId;
  String carType;
  List<String> images;
  List<String> thumbnails;
  bool isActive;
  num price;
  String description;
  String list;
  List<dynamic> addOns;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory ServiceId.fromJson(Map<String, dynamic> json) => ServiceId(
    id: json["_id"],
    name: json["name"],
    categoryId: json["categoryId"],
    carType: json["carType"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnails: List<String>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    price: json["price"],
    description: json["description"],
    list: json["list"],
    addOns: List<dynamic>.from(json["addOns"].map((x) => x)),
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "categoryId": categoryId,
    "carType": carType,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "price": price,
    "description": description,
    "list": list,
    "addOns": List<dynamic>.from(addOns.map((x) => x)),
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}
