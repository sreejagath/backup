// To parse this JSON data, do
//
//     final quickHelpCategoryModel = quickHelpCategoryModelFromJson(jsonString);

import 'dart:convert';

QuickHelpCategoryModel quickHelpCategoryModelFromJson(String str) => QuickHelpCategoryModel.fromJson(json.decode(str));

String quickHelpCategoryModelToJson(QuickHelpCategoryModel data) => json.encode(data.toJson());

class QuickHelpCategoryModel {
  QuickHelpCategoryModel({
    this.status,
    this.message,
    this.resultData,
  });

  String status;
  String message;
  List<QuickHelpCategoryResultData> resultData;

  factory QuickHelpCategoryModel.fromJson(Map<String, dynamic> json) => QuickHelpCategoryModel(
    status: json["status"],
    message: json["message"],
    resultData: List<QuickHelpCategoryResultData>.from(json["resultData"].map((x) => QuickHelpCategoryResultData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "resultData": List<dynamic>.from(resultData.map((x) => x.toJson())),
  };
}

class QuickHelpCategoryResultData {
  QuickHelpCategoryResultData({
    this.id,
    this.name,
    this.images,
    this.thumbnails,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.thumbUrl,
    this.imageUrl,
  });

  String id;
  String name;
  List<String> images;
  List<String> thumbnails;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;
  num v;
  List<String> thumbUrl;
  List<String> imageUrl;

  factory QuickHelpCategoryResultData.fromJson(Map<String, dynamic> json) => QuickHelpCategoryResultData(
    id: json["_id"],
    name: json["name"],
    images: List<String>.from(json["images"].map((x) => x)),
    thumbnails: List<String>.from(json["thumbnails"].map((x) => x)),
    isActive: json["isActive"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    thumbUrl: List<String>.from(json["thumbURL"].map((x) => x)),
    imageUrl: List<String>.from(json["imageURL"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "images": List<dynamic>.from(images.map((x) => x)),
    "thumbnails": List<dynamic>.from(thumbnails.map((x) => x)),
    "isActive": isActive,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
    "thumbURL": List<dynamic>.from(thumbUrl.map((x) => x)),
    "imageURL": List<dynamic>.from(imageUrl.map((x) => x)),
  };
}
