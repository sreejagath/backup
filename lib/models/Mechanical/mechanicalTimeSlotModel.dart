// To parse this JSON data, do
//
//     final mechanicalTimeSlotModel = mechanicalTimeSlotModelFromJson(jsonString);

import 'dart:convert';

MechanicalTimeSlotModel mechanicalTimeSlotModelFromJson(String str) => MechanicalTimeSlotModel.fromJson(json.decode(str));

String mechanicalTimeSlotModelToJson(MechanicalTimeSlotModel data) => json.encode(data.toJson());

class MechanicalTimeSlotModel {
  MechanicalTimeSlotModel({
    this.status,
    this.message,
    this.result,
  });

  String status;
  String message;
  List<String> result;

  factory MechanicalTimeSlotModel.fromJson(Map<String, dynamic> json) => MechanicalTimeSlotModel(
    status: json["status"],
    message: json["message"],
    result: List<String>.from(json["result"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "result": List<dynamic>.from(result.map((x) => x)),
  };
}
