import 'package:pexa_customer/controller/splash_controller.dart';
import 'package:pexa_customer/controller/theme_controller.dart';
import 'package:pexa_customer/data/repository/splash_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

Future<Map<String, Map<String, String>>> init() async {
  // Core
  final sharedPreferences = await SharedPreferences.getInstance();
  Get.lazyPut(() => sharedPreferences);

  // Repository
  Get.lazyPut(() => SplashRepo(sharedPreferences: Get.find(),));

  // Controller
  Get.lazyPut(() => ThemeController(sharedPreferences: Get.find()));
  Get.lazyPut(() => SplashController(splashRepo: Get.find()));
  Map<String, Map<String, String>> _languages = Map();
  return _languages;
}
