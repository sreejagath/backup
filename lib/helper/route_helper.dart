import 'dart:convert';

import 'package:pexa_customer/view/screens/category/category_screen.dart';
import 'package:pexa_customer/view/screens/infoPage/infoPage.dart';
import 'package:pexa_customer/view/screens/profile/profile_screen.dart';
import 'package:pexa_customer/view/screens/splash/splash_screen.dart';
import 'package:pexa_customer/view/screens/support/support_screen.dart';
import 'package:pexa_customer/view/screens/dashboard/bottomAppBar.dart';
import 'package:get/get.dart';

class RouteHelper {
  static const String initial = '/';
  static const String splash = '/splash';
  static const String language = '/language';
  static const String selectVehicle = '/vehicle';
  static const String onBoarding = '/on-boarding';
  static const String signIn = '/sign-in';
  static const String signUp = '/sign-up';
  static const String verification = '/verification';
  static const String accessLocation = '/access-location';
  static const String pickMap = '/pick-map';
  static const String interest = '/interest';
  static const String main = '/main';
  static const String productListing = '/productlisting';
  static const String forgotPassword = '/forgot-password';
  static const String resetPassword = '/reset-password';
  static const String search = '/search';
  static const String restaurant = '/restaurant';
  static const String orderDetails = '/order-details';
  static const String profile = '/profile';
  static const String updateProfile = '/update-profile';
  static const String coupon = '/coupon';
  static const String notification = '/notification';
  static const String map = '/map';
  static const String address = '/address';
  static const String orderSuccess = '/order-successful';
  static const String payment = '/payment';
  static const String checkout = '/checkout';
  static const String orderTracking = '/track-order';
  static const String basicCampaign = '/basic-campaign';
  static const String html = '/html';
  static const String info = '/info';
  static const String categories = '/categories';
  static const String categoryProduct = '/category-product';
  static const String popularFoods = '/popular-foods';
  static const String itemCampaign = '/item-campaign';
  static const String support = '/help-and-support';
  static const String rateReview = '/rate-and-review';
  static const String update = '/update';
  static const String cart = '/cart';
  static const String addAddress = '/add-address';
  static const String restaurantReview = '/restaurant-review';
  static const String allRestaurants = '/restaurants';

  static String getInitialRoute() => '$initial';

  static String getSplashRoute() => '$splash';

  static String getLanguageRoute(String page) => '$language?page=$page';

  static String getSelectVehicle(String page) => '$selectVehicle?page=$page';

  static String getProductListingRoute() => '$productListing';

  static String getOnBoardingRoute() => '$onBoarding';

  static String getSignInRoute(String page) => '$signIn?page=$page';

  static String getSignUpRoute() => '$signUp';

  static String getVerificationRoute(
      String number, String token, String page, String pass) {
    return '$verification?page=$page&number=$number&token=$token&pass=$pass';
  }

  static String getAccessLocationRoute(String page) =>
      '$accessLocation?page=$page';

  static String getPickMapRoute(String page, bool canRoute) =>
      '$pickMap?page=$page&route=${canRoute.toString()}';

  static String getInterestRoute() => '$interest';

  static String getMainRoute(String page) => '$main?page=$page';

  static String getForgotPassRoute() => '$forgotPassword';

  static String getResetPasswordRoute(
          String phone, String token, String page) =>
      '$resetPassword?phone=$phone&token=$token&page=$page';

  static String getSearchRoute() => '$search';

  static String getRestaurantRoute(int id) => '$restaurant?id=$id';

  static String getOrderDetailsRoute(int orderID) {
    return '$orderDetails?id=$orderID';
  }

  static String getProfileRoute() => '$profile';

  static String getUpdateProfileRoute() => '$updateProfile';

  static String getCouponRoute() => '$coupon';

  static String getNotificationRoute() => '$notification';

  // static String getMapRoute(AddressModel addressModel, String page) {
  //   List<int> _encoded = utf8.encode(jsonEncode(addressModel.toJson()));
  //   String _data = base64Encode(_encoded);
  //   return '$map?address=$_data&page=$page';
  // }

  static String getAddressRoute() => '$address';

  static String getOrderSuccessRoute(String orderID, String status) =>
      '$orderSuccess?id=$orderID&status=$status';

  static String getPaymentRoute(String id, int user) =>
      '$payment?id=$id&user=$user';

  static String getCheckoutRoute(String page) => '$checkout?page=$page';

  static String getOrderTrackingRoute(int id) => '$orderTracking?id=$id';

  static String getBasicCampaignRoute(int id, String title, String image) =>
      '$basicCampaign?id=$id&image=$image&title=$title';

  static String getHtmlRoute(String page) => '$html?page=$page';

  static String getInfoPage(String page) => '$info?page=$page';

  static String getCategoryRoute() => '$categories';

  static String getCategoryProductRoute(int id, String name) {
    List<int> _encoded = utf8.encode(name);
    String _data = base64Encode(_encoded);
    return '$categoryProduct?id=$id&name=$_data';
  }

  static String getPopularFoodRoute(bool isPopular) =>
      '$popularFoods?page=${isPopular ? 'popular' : 'reviewed'}';

  static String getItemCampaignRoute() => '$itemCampaign';

  static String getSupportRoute() => '$support';

  static String getReviewRoute() => '$rateReview';

  static String getUpdateRoute(bool isUpdate) =>
      '$update?update=${isUpdate.toString()}';

  static String getCartRoute() => '$cart';

  static String getAddAddressRoute() => '$addAddress';

  static String getRestaurantReviewRoute(int restaurantID) =>
      '$restaurantReview?id=$restaurantID';

  static String getAllRestaurantRoute(String page) =>
      '$allRestaurants?page=$page';

  static List<GetPage> routes = [
    GetPage(
        name: initial,
        page: () => NewDashBoard(
              pageIndex: 0,
            )
        ),
    GetPage(
        name: splash,
        page: () => SplashScreen(
            // orderID:
            //     Get.parameters['id'] == 'null' ? null : Get.parameters['id']
                )),

    GetPage(name: profile, page: () => ProfileScreen()
    ),

    GetPage(
        name: info,
        page: () => InformationPage(
          title: Get.parameters['page'],
        )),
    GetPage(name: categories, page: () => CategoryScreen()
    ),

    GetPage(name: support, page: () => SupportScreen()
        ),

  ];
}
