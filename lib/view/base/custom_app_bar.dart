import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/helper/route_helper.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/cart_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool isBackButtonExist;
  final Function onBackPressed;
  final bool showCart;

  CustomAppBar(
      {@required this.title,
      this.isBackButtonExist = true,
      this.onBackPressed,
      this.showCart = false});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title, style: mediumFont(Colors.black)),
      centerTitle: true,
      leading: isBackButtonExist
          ? IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Theme.of(context).textTheme.bodyText1.color,
              onPressed: () => onBackPressed != null
                  ? onBackPressed()
                  : Navigator.pop(context),
            )
          : SizedBox(),
      backgroundColor: Theme.of(context).cardColor,
      elevation: 0,
      actions: showCart
          ? [
              IconButton(
                onPressed: () => Get.toNamed(RouteHelper.getCartRoute()),
                icon: CartWidget(
                    color: Theme.of(context).textTheme.bodyText1.color,
                    size: 25),
              )
            ]
          : null,
    );
  }

  @override
  Size get preferredSize =>
      Size(Dimensions.WEB_MAX_WIDTH, GetPlatform.isDesktop ? 70 : 50);
}
