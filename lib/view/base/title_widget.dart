import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/helper/responsive_helper.dart';
import 'package:flutter/material.dart';

class TitleWidget extends StatelessWidget {
  final String title;
  final Function onTap;
  TitleWidget({@required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(title, style: mediumFont(Colors.black)),
      (onTap != null && !ResponsiveHelper.isDesktop(context)) ? InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 5, 0, 5),
          child: Text(
            'View All',
            style: smallFont(Colors.black),
          ),
        ),
      ) : SizedBox(),
    ]);
  }
}
