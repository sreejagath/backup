import 'package:pexa_customer/util/dimensions.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showCustomSnackBar(String message, {bool isError = true}) {
  Get.showSnackbar(GetSnackBar(
    backgroundColor: isError ? Colors.red : Colors.green,
    message: message,
    maxWidth: Dimensions.WEB_MAX_WIDTH,
    duration: Duration(seconds: 3),
    snackStyle: SnackStyle.FLOATING,
    margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
    borderRadius: Dimensions.RADIUS_SMALL,
    isDismissible: true,
    dismissDirection: DismissDirection.horizontal,
  ));
}

void customSnackBar(context, message, {bool isError = true}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(message, style: TextStyle(color: Colors.white),),
    elevation: 0,
    behavior: SnackBarBehavior.floating,
    backgroundColor: isError ? Colors.red.withOpacity(1) : Colors.green,
    duration: Duration(seconds: 2),
    margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
  ));
}
