import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:flutter/cupertino.dart';

class CustomImage extends StatelessWidget {
  final String image;
  final double height;
  final double width;
  final BoxFit fit;
  final String placeholder;

  CustomImage(
      {@required this.image,
      this.height,
      this.width,
      this.fit,
      this.placeholder});

  final CacheManager cacheManager = CacheManager(Config('images_Key',
      maxNrOfCacheObjects: 200, stalePeriod: const Duration(days: 30)));

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      cacheManager: cacheManager,
      imageUrl: image,
      height: height,
      width: width,
      fit: fit,
      placeholder: (context, url) => Center(
          child: SizedBox(
              // height: 35,
              // width: 35,
              child: Image.asset(Images.loading, fit: BoxFit.cover))),
      errorWidget: (context, url, error) => Center(
          child: SizedBox(
              height: 35,
              width: 35,
              child: Image.asset(Images.loading, fit: BoxFit.fill))),
    );
  }
}
