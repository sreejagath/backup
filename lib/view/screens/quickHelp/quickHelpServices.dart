import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/quickHelp/widget/quickHelpServiceTile.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuickHelpServicesPage extends StatelessWidget {
  QuickHelpServicesPage({Key key, @required this.title, this.index})
      : super(key: key);
  final String title;
  final int index;
  final quickHelpController = Get.find<QuickHelpController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              title: IntrinsicWidth(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: Colors.white.withOpacity(0.5),
                  ),
                  child: Center(
                      child: Text(title.toUpperCase(),
                          style: mediumFont(Colors.black))),
                ),
              ),
              centerTitle: true,
              leading: SizedBox(
                child: Center(
                  child: InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: Colors.white.withOpacity(0.5),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.black,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            backgroundColor: Colors.white,
            body: quickHelpController.quickHelpServiceList.isEmpty
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Services not listed',
                            style: smallFont(Colors.black)),
                        Get.find<AuthFactorsController>().isLoggedIn
                            ? SizedBox()
                            : Text(
                                'Please select the Car Model...!',
                                style: mediumFont(Colors.red),
                              )
                      ],
                    ),
                  )
                : CustomScrollView(
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    slivers: [
                      SliverToBoxAdapter(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 250,
                          child: Stack(
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 250,
                                child: Image.network(
                                  'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FserviceBanner%2Fquick.jpg?alt=media&token=20306bae-697d-4d4b-8550-443c1da91186',
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                child: Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        topRight: Radius.circular(20)),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(List.generate(
                            quickHelpController.quickHelpServiceList.length,
                            (index) {
                          return QuickHelpServiceTile(
                            index: index,
                            quickHelpServiceResultData:
                                quickHelpController.quickHelpServiceList[index],
                          );
                        })),
                      ),
                      SliverToBoxAdapter(
                          child: SizedBox(
                        height: 10,
                      ))
                    ],
                  ))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
