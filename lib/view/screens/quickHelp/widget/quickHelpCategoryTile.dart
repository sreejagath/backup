import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/models/QucikHelp/qucikHelpCategory.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/quickHelp/quickHelpServices.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuickHelpCategoryTile extends StatelessWidget {
  QuickHelpCategoryTile({Key key, this.quickHelpCategoryResultData, this.index})
      : super(key: key);
  final QuickHelpCategoryResultData quickHelpCategoryResultData;
  final int index;
  final quickHelpController = Get.find<QuickHelpController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          
          
          quickHelpController
              .getQuickHelpServiceWithCatId(quickHelpCategoryResultData.id)
              .then((value) => Get.to(() => QuickHelpServicesPage(
            title: quickHelpCategoryResultData.name,
            index: index,
          )));
        },
        child: Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                Container(
                  height: 75,
                  width: 75,
                  clipBehavior: Clip.hardEdge,
                  margin: EdgeInsets.only(top: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Center(
                      child: CustomImage(
                        image: quickHelpCategoryResultData.thumbUrl[0],
                        height: 50,
                      )),
                ),
                SizedBox(height: 10),
                Flexible(
                  child: SizedBox(
                    width: 90,
                    child: Text(
                      quickHelpCategoryResultData.name,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: verySmallFontW600(Colors.black),
                    ),
                  ),
                ),
              ],
            )));
  }
}