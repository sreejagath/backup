import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/controller/tab_controller.dart';
import 'package:pexa_customer/view/screens/quickHelp/widget/quickHelpCategoryTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuickHelpSectionNew extends StatelessWidget {
  const QuickHelpSectionNew({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final control = Get.find<TabControllerMethod>();
    final quickHelpController = Get.find<QuickHelpController>();

    return Obx(
      () => SliverToBoxAdapter(
        child: Container(
          decoration: BoxDecoration(
              image: control.tabIndex.value == 1
                  ? DecorationImage(
                      image: AssetImage(
                        "assets/carSpa/logo2.png",
                      ),
                      fit: BoxFit.cover,
                    )
                  : null),
          child: CustomScrollView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              primary: false,
              slivers: [
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
                      mainAxisSpacing: 10.0,
                      crossAxisSpacing: 10.0,
                      childAspectRatio: 1.0),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return Obx(
                        () => quickHelpController
                                    .quickHelpCategoryData.length ==
                                0
                            ? Container(
                                child: Center(
                                child: Text('No Products'),
                              ))
                            : QuickHelpCategoryTile(
                                index: index,
                                quickHelpCategoryResultData: quickHelpController
                                    .quickHelpCategoryData[index],
                              ),
                      );
                    },
                    childCount: quickHelpController
                                .quickHelpCategoryData.length !=
                            0
                        ? control.tabIndex.value == 6
                            ? quickHelpController.quickHelpCategoryData.length >
                                    3
                                ? 3
                                : quickHelpController
                                    .quickHelpCategoryData.length
                            : quickHelpController.quickHelpCategoryData.length
                        : 0,
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
