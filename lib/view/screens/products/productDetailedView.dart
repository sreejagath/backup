import 'package:carousel_slider/carousel_slider.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/productController.dart';
import 'package:pexa_customer/view/base/cart_widget.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carShoppee/carShoppeBuyNow.dart';
import 'package:pexa_customer/view/screens/cart/cart_screen.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductDetailedView extends StatefulWidget {
  ProductDetailedView({Key key}) : super(key: key);

  @override
  _ProductDetailedViewState createState() => _ProductDetailedViewState();
}

class _ProductDetailedViewState extends State<ProductDetailedView> {
  final productCategoryController = Get.find<ProductCategoryController>();
  final cartController = Get.find<CartControllerFile>();
  String prodQty = '1';

  @override
  Widget build(BuildContext context) {
    Get.find<ProductDetailsController>()
        .checkStockStatus(productCategoryController.product['quantity']);
    Get.find<ProductDetailsController>()
        .checkProductInCart(productCategoryController.product['_id']);
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            backgroundColor: Colors.white,
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey.withAlpha(0),
              elevation: 0,
              leading: SizedBox(
                child: Center(
                  child: InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: white.withOpacity(0.4),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.black,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              actions: [
                SizedBox(
                  child: Center(
                    child: Bouncing(
                      onPress: () {
                        cartController
                            .getCartDetails(true)
                            .then((value) => Get.to(() => CartScreen(
                                  fromNav: false,
                                  fromMain: true,
                                  prodId:
                                      productCategoryController.product['_id'],
                                )));
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          color: white.withOpacity(0.4),
                        ),
                        child: Center(
                          child: CartWidget(
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                              size: 20),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                )
              ],
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Stack(
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width,
                          child: CarouselSlider.builder(
                            options: CarouselOptions(
                              autoPlay: false,
                              viewportFraction: 1.0,
                              enlargeCenterPage: true,
                              height: 260,
                              disableCenter: true,
                              autoPlayInterval: Duration(seconds: 6),
                              onPageChanged: (index, reason) {},
                            ),
                            itemCount: productCategoryController
                                .product['imageURL'].length,
                            itemBuilder: (context, index, _) {
                              return Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).cardColor,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors
                                            .grey[Get.isDarkMode ? 800 : 200],
                                        spreadRadius: 1,
                                        blurRadius: 5)
                                  ],
                                ),
                                padding: EdgeInsets.all(20),
                                child: ClipRRect(
                                  child: Image.network(
                                    productCategoryController
                                        .product['imageURL'][index],
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              );
                            },
                          )),
                      Positioned(
                        top: MediaQuery.of(context).size.height * 0.35,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black,
                                blurRadius: 20,
                                offset: Offset(0, 10),
                              ),
                            ],
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              productCategoryController.product['name']
                                  .toString()
                                  .toUpperCase(),
                              style: largeFont(Colors.black)),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: [
                              productCategoryController.product['price']==productCategoryController.product['offerPrice']?
                              Container():
                              Row(children:[
                              Text(
                                productCategoryController.product['price']
                                    .toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: mediumFontWithLine(Colors.grey),
                              ),
                              SizedBox(
                                width: 10,
                              ),]),
                              Text(
                                '₹ ' +
                                    productCategoryController
                                        .product['offerPrice']
                                        .toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: mediumFont(Colors.black),
                              ),SizedBox(
                                width: 10,
                              ),
                              productCategoryController
                                        .product['offerPrice']!=productCategoryController
                                        .product['price']?
                              Text('You will save ₹ ${
                                  productCategoryController
                                      .product['price'] -
                                      productCategoryController
                                          .product['offerPrice']
                              } on this purchase',style: mediumFont(Colors.green),):Container(),
                             // }')
                            //:Container()
                            ],
                          ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          // Text(
                          //   "Be the first to review the product",
                          //   style: robotoRegular.copyWith(
                          //       fontSize: 15, color: botAppBarColor2),
                          // ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          // Container(
                          //     width: MediaQuery.of(context).size.width * 0.65,
                          //     child: RatingBar(
                          //         size: 22, rating: 3.4, ratingCount: 3)),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Details : ",
                                style: smallFontW600(Colors.black),
                              ),
                              SizedBox(
                                width: 225,
                                child: Text(
                                  productCategoryController
                                      .product['description'],
                                  style: mediumFont(Colors.black),
                                  maxLines: 6,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Get.find<ProductDetailsController>().outOfStock.value
                              ? SizedBox()
                              : Row(
                                  children: [
                                    Text(
                                      "Stock : ",
                                      style: smallFontW600(Colors.black),
                                    ),
                                    Text(
                                        Get.find<ProductDetailsController>()
                                                .outOfStock
                                                .value
                                            ? "OUT OF STOCK"
                                            : productCategoryController
                                                .product['quantity']
                                                .toString(),
                                        style: smallFont(Colors.red))
                                  ],
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Quantity",
                            style: mediumFont(Colors.black),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 40,
                            width: 60,
                            decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                border: Border.all(color: Colors.black)),
                            child: Center(
                              child: DropdownButton(
                                hint: Text("1"),
                                value: prodQty,
                                style: TextStyle(
                                    fontSize: 20, color: Colors.black),
                                icon: const Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 15,
                                ),
                                items: ['1', '2', '3', '4', '5', '6']
                                    .map((String items) {
                                  return DropdownMenuItem(
                                    value: items,
                                    child: Text(items),
                                  );
                                }).toList(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    prodQty = newValue;
                                  });
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              color: Colors.transparent,
              elevation: 0,
              child: Container(
                height: Get.find<ProductDetailsController>().outOfStock.value
                    ? 110
                    : 80,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                child: Get.find<ProductDetailsController>().outOfStock.value
                    ? Column(
                        children: [
                          SizedBox(
                            child: Text(
                              'OUT OF STOCK',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 2,
                                child: Obx(
                                  () => Bouncing(
                                    onPress: Get.find<
                                                ProductDetailsController>()
                                            .outOfStock
                                            .value
                                        ? null
                                        : Get.find<ProductDetailsController>()
                                                .isInCart
                                                .value
                                            ? () {
                                                Get.find<
                                                        ProductDetailsController>()
                                                    .checkProductInCart(
                                                        productCategoryController
                                                            .product['_id'])
                                                    .then((value) =>
                                                        cartController
                                                            .getCartDetails(
                                                                true)
                                                            .then((value) =>
                                                                Get.to(() =>
                                                                    CartScreen(
                                                                      fromNav:
                                                                          false,
                                                                      fromMain:
                                                                          true,
                                                                      prodId: productCategoryController
                                                                              .product[
                                                                          '_id'],
                                                                    ))));
                                              }
                                            : () {
                                                if (Get.find<
                                                        AuthFactorsController>()
                                                    .isLoggedIn) {
                                                  if (int.parse(prodQty) >
                                                      productCategoryController
                                                              .product[
                                                          'quantity']) {
                                                    Get.snackbar('Warning',
                                                        'Quantity must be less than the Stock...!',
                                                        snackPosition:
                                                            SnackPosition.TOP,
                                                        duration: Duration(
                                                            seconds: 2),
                                                        backgroundColor:
                                                            Colors.red,
                                                        colorText: Colors.white,
                                                        snackStyle: SnackStyle
                                                            .FLOATING);
                                                    return;
                                                  } else {
                                                    cartController
                                                        .addOrUpdateToCart(
                                                            productCategoryController
                                                                .product['_id'],
                                                            int.parse(prodQty),
                                                            'add')
                                                        .then((value) => Get.find<
                                                                ProductDetailsController>()
                                                            .checkProductInCart(
                                                                productCategoryController
                                                                        .product[
                                                                    '_id']));
                                                  }
                                                } else {
                                                  Get.snackbar('Failed',
                                                      'Please Login to Continue...!',
                                                      snackPosition:
                                                          SnackPosition.TOP,
                                                      duration:
                                                          Duration(seconds: 2),
                                                      backgroundColor:
                                                          Colors.red,
                                                      colorText: Colors.white,
                                                      snackStyle:
                                                          SnackStyle.FLOATING);
                                                }
                                              },
                                    child: Container(
                                      height: 40,
                                      decoration: BoxDecoration(
                                          color: Get.find<
                                                      ProductDetailsController>()
                                                  .outOfStock
                                                  .value
                                              ? Colors.grey
                                              : botAppBarColor,
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.8),
                                              blurRadius: Get.find<
                                                          ProductDetailsController>()
                                                      .outOfStock
                                                      .value
                                                  ? 0
                                                  : 7,
                                              offset: Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Center(
                                          child: Obx(
                                        () => Text(
                                          Get.find<ProductDetailsController>()
                                                  .isInCart
                                                  .value
                                              ? "Go to Cart"
                                              : "Add to Cart",
                                          style: mediumFont(Colors.black),
                                        ),
                                      )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 2,
                                child: Bouncing(
                                  onPress: Get.find<ProductDetailsController>()
                                          .outOfStock
                                          .value
                                      ? null
                                      : () {
                                          if (Get.find<AuthFactorsController>()
                                              .isLoggedIn) {
                                            if (int.parse(prodQty) >
                                                productCategoryController
                                                    .product['quantity']) {
                                              Get.snackbar('Warning',
                                                  'Quantity must be less than the Stock...!',
                                                  snackPosition:
                                                      SnackPosition.TOP,
                                                  duration:
                                                      Duration(seconds: 2),
                                                  backgroundColor: Colors.red,
                                                  colorText: Colors.white,
                                                  snackStyle:
                                                      SnackStyle.FLOATING);
                                              return;
                                            } else {
                                              // Get.to(() => CarShoppeBuyNow(
                                              //       prodId: productCategoryController
                                              //           .product['_id'],
                                              //       price: productCategoryController
                                              //           .product['price'],
                                              //       offerPrice: productCategoryController
                                              //           .product['offerPrice'],
                                              //       count: int.parse(prodQty),
                                              //     ));
                                              Get.find<CouponController>()
                                                  .clearValue();
                                              print((int.parse(prodQty) *
                                                      productCategoryController
                                                              .product[
                                                          'offerPrice'])
                                                  .toString());
                                              Get.find<AddressControllerFile>()
                                                  .getAddress()
                                                  .then((value) => {
                                                        if (Get.find<
                                                                AddressControllerFile>()
                                                            .addressList
                                                            .isNotEmpty)
                                                          {
                                                            Get.find<AddressControllerFile>()
                                                                .getDefaultAddress()
                                                                .then((value) => Get
                                                                        .find<
                                                                            ProductCategoryController>()
                                                                    .getShippingDetails((int.parse(prodQty) *
                                                                            productCategoryController.product[
                                                                                'offerPrice'])
                                                                        .toString())
                                                                    .then((value) =>
                                                                        Get.to(() =>
                                                                            CarShoppeBuyNow(
                                                                              prodId: productCategoryController.product['_id'],
                                                                              price: productCategoryController.product['price'],
                                                                              offerPrice: productCategoryController.product['offerPrice'],
                                                                              count: int.parse(prodQty),
                                                                            ))))
                                                          }
                                                        else
                                                          {
                                                            Get.to(() =>
                                                                CarShoppeBuyNow(
                                                                  prodId: productCategoryController
                                                                          .product[
                                                                      '_id'],
                                                                  price: productCategoryController
                                                                          .product[
                                                                      'price'],
                                                                  offerPrice: productCategoryController
                                                                          .product[
                                                                      'offerPrice'],
                                                                  count: int.parse(
                                                                      prodQty),
                                                                ))
                                                            // carShoppeBuyNowAddressBottumUp(
                                                            //     context,
                                                            //     productCategoryController
                                                            //         .product['_id'],
                                                            //     productCategoryController
                                                            //         .product['price'],
                                                            //     productCategoryController
                                                            //             .product[
                                                            //         'offerPrice'],
                                                            //     int.parse(prodQty))
                                                          }
                                                      });
                                            }
                                          } else {
                                            Get.snackbar('Failed',
                                                'Please Login to Continue...!',
                                                snackPosition:
                                                    SnackPosition.TOP,
                                                duration: Duration(seconds: 2),
                                                backgroundColor: Colors.red,
                                                colorText: Colors.white,
                                                snackStyle:
                                                    SnackStyle.FLOATING);
                                          }
                                        },
                                  child: Container(
                                    height: 40,
                                    decoration: BoxDecoration(
                                        color:
                                            Get.find<ProductDetailsController>()
                                                    .outOfStock
                                                    .value
                                                ? Colors.grey
                                                : botAppBarColor,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.8),
                                            blurRadius: Get.find<
                                                        ProductDetailsController>()
                                                    .outOfStock
                                                    .value
                                                ? 0
                                                : 7,
                                            offset: Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Center(
                                      child: Text(
                                        "Buy Now",
                                        style: mediumFont(Colors.black),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      )
                    : Row(
                        children: [
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            flex: 2,
                            child: Obx(
                              () => Bouncing(
                                onPress: Get.find<ProductDetailsController>()
                                        .outOfStock
                                        .value
                                    ? null
                                    : Get.find<ProductDetailsController>()
                                            .isInCart
                                            .value
                                        ? () {
                                            Get.find<ProductDetailsController>()
                                                .checkProductInCart(
                                                    productCategoryController
                                                        .product['_id'])
                                                .then((value) => cartController
                                                    .getCartDetails(true)
                                                    .then((value) =>
                                                        Get.to(() => CartScreen(
                                                              fromNav: false,
                                                              fromMain: true,
                                                              prodId:
                                                                  productCategoryController
                                                                          .product[
                                                                      '_id'],
                                                            ))));
                                          }
                                        : () {
                                            if (Get.find<
                                                    AuthFactorsController>()
                                                .isLoggedIn) {
                                              if (int.parse(prodQty) >
                                                  productCategoryController
                                                      .product['quantity']) {
                                                Get.snackbar('Warning',
                                                    'Quantity must be less than the Stock...!',
                                                    snackPosition:
                                                        SnackPosition.TOP,
                                                    duration:
                                                        Duration(seconds: 2),
                                                    backgroundColor: Colors.red,
                                                    colorText: Colors.white,
                                                    snackStyle:
                                                        SnackStyle.FLOATING);
                                                return;
                                              } else {
                                                cartController
                                                    .addOrUpdateToCart(
                                                        productCategoryController
                                                            .product['_id'],
                                                        int.parse(prodQty),
                                                        'add')
                                                    .then((value) => Get.find<
                                                            ProductDetailsController>()
                                                        .checkProductInCart(
                                                            productCategoryController
                                                                    .product[
                                                                '_id']));
                                              }
                                            } else {
                                              Get.snackbar('Failed',
                                                  'Please Login to Continue...!',
                                                  snackPosition:
                                                      SnackPosition.TOP,
                                                  duration:
                                                      Duration(seconds: 2),
                                                  backgroundColor: Colors.red,
                                                  colorText: Colors.white,
                                                  snackStyle:
                                                      SnackStyle.FLOATING);
                                            }
                                          },
                                child: Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color:
                                          Get.find<ProductDetailsController>()
                                                  .outOfStock
                                                  .value
                                              ? Colors.grey
                                              : botAppBarColor,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.8),
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Center(
                                      child: Obx(
                                    () => Text(
                                      Get.find<ProductDetailsController>()
                                              .isInCart
                                              .value
                                          ? "Go to Cart"
                                          : "Add to Cart",
                                      style: mediumFont(Colors.black),
                                    ),
                                  )),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            flex: 2,
                            child: Bouncing(
                              onPress: Get.find<ProductDetailsController>()
                                      .outOfStock
                                      .value
                                  ? null
                                  : () {
                                      if (Get.find<AuthFactorsController>()
                                          .isLoggedIn) {
                                        if (int.parse(prodQty) >
                                            productCategoryController
                                                .product['quantity']) {
                                          Get.snackbar('Warning',
                                              'Quantity must be less than the Stock...!',
                                              snackPosition: SnackPosition.TOP,
                                              duration: Duration(seconds: 2),
                                              backgroundColor: Colors.red,
                                              colorText: Colors.white,
                                              snackStyle: SnackStyle.FLOATING);
                                          return;
                                        } else {
                                          // Get.to(() => CarShoppeBuyNow(
                                          //       prodId: productCategoryController
                                          //           .product['_id'],
                                          //       price: productCategoryController
                                          //           .product['price'],
                                          //       offerPrice: productCategoryController
                                          //           .product['offerPrice'],
                                          //       count: int.parse(prodQty),
                                          //     ));
                                          Get.find<CouponController>()
                                              .clearValue();
                                          print((int.parse(prodQty) *
                                                  productCategoryController
                                                      .product['offerPrice'])
                                              .toString());
                                          Get.find<AddressControllerFile>()
                                              .getAddress()
                                              .then((value) => {
                                                    if (Get.find<
                                                            AddressControllerFile>()
                                                        .addressList
                                                        .isNotEmpty)
                                                      {
                                                        Get.find<AddressControllerFile>()
                                                            .getDefaultAddress()
                                                            .then((value) => Get
                                                                    .find<
                                                                        ProductCategoryController>()
                                                                .getShippingDetails((int.parse(prodQty) *
                                                                        productCategoryController.product[
                                                                            'offerPrice'])
                                                                    .toString())
                                                                .then((value) =>
                                                                        Get.to(() =>
                                                                            CarShoppeBuyNow(
                                                                              prodId: productCategoryController.product['_id'],
                                                                              price: productCategoryController.product['price'],
                                                                              offerPrice: productCategoryController.product['offerPrice'],
                                                                              count: int.parse(prodQty),
                                                                            ))
                                                                    // carShoppeBuyNowAddressBottumUp(
                                                                    //     context,
                                                                    //     productCategoryController
                                                                    //         .product['_id'],
                                                                    //     productCategoryController
                                                                    //         .product['price'],
                                                                    //     productCategoryController
                                                                    //         .product['offerPrice'],
                                                                    //     int.parse(prodQty))
                                                                    ))
                                                      }
                                                    else
                                                      {
                                                        Get.to(
                                                            () =>
                                                                CarShoppeBuyNow(
                                                                  prodId: productCategoryController
                                                                          .product[
                                                                      '_id'],
                                                                  price: productCategoryController
                                                                          .product[
                                                                      'price'],
                                                                  offerPrice: productCategoryController
                                                                          .product[
                                                                      'offerPrice'],
                                                                  count: int.parse(
                                                                      prodQty),
                                                                ))
                                                        // carShoppeBuyNowAddressBottumUp(
                                                        //     context,
                                                        //     productCategoryController
                                                        //         .product['_id'],
                                                        //     productCategoryController
                                                        //         .product['price'],
                                                        //     productCategoryController
                                                        //             .product[
                                                        //         'offerPrice'],
                                                        //     int.parse(prodQty))
                                                      }
                                                  });
                                        }
                                      } else {
                                        Get.snackbar('Failed',
                                            'Please Login to Continue...!',
                                            snackPosition: SnackPosition.TOP,
                                            duration: Duration(seconds: 2),
                                            backgroundColor: Colors.red,
                                            colorText: Colors.white,
                                            snackStyle: SnackStyle.FLOATING);
                                      }
                                    },
                              child: Container(
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Get.find<ProductDetailsController>()
                                            .outOfStock
                                            .value
                                        ? Colors.grey
                                        : botAppBarColor,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.8),
                                        blurRadius: 7,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Center(
                                  child: Text(
                                    "Buy Now",
                                    style: mediumFont(Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                        ],
                      ),
              ),
            ),
          )
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
