import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/address_edit/addressDetailsPage.dart';
import 'package:pexa_customer/view/screens/success/success.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class ShoppeCheckOut extends StatelessWidget {
  ShoppeCheckOut({Key key, this.amount}) : super(key: key);
  final double amount;

  final addressController = Get.find<AddressControllerFile>();
  final TextEditingController codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //Get.find<ProductCategoryController>().getShippingDetails(String total);
    return Scaffold(
      appBar: AppBar(
        title: Text("Checkout", style: mediumFont(Colors.black)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 15,
          ),
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Obx(
            () => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/carSpa/latestlogo.png',
                  height: 100,
                ),
                SizedBox(
                  height: 20,
                ),
                Bouncing(
                  onPress: () {
                    Get.find<AddressControllerFile>().fromPexaShoppe.value =
                        true;
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddressDetailsPage(
                                backContext: context,
                              )),
                    );
                  },
                  child: IntrinsicWidth(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                        color: botAppBarColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Add / Change Address',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                addressController.addressList.isEmpty
                    ? SizedBox(
                        height: 80,
                        child: Center(
                          child: (addressController.isNoAddress.value)
                              ? Text(
                                  'No Saved Address',
                                  style: mediumFont(Colors.black),
                                )
                              : SizedBox(
                                  height: 35,
                                  width: 35,
                                  child: Image.asset(Images.spinner,
                                      fit: BoxFit.fill)),
                        ),
                      )
                    : Container(
                        padding: EdgeInsets.all(15),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: addressController.defaultAddress.isNotEmpty
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        addressController
                                            .defaultAddress['name'],
                                        style: mediumFont(Colors.black),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 0.1, horizontal: 5),
                                          decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Text(
                                              addressController
                                                  .defaultAddress['type'],
                                              style: mediumFont(Colors.black))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 2.5,
                                  ),
                                  Text(
                                      addressController
                                              .defaultAddress['house'] +
                                          ', ' +
                                          addressController
                                              .defaultAddress['street'] +
                                          ', ' +
                                          addressController
                                              .defaultAddress['pincode']
                                              .toString(),
                                      style: smallFont(Colors.black)),
                                  SizedBox(
                                    height: 2.5,
                                  ),
                                  Text(
                                    addressController.defaultAddress['mobile']
                                        .toString(),
                                    style: smallFontW600(Colors.black),
                                  ),
                                ],
                              )
                            : Image.asset(
                                'assets/carSpa/loading.gif',
                                height: 50,
                              )),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Apply Coupon Code',
                  style: mediumFont(Colors.black),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 40,
                      width: 220,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Colors.white,
                          border: Border.all(color: Colors.black)),
                      child: TextField(
                        textAlign: TextAlign.center,
                        controller:
                            Get.find<CouponController>().controller.value,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Enter the coupon',
                            hintStyle: smallFont(Colors.grey)),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Bouncing(
                      onPress: () {
                        if (Get.find<CouponController>()
                                    .controller
                                    .value
                                    .text
                                    .trim() !=
                                null ||
                            Get.find<CouponController>()
                                    .controller
                                    .value
                                    .text
                                    .trim() !=
                                '') {
                          Get.find<CouponController>().checkCoupon(amount);
                        }
                      },
                      child: Container(
                        height: 35,
                        width: 35,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: botAppBarColor,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: Icon(
                            Icons.verified_outlined,
                            color: Colors.black,
                            size: 15,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Get.find<CouponController>().showDetails.value
                    ? Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'This Coupon will be applicable only for the minimum purchase'
                            'amount of ${Get.find<CouponController>().minAmount.value}',
                            style: verySmallFontW600(Colors.red),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )
                    : SizedBox(),
                Get.find<CouponController>().isApplied.value
                    ? Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                (Get.find<CouponController>().couponName.value),
                                style: mediumFont(Colors.black),
                              ),
                              Text(
                                ' coupon is applied..!',
                                style: smallFontW600(Colors.green),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Bouncing(
                                  onPress: () {
                                    Get.find<CouponController>().clearValue();
                                    Get.find<ProductCategoryController>()
                                        .getShippingDetails(
                                            amount.toInt().toString());
                                  },
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.red[900],
                                    size: 20,
                                  ))
                            ],
                          )
                        ],
                      )
                    : SizedBox(),
                SizedBox(
                  height: 25,
                ),
                Divider(
                  color: Colors.grey,
                  thickness: 1,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Amount',
                        style: smallFont(Colors.black),
                      ),
                      Text(
                        '₹ ' + amount.toString(),
                        style: smallFontW600(Colors.black),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Delivery Charge',
                        style: smallFont(Colors.black),
                      ),
                      Text(
                        '₹ ' +
                            Get.find<ProductCategoryController>()
                                .buyNowShipping
                                .value
                                .toString(),
                        style: smallFontW600(Colors.black),
                      )
                    ],
                  ),
                ),
                Get.find<CouponController>().isApplied.value
                    ? Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Discount',
                                  style: smallFont(Colors.black),
                                ),
                                Text(
                                  '₹ ' +
                                      Get.find<CouponController>()
                                          .discount
                                          .toString(),
                                  style: smallFontW600(Colors.black),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Amount to Pay',
                                  style: smallFont(Colors.black),
                                ),
                                Text(
                                  '₹ ' +
                                      (Get.find<CouponController>()
                                                  .finalAmount +
                                              Get.find<
                                                      ProductCategoryController>()
                                                  .buyNowShipping
                                                  .value)
                                          .toString(),
                                  style: smallFontW600(Colors.black),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Amount to Pay',
                                  style: smallFont(Colors.black),
                                ),
                                Text(
                                  '₹ ' +
                                      (amount +
                                              Get.find<
                                                      ProductCategoryController>()
                                                  .buyNowShipping
                                                  .value)
                                          .toString(),
                                  style: smallFontW600(Colors.black),
                                )
                              ],
                            ),
                          )
                        ],
                      )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: SizedBox(
          height: 55,
          child: Center(
            child: Bouncing(
              onPress: addressController.addressList.isEmpty
                  ? null
                  : () {
                      Get.find<CouponController>().isApplied.value
                          ? Get.find<CartControllerFile>()
                              .placeOrderWithCoupon(
                                  Get.find<CouponController>().couponName.value)
                              .then((value) => (value)
                                  ? {Get.to(() => SuccessPage())}
                                  : null)
                          : Get.find<CartControllerFile>().placeOrder().then(
                              (value) => (value)
                                  ? {Get.to(() => SuccessPage())}
                                  : null);
                    },
              child: IntrinsicWidth(
                child: Container(
                  height: 40,
                  width: 150,
                  margin: EdgeInsets.only(bottom: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(7)),
                    color: addressController.addressList.isEmpty
                        ? Colors.grey[300]
                        : botAppBarColor,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Proceed',
                      style: mediumFont(Colors.black),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
