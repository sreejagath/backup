import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/address_edit/addressDetailsPage.dart';
import 'package:pexa_customer/view/screens/success/success.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class CarShoppeBuyNow extends StatelessWidget {
  CarShoppeBuyNow(
      {Key key, this.price, this.count, this.offerPrice, this.prodId})
      : super(key: key);
  final String prodId;
  final int price;
  final int offerPrice;
  final int count;
  final addressController = Get.find<AddressControllerFile>();

  @override
  Widget build(BuildContext context) {
    var total = ((offerPrice * count) +
        Get.find<ProductCategoryController>().buyNowShipping.value);
        print(total);
    Get.find<ProductCategoryController>().getShippingDetails(total.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text("Checkout", style: mediumFont(Colors.black)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 15,
          ),
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
      ),
      backgroundColor: Colors.grey.shade100,
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: SingleChildScrollView(
          child: Obx(
            () => Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Bouncing(
                  onPress: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddressDetailsPage(
                                backContext: context,
                              )),
                    );
                    // Get.to(() => AddressDetailsPage(backContext: context,));
                  },
                  child: IntrinsicWidth(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                        color: botAppBarColor,
                        // boxShadow: [
                        //   BoxShadow(
                        //     color: Colors.grey.withOpacity(0.5),
                        //     blurRadius: 7,
                        //     offset: Offset(0, 3),
                        //   ),
                        // ],
                      ),
                      child: Center(
                        child: Text(
                          'Add / Change Address',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                addressController.addressList.isEmpty
                    ? SizedBox(
                        height: 80,
                        child: Center(
                          child: (addressController.isNoAddress.value)
                              ? Text(
                                  'No Saved Address',
                                  style: mediumFont(Colors.black),
                                )
                              : SizedBox(
                                  height: 35,
                                  width: 35,
                                  child: Image.asset(Images.spinner,
                                      fit: BoxFit.fill)),
                        ),
                      )
                    : Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          //borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: Colors.white,
                          // boxShadow: [
                          //   BoxShadow(
                          //     color: Colors.grey.withOpacity(0.5),
                          //     blurRadius: 7,
                          //     offset:
                          //         Offset(0, 3), // changes position of shadow
                          //   ),
                          // ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Deliver to:'),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  addressController.defaultAddress['name'],
                                  style: mediumFont(Colors.black),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 0.1, horizontal: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Text(
                                      addressController.defaultAddress['type'],
                                      style: mediumFont(Colors.black),
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 2.5,
                            ),
                            Text(
                              addressController.defaultAddress['house'] +
                                  ', ' +
                                  addressController.defaultAddress['street'] +
                                  ', ' +
                                  addressController.defaultAddress['pincode']
                                      .toString(),
                              style: smallFont(Colors.black),
                            ),
                            SizedBox(
                              height: 2.5,
                            ),
                            Text(
                              addressController.defaultAddress['mobile']
                                  .toString(),
                              style: smallFontW600(Colors.black),
                            ),
                          ],
                        ),
                      ),
                SizedBox(
                  height: 10,
                ),
                // SizedBox(
                //   height: 20,
                // ),
                Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Apply Coupon Code',
                        style: mediumFont(Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 40,
                            width: 250,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: Colors.black)),
                            child: TextField(
                              textAlign: TextAlign.center,
                              controller:
                                  Get.find<CouponController>().controller.value,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter the coupon',
                                  hintStyle: mediumFont(Colors.grey)),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Bouncing(
                            onPress: () {
                              if (Get.find<CouponController>()
                                          .controller
                                          .value
                                          .text
                                          .trim() !=
                                      null ||
                                  Get.find<CouponController>()
                                          .controller
                                          .value
                                          .text
                                          .trim() !=
                                      '') {
                                Get.find<CouponController>().checkCoupon(
                                    double.parse(offerPrice.toString()) *
                                        count);
                              }
                            },
                            child: Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: botAppBarColor,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.verified_outlined,
                                  color: Colors.black,
                                  size: 15,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Get.find<CouponController>().showDetails.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'This Coupon will be applicable only for the minimum purchase \n'
                                  'amount of ${Get.find<CouponController>().minAmount.value}',
                                  style: smallFontW600(Colors.red),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            )
                          : SizedBox(),
                      Get.find<CouponController>().isApplied.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      (Get.find<CouponController>()
                                          .couponName
                                          .value),
                                      style: mediumFont(Colors.black),
                                    ),
                                    Text(
                                      ' Coupon is applied successfully!',
                                      style: smallFontW600(Colors.green),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Bouncing(
                                        onPress: () {
                                          Get.find<CouponController>()
                                              .clearValue();
                                          Get.find<ProductCategoryController>()
                                              .getShippingDetails(
                                                  (price * count)
                                                      .toInt()
                                                      .toString());
                                        },
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.red[900],
                                          size: 20,
                                        ))
                                  ],
                                )
                              ],
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    //borderRadius: BorderRadius.all(Radius.circular(7)),
                    color: Colors.white,
                    // boxShadow: [
                    //   BoxShadow(
                    //     color: Colors.grey.withOpacity(0.5),
                    //     blurRadius: 7,
                    //     offset: Offset(0, 3), // changes position of shadow
                    //   ),
                    // ],
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Price Details",
                            style: mediumFont(Colors.black),
                          ),
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Price ( ${count.toString()} items )",
                            style: smallFont(Colors.grey),
                          ),
                          Text(
                            (price * count).toString(),
                            style: smallFontW600(Colors.black),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Discount",
                            style: smallFont(Colors.grey),
                          ),
                          Text(
                              ((price * count) - (offerPrice * count))
                                  .toString(),
                              style: smallFontW600(((price * count) - (offerPrice * count))>0?Colors.green: Colors.black))
                        ],
                      ),
                      SizedBox(
                        height: Get.find<CouponController>().isApplied.value
                            ? 5
                            : 0,
                      ),
                      Get.find<CouponController>().isApplied.value
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Coupon Discount",
                                  style: smallFont(Colors.grey),
                                ),
                                Text(
                                    Get.find<CouponController>()
                                        .discount
                                        .toString(),
                                    style: smallFontW600(Colors.black))
                              ],
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Delivery Charge",
                              style: smallFont(Colors.grey)),
                          Text(
                              Get.find<ProductCategoryController>()
                                  .buyNowShipping
                                  .value
                                  .toString(),
                              style: smallFontW600(Colors.black))
                        ],
                      ),
                      Divider(
                        color: Colors.grey,
                        thickness: 1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Total Amount",
                            style: mediumFont(Colors.black),
                          ),
                          Get.find<CouponController>().isApplied.value
                              ? Text(
                                  Get.find<CouponController>()
                                      .finalAmount
                                      .value
                                      .toString(),
                                  style: mediumFont(Colors.black),
                                )
                              : Text(
                                  ((offerPrice * count) +
                                          Get.find<ProductCategoryController>()
                                              .buyNowShipping
                                              .value)
                                      .toString(),
                                  style: mediumFont(Colors.black),
                                )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Bouncing(
                  onPress: Get.find<CouponController>().isApplied.value
                      ? () {
                          Map<String, dynamic> body = {
                            "product": prodId,
                            "count": count,
                            "couponCode":
                                Get.find<CouponController>().couponName.value
                          };
                          Get.find<ProductCategoryController>()
                              .buyNowProduct(body)
                              .then((value) => (value)
                                  ? {Get.to(() => SuccessPage())}
                                  : null);
                        }
                      : () {
                          Map<String, dynamic> body = {
                            "product": prodId,
                            "count": count
                          };
                          Get.find<ProductCategoryController>()
                              .buyNowProduct(body)
                              .then((value) => (value)
                                  ? {Get.to(() => SuccessPage())}
                                  : null);
                        },
                  child: Container(
                    height: 40,
                    width: 250,
                    margin: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      color: addressController.addressList.isEmpty
                          ? Colors.grey[300]
                          : botAppBarColor,
                      // boxShadow: [
                      //   BoxShadow(
                      //     color: Colors.grey.withOpacity(0.5),
                      //     blurRadius: 7,
                      //     offset: Offset(0, 3), // changes position of shadow
                      //   ),
                      // ],
                    ),
                    child: Center(
                      child: Text(
                        'Proceed',
                        style: mediumFont(Colors.black),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
