import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/tab_controller.dart';
import 'package:pexa_customer/view/screens/carShoppee/widget/categoryTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarShoppe extends StatelessWidget {
  const CarShoppe({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final categoryModelController = Get.find<ProductCategoryController>();
    final control = Get.find<TabControllerMethod>();
    return Obx(
      () => SliverPadding(
        padding: EdgeInsets.all(10),
        sliver: SliverGrid(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 1.0,
          ),
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.3,
              child: CategoryTile(
                categoryResultData: categoryModelController.categoryList[index],
                index: index,
              ),
            );
          },
                  childCount: categoryModelController.categoryList.length != 0
                      ? control.tabIndex.value == 6
                          ? categoryModelController.categoryList.length > 9
                              ? 9
                              : categoryModelController.categoryList.length
                          : categoryModelController.categoryList.length
                      : 0),
        ),
      ),
    );
  }
}
