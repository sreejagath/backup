import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/models/carShoppe/category_Model.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/subcategory/subcategory.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryTile extends StatelessWidget {
  CategoryTile({Key key, this.categoryResultData, this.index})
      : super(key: key);
  final CategoryResultData categoryResultData;
  final int index;
  final categoryModelController = Get.find<ProductCategoryController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        
        categoryModelController.fetchSubCategoryData(categoryResultData.id);
        Get.to(() => SubCategory(title: categoryResultData.name));
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomImage(
            image: categoryResultData.thumbUrl[0],
            height: MediaQuery.of(context).size.width * 0.2,
            width: MediaQuery.of(context).size.width * 0.2,
            fit: BoxFit.cover,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            categoryResultData.name,
            style: verySmallFontW600(Colors.black),
          )
        ],
      ),
    );
  }

  // fileSize(value) async {
  //   
  //   http.Response r = await http.head(Uri.parse(value));
  //   
  // }
}
