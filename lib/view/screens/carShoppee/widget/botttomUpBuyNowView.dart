// import 'package:pexa_customer/constants/color.dart';
// import 'package:pexa_customer/constants/new%20fonts.dart';
// import 'package:pexa_customer/controller/myController/addressController.dart';
// import 'package:pexa_customer/controller/myController/caegoryController.dart';
// import 'package:pexa_customer/util/images.dart';
// import 'package:pexa_customer/view/screens/address_edit/addressDetailsPage.dart';
// import 'package:pexa_customer/view/screens/success/success.dart';
// import 'package:pexa_customer/widgets/bouncing.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// Future<dynamic> carShoppeBuyNowAddressBottumUp(
//     BuildContext context, String prodId, int price, int offerPrice, int count) {
//   final addressController = Get.find<AddressControllerFile>();
//   return showModalBottomSheet(
//       clipBehavior: Clip.antiAlias,
//       isScrollControlled: true,
//       backgroundColor: Colors.transparent,
//       context: context,
//       builder: (BuildContext context) {
//         return IntrinsicHeight(
//           child: Container(
//             // height: MediaQuery.of(context).size.height * .40,
//             padding: EdgeInsets.symmetric(horizontal: 10),
//             decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.only(
//                   topLeft: Radius.circular(20),
//                   topRight: Radius.circular(20),
//                 )),
//             child: Column(
//               children: [
//                 SizedBox(
//                   height: 15,
//                 ),
//                 Bouncing(
//                   onPress: () {
//                     Navigator.pop(context);
//                     Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (context) => AddressDetailsPage(
//                                 backContext: context,
//                               )),
//                     );
//                     // Get.to(() => AddressDetailsPage(backContext: context,));
//                   },
//                   child: IntrinsicWidth(
//                     child: Container(
//                       padding:
//                           EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.all(Radius.circular(7)),
//                         color: botAppBarColor,
//                         boxShadow: [
//                           BoxShadow(
//                             color: Colors.grey.withOpacity(0.5),
//                             blurRadius: 7,
//                             offset: Offset(0, 3),
//                           ),
//                         ],
//                       ),
//                       child: Center(
//                         child: Text(
//                           'Add / Change Address',
//                           style: mediumFont(Colors.black),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 addressController.addressList.isEmpty
//                     ? SizedBox(
//                         height: 80,
//                         child: Center(
//                           child: (addressController.isNoAddress.value)
//                               ? Text(
//                                   'No Saved Address',
//                                   style: mediumFont(Colors.black),
//                                 )
//                               : SizedBox(
//                                   height: 35,
//                                   width: 35,
//                                   child: Image.asset(Images.loading,
//                                       fit: BoxFit.fill)),
//                         ),
//                       )
//                     : Container(
//                         padding: EdgeInsets.all(15),
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.all(Radius.circular(7)),
//                           color: Colors.white,
//                           boxShadow: [
//                             BoxShadow(
//                               color: Colors.grey.withOpacity(0.5),
//                               blurRadius: 7,
//                               offset:
//                                   Offset(0, 3), // changes position of shadow
//                             ),
//                           ],
//                         ),
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Text(
//                                   addressController.defaultAddress['name'],
//                                   style: mediumFont(Colors.black),
//                                 ),
//                                 SizedBox(
//                                   width: 10,
//                                 ),
//                                 Container(
//                                     padding: EdgeInsets.symmetric(
//                                         vertical: 0.1, horizontal: 5),
//                                     decoration: BoxDecoration(
//                                         color: Colors.grey[200],
//                                         borderRadius: BorderRadius.all(
//                                             Radius.circular(5))),
//                                     child: Text(
//                                       addressController.defaultAddress['type'],
//                                       style: mediumFont(Colors.black),
//                                     )),
//                               ],
//                             ),
//                             SizedBox(
//                               height: 2.5,
//                             ),
//                             Text(
//                               addressController.defaultAddress['house'] +
//                                   ', ' +
//                                   addressController.defaultAddress['street'] +
//                                   ', ' +
//                                   addressController.defaultAddress['pincode']
//                                       .toString(),
//                               style: smallFont(Colors.black),
//                             ),
//                             SizedBox(
//                               height: 2.5,
//                             ),
//                             Text(
//                               addressController.defaultAddress['mobile']
//                                   .toString(),
//                               style: smallFontW600(Colors.black),
//                             ),
//                           ],
//                         ),
//                       ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Container(
//                   padding: EdgeInsets.all(15),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.all(Radius.circular(7)),
//                     color: Colors.white,
//                     boxShadow: [
//                       BoxShadow(
//                         color: Colors.grey.withOpacity(0.5),
//                         blurRadius: 7,
//                         offset: Offset(0, 3), // changes position of shadow
//                       ),
//                     ],
//                   ),
//                   child: Column(
//                     children: [
//                       SizedBox(
//                         width: MediaQuery.of(context).size.width,
//                         child: Align(
//                           alignment: Alignment.centerLeft,
//                           child: Text(
//                             "Price Details",
//                             style: mediumFont(Colors.black),
//                           ),
//                         ),
//                       ),
//                       Divider(
//                         color: Colors.grey,
//                         thickness: 1,
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Text(
//                             "Price ( ${count.toString()} items )",
//                             style: smallFont(Colors.grey),
//                           ),
//                           Text(
//                             (price * count).toString(),
//                             style: smallFontW600(Colors.black),
//                           )
//                         ],
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Text(
//                             "Discount",
//                             style: smallFont(Colors.grey),
//                           ),
//                           Text(
//                               ((price * count) - (offerPrice * count))
//                                   .toString(),
//                               style: smallFontW600(Colors.black))
//                         ],
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Text("Delivery Charge",
//                               style: smallFont(Colors.grey)),
//                           Text(
//                               Get.find<ProductCategoryController>()
//                                   .buyNowShipping
//                                   .value
//                                   .toString(),
//                               style: smallFontW600(Colors.black))
//                         ],
//                       ),
//                       Divider(
//                         color: Colors.grey,
//                         thickness: 1,
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Text(
//                             "Total Amount",
//                             style: mediumFont(Colors.black),
//                           ),
//                           Text(
//                             ((offerPrice * count) +
//                                     Get.find<ProductCategoryController>()
//                                         .buyNowShipping
//                                         .value)
//                                 .toString(),
//                             style: mediumFont(Colors.black),
//                           )
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 Bouncing(
//                   onPress: () {
//                     Get.find<ProductCategoryController>()
//                         .buyNowProduct(prodId, count)
//                         .then((value) =>
//                             (value) ? {Get.to(() => SuccessPage())} : null);
//                   },
//                   child: Container(
//                     height: 40,
//                     width: 150,
//                     margin: EdgeInsets.only(bottom: 15),
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.all(Radius.circular(7)),
//                       color: addressController.addressList.isEmpty
//                           ? Colors.grey[300]
//                           : botAppBarColor,
//                       boxShadow: [
//                         BoxShadow(
//                           color: Colors.grey.withOpacity(0.5),
//                           blurRadius: 7,
//                           offset: Offset(0, 3), // changes position of shadow
//                         ),
//                       ],
//                     ),
//                     child: Center(
//                       child: Text(
//                         'Proceed',
//                         style: mediumFont(Colors.black),
//                       ),
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           ),
//         );
//       });
// }
