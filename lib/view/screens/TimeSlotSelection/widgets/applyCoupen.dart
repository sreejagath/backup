import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';

class ApplyCoupenTimeSlot extends StatelessWidget {
  ApplyCoupenTimeSlot({Key key, this.route, this.carSpaServiceResultData})
      : super(key: key);
  final String route;
  final CarSpaServiceResultData carSpaServiceResultData;

  Future goBack(BuildContext context) async {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 45,
              width: 220,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Colors.white,
                  border: Border.all(color: Colors.black)),
              child: TextField(
                textAlign: TextAlign.center,
                controller: Get.find<CouponController>().controller.value,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Enter the coupon',
                    hintStyle: smallFont(Colors.grey)),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Bouncing(
              onPress: () {
                if (Get.find<CouponController>().controller.value.text.trim() !=
                        null &&
                    Get.find<CouponController>().controller.value.text.trim() !=
                        '') {
                  (route == 'carSpa')
                      ? goBack(context).then((value) =>
                          Get.find<CouponController>().checkCoupon(
                              Get.find<CarSpaController>()
                                  .carSpaAddOnTotal
                                  .value
                                  .toDouble()))
                      : goBack(context).then((value) =>
                          Get.find<CouponController>().checkCoupon(
                              Get.find<MechanicalController>()
                                  .mechanicalAddOnTotal
                                  .value
                                  .toDouble()));
                }
              },
              child: Container(
                height: 35,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: botAppBarColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.verified_outlined,
                      color: Colors.black,
                      size: 15,
                    ),
                    Text(
                      ' Verify',
                      style: smallFontW600(Colors.black),
                    )
                  ],
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
