import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/newSearchController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/search/widget/searchItemTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class NewSearchScreen extends StatefulWidget {
  NewSearchScreen({Key key}) : super(key: key);

  @override
  State<NewSearchScreen> createState() => _NewSearchScreenState();
}

class _NewSearchScreenState extends State<NewSearchScreen> {
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Get.find<PexaSearchController>().searchResultList.clear();
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Center(
                  child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Row(children: [
                        Expanded(
                          child: Container(
                            height: 45,
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                border: Border.all(color: Colors.black)),
                            child: TextField(
                              controller: searchController,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  suffixIcon: Bouncing(
                                    onPress: () {
                                      Get.find<PexaSearchController>()
                                          .searchProducts(
                                              searchController.text.trim());
                                    },
                                    child: Icon(
                                      Icons.search,
                                      size: 20,
                                    ),
                                  )),
                              onSubmitted: (value) {
                                Get.find<PexaSearchController>()
                                    .searchProducts(value);
                              },
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Text(
                            "Cancel",
                            style: mediumFont(Colors.black),
                          ),
                        )
                      ]))),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: Obx(
                  () =>
                      Get.find<PexaSearchController>().searchResultList.isEmpty
                          ? Get.find<PexaSearchController>().isLoad.value
                              ? Center(
                                  child: LoadingAnimationWidget.twistingDots(
                                    leftDotColor: const Color(0xFF4B4B4D),
                                    rightDotColor: const Color(0xFFf7d417),
                                    size: 50,
                                  ),
                                )
                              : Center(
                                  child: Text(
                                    "Nothing to display..!",
                                    style: mediumFont(Colors.black),
                                  ),
                                )
                          : GridView.builder(
                              // physics: NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 1.0,
                              ),
                              itemCount: Get.find<PexaSearchController>()
                                  .searchResultList
                                  .length,
                              itemBuilder: (context, index) {
                                return SizedBox(
                                  child: SearchItemTile(
                                      searchResultData:
                                          Get.find<PexaSearchController>()
                                              .searchResultList[index]),
                                );
                              },
                            ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
