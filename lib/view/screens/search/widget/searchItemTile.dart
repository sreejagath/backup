import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/models/carShoppe/searchModel.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/products/productDetailedView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchItemTile extends StatelessWidget {
  SearchItemTile({Key key, this.searchResultData}) : super(key: key);
  final SearchResultData searchResultData;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        print(searchResultData.id);
        Get.find<ProductCategoryController>()
            .fetchProductDetails(searchResultData.id)
            .then((value) => Get.to(() => ProductDetailedView()));
      },
      child: Card(
        elevation: 2,
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  // Container(
                  //   decoration: BoxDecoration(
                  //     color: Colors.white,
                  //     image: DecorationImage(
                  //         image: NetworkImage(
                  //           searchResultData.thumbUrl[0],
                  //         ),
                  //         fit: BoxFit.contain)
                  //   ),
                  // ),
                  SizedBox(
                    child: CustomImage(
                      image: searchResultData.thumbUrl[0],
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(100),
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text('₹ ' + searchResultData.price.toString(),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: verySmallFontwithLine(Colors.grey)),
                          Text(
                            '₹ ' + searchResultData.offerPrice.toString(),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: smallFont(Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                searchResultData.name.toUpperCase(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: smallFontW600(Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
