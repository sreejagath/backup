import 'package:pexa_customer/ApiServices/loginApi.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/view/base/custom_snackbar.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/otp/otp.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController phoneNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    AuthFactorsController controller = Get.find();
    return Scaffold(
      backgroundColor: blackPrimary,
      body: Obx(() => (Get.find<ConnectivityController>().status.value)
          ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/carSpa/latestlogo.png',
                        height: 180,
                        width: 180,
                      ),
                      SizedBox(
                        height: 80,
                      ),
                      Column(
                        children: [
                          Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                border: Border.all(color: botAppBarColor)),
                            child: TextField(
                              controller: phoneNumberController,
                              style: TextStyle(color: Colors.white),
                              onChanged: (number) {
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.phone,
                                    color: botAppBarColor,
                                  ),
                                  hintText: 'Enter Phone Number',
                                  border: InputBorder.none),
                              keyboardType: TextInputType.phone,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Bouncing(
                            onPress: () {
                              (phoneNumberController.text.length == 0)
                                  ? Get.snackbar(
                                      'Error', 'Please enter your phone number',
                                      backgroundColor: Colors.red[700],
                                      colorText: Colors.white)
                                  : (phoneNumberController.text.length != 10)
                                      ? Get.snackbar('Error',
                                          'Please enter a valid phone number',
                                          backgroundColor: Colors.red[700],
                                          colorText: Colors.white)
                                      : phoneNumberController.text
                                              .toString()
                                              .contains(new RegExp(r'[^0-9]'))
                                          ? Get.snackbar('Error',
                                              'Please enter a valid phone number',
                                              backgroundColor: Colors.red[700],
                                              colorText: Colors.white)
                                          : Authentication()
                                              .login(
                                                  phone: phoneNumberController
                                                      .text
                                                      .toString())
                                              .then((value) {
                                              controller.phoneNumberState
                                                          .value ==
                                                      ''
                                                  ? Get.to(() => OtpScreen(),
                                                      arguments:
                                                          phoneNumberController
                                                              .text
                                                              .toString())
                                                  : customSnackBar(context,
                                                      'Something Went Wrong!');
                                            });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: botAppBarColor,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              height: 40,
                              width: MediaQuery.of(context).size.width * .60,
                              child: Center(
                                child: Text(
                                  'Login',
                                  style: mediumFont(Colors.black),
                                ),
                              ),
                            ),
                          ),
                          // SizedBox(
                          //   height: 50,
                          // ),
                          // GestureDetector(
                          //   onTap: () async {
                          //     Get.find<GuestController>().guestLogin();
                          //   },
                          //   child: Text(
                          //     'Continue as Guest',
                          //     style: mediumFont(Colors.white),
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          : NoInternetScreenView()),
    );
  }
}
