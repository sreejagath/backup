import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class AddressViewPage extends StatefulWidget {
  AddressViewPage({Key key, this.backContext}) : super(key: key);
  final BuildContext backContext;

  @override
  _AddressViewPageState createState() => _AddressViewPageState();
}

class _AddressViewPageState extends State<AddressViewPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController pinCodeController = TextEditingController();
  TextEditingController landMarkController = TextEditingController();
  TextEditingController altNumberController = TextEditingController();
  String type = 'Home-1';
  String mark = "";
  List location = [];
  bool isAlter = false;
  final _formKey = GlobalKey<FormState>();
  final addressController = Get.find<AddressControllerFile>();

  Future goBack(BuildContext context) async {
    Navigator.pop(context);
  }

  void showErrorSnackBar(String message) {
    Get.snackbar('Error', message,
        snackPosition: SnackPosition.TOP,
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
        colorText: Colors.black,
        snackStyle: SnackStyle.FLOATING);
  }

  @override
  void initState() {
    super.initState();
    if(Get.find<AuthFactorsController>().phoneNumber != null || Get.find<AuthFactorsController>().phoneNumber != ''){
      setState(() {
        numberController.text=Get.find<AuthFactorsController>().phoneNumber.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            body: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('ADDRESS', style: largeFont(Colors.black)),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey[300]),
                        child: Center(
                          child: DropdownButton(
                            value: type,
                            style: mediumFont(Colors.black),
                            icon: Icon(Icons.arrow_drop_down),
                            items: ['Home-1', 'Home-2', 'Work', 'Other']
                                .map((String items) {
                              return DropdownMenuItem(
                                value: items,
                                child: Text(items),
                              );
                            }).toList(),
                            onChanged: (String newValue) {
                              setState(() {
                                type = newValue;
                              });
                            },
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        // height: 40,
                        padding: EdgeInsets.symmetric(vertical: .05),
                        width: MediaQuery.of(context).size.width,
                        child: TextFormField(
                          controller: nameController,
                          decoration: const InputDecoration(
                            labelText: 'Name(Required)*',
                            contentPadding: EdgeInsets.all(10),
                            focusColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(width: 1)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter the name';
                            }
                            return null;
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        // height: 40,
                        padding: EdgeInsets.symmetric(vertical: .05),
                        child: TextFormField(
                          controller: numberController,
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            labelText: 'Number(Required)*',
                            contentPadding: EdgeInsets.all(10),
                            focusColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(width: 1)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter the Number';
                            }
                            return null;
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isAlter = !isAlter;
                          });
                        },
                        child: Text(
                          "+ Add Alternative Number",
                          style: smallFontW600(Colors.blue[900]),
                        ),
                      ),
                      isAlter
                          ? const SizedBox(
                              height: 10,
                            )
                          : SizedBox(),
                      isAlter
                          ? Container(
                              // height: 40,
                              padding: EdgeInsets.symmetric(vertical: .05),
                              width: MediaQuery.of(context).size.width,
                              child: TextFormField(
                                controller: altNumberController,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  labelText: 'Alternative Number',
                                  contentPadding: EdgeInsets.all(10),
                                  focusColor: Colors.black,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(width: 1)),
                                ),
                              ),
                            )
                          : SizedBox(),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              // height: 40,
                              padding: EdgeInsets.symmetric(vertical: .05),
                              child: TextFormField(
                                controller: pinCodeController,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  labelText: 'Pincode(Required)*',
                                  contentPadding: EdgeInsets.all(10),
                                  focusColor: Colors.black,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(width: 1)),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter the Pincode';
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            flex: 2,
                            child: GestureDetector(
                              onTap: () async {
                                var position = await GeolocatorPlatform.instance
                                    .getCurrentPosition();
                                List<Placemark> placemarks =
                                    await placemarkFromCoordinates(
                                        position.latitude, position.longitude);
                                Placemark place = placemarks[0];
                                if (place != null) {
                                  setState(() {
                                    pinCodeController.text =
                                        place.postalCode.toString();
                                    stateController.text =
                                        place.administrativeArea.toString();
                                    cityController.text =
                                        place.locality.toString();
                                    streetController.text =
                                        '${place.subLocality}, ' +
                                            '${place.locality.toString()}, ' +
                                            '${place.subAdministrativeArea}, ' +
                                            '${place.administrativeArea.toString()}';
                                  });
                                }
                              },
                              child: Container(
                                // height: 40,
                                padding: EdgeInsets.symmetric(vertical: 2.5),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    color: Colors.black.withOpacity(0.7)),
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.location_on_outlined,
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 2.5,
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            'Use Current',
                                            style: smallFontW600(Colors.white),
                                          ),
                                          Text(
                                            'Location',
                                            style: smallFontW600(Colors.white),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              // height: 40,
                              padding: EdgeInsets.symmetric(vertical: .05),
                              child: TextFormField(
                                controller: stateController,
                                decoration: const InputDecoration(
                                  labelText: 'State(Required)*',
                                  contentPadding: EdgeInsets.all(10),
                                  focusColor: Colors.black,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(width: 1)),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter the State';
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                              // height: 40,
                              padding: EdgeInsets.symmetric(vertical: .05),
                              child: TextFormField(
                                controller: cityController,
                                decoration: const InputDecoration(
                                  labelText: 'City(Required)*',
                                  contentPadding: EdgeInsets.all(10),
                                  focusColor: Colors.black,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.black)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(width: 1)),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter the City';
                                  }
                                  return null;
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        // height: 40,
                        padding: EdgeInsets.symmetric(vertical: .05),
                        width: MediaQuery.of(context).size.width,
                        child: TextFormField(
                          controller: houseController,
                          decoration: const InputDecoration(
                            labelText: 'House No/Build No(Required)*',
                            contentPadding: EdgeInsets.all(10),
                            focusColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(width: 1)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter the House No/Build No';
                            }
                            return null;
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        // height: 40,
                        padding: EdgeInsets.symmetric(vertical: .05),
                        width: MediaQuery.of(context).size.width,
                        child: TextFormField(
                          controller: streetController,
                          decoration: const InputDecoration(
                            labelText: 'Street Address(Required)*',
                            contentPadding: EdgeInsets.all(10),
                            focusColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(width: 1)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter the Street address';
                            }
                            return null;
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        // height: 40,
                        padding: EdgeInsets.symmetric(vertical: .05),
                        width: MediaQuery.of(context).size.width,
                        child: TextFormField(
                          controller: landMarkController,
                          decoration: const InputDecoration(
                            labelText: 'Landmark',
                            contentPadding: EdgeInsets.all(10),
                            focusColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(width: 1, color: Colors.black)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(width: 1)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Bouncing(
                        onPress: () async {
                          if (_formKey.currentState.validate()) {
                            
                            if (numberController.text.toString().length != 10) {
                              showErrorSnackBar(
                                  'Enter the 10 Digits Valid Number...');
                              return;
                            }
                            List<Location> locations =
                                await locationFromAddress(
                                    streetController.text.toString());
                            if (locations.isEmpty) {
                              
                            } else {
                              Map<String, dynamic> body = {};
                              if (landMarkController.text.trim() == null ||
                                  landMarkController.text.trim() == '') {
                                mark = ' ';
                              } else {
                                mark = landMarkController.text.toString();
                              }
                              if (altNumberController.text.trim() == null ||
                                  altNumberController.text.trim() == '') {
                                body = {
                                  "name": nameController.text.toString(),
                                  "mobile": int.parse(
                                      numberController.text.toString()),
                                  "house": houseController.text.toString(),
                                  "street": streetController.text.toString(),
                                  "city": cityController.text.toString(),
                                  "state": stateController.text.toString(),
                                  "pincode": int.parse(
                                      pinCodeController.text.toString()),
                                  "type": type,
                                  "location": [
                                    locations[0].latitude,
                                    locations[0].longitude
                                  ],
                                  "landmark": mark
                                };
                              } else {
                                body = {
                                  "name": nameController.text.toString(),
                                  "mobile": int.parse(
                                      numberController.text.toString()),
                                  "altPhone": int.parse(
                                      altNumberController.text.toString()),
                                  "house": houseController.text.toString(),
                                  "street": streetController.text.toString(),
                                  "city": cityController.text.toString(),
                                  "state": stateController.text.toString(),
                                  "pincode": int.parse(
                                      pinCodeController.text.toString()),
                                  "type": type,
                                  "location": [
                                    locations[0].latitude,
                                    locations[0].longitude
                                  ],
                                  "landmark": mark
                                };
                              }
                              addressController.addAdress(body).then((value) =>
                                  value
                                      ? {
                                          goBack(context).then((value) =>
                                              addressController.getAddress())
                                        }
                                      : null);
                            }
                          }
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: botAppBarColor),
                          child: Center(
                            child: Text(
                              'SAVE',
                              style: mediumFont(Colors.black),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
