import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/address_edit/addressPage.dart';
import 'package:pexa_customer/view/screens/address_edit/widget/addressListTile.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddressDetailsPage extends StatelessWidget {
  AddressDetailsPage({Key key, this.backContext}) : super(key: key);
  final BuildContext backContext;

  findDefaultAddress() {
    Get.find<AddressControllerFile>().getDefaultAddress();
  }

  @override
  Widget build(BuildContext context) {
    final addressController = Get.find<AddressControllerFile>();
    return Obx(() =>
    (Get
        .find<ConnectivityController>()
        .status
        .value)
        ? Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: 15,
          ),
          onPressed: () {
            Navigator.of(context).pop(true);
            Get
                .find<AddressControllerFile>()
                .fromPexaShoppe
                .value
                ? findDefaultAddress()
            // ignore: unnecessary_statements
                : null;
          },
        ),
      ),
      body: Padding(
        padding:
        const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
        child: Column(
          children: [
            Bouncing(
              onPress: () {
                Get.to(() =>
                    AddressViewPage(
                      backContext: backContext,
                    ));
              },
              child: Container(
                height: 40,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: Center(
                    child: Text(
                      '+ Add New Address',
                      style: mediumFont(Colors.black),
                    )),
                decoration: BoxDecoration(
                    // boxShadow: [
                    //   BoxShadow(
                    //     color: Colors.grey.withOpacity(0.5),
                    //     blurRadius: 7,
                    //     offset:
                    //     Offset(0, 3), // changes position of shadow
                    //   ),
                    // ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: botAppBarColor),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Obx(() =>
                Column(
                  children: [
                    addressController.addressList.isNotEmpty
                        ? Text(
                      addressController.addressList.length
                          .toString() +
                          ' saved Addresses',
                      style: mediumFont(Colors.grey),
                    )
                        : SizedBox(),
                    addressController.addressList.isNotEmpty
                        ? SizedBox(
                      height: 10,
                    )
                        : SizedBox(),
                  ],
                )),
            Obx(
                  () =>
                  Expanded(
                      flex: 1,
                      child: addressController.addressList.isEmpty
                          ? Center(
                        child: (addressController.isNoAddress.value)
                            ? Text(
                          'No Saved Addressess',
                          style: mediumFont(Colors.red),
                        )
                            : SizedBox(
                            height: 35,
                            width: 35,
                            child: Image.asset(Images.spinner,
                                fit: BoxFit.fill)),
                      )
                          : ListView.builder(
                          itemCount: addressController.addressList.length,
                          itemBuilder: (context, index) {
                            return Column(
                              children: [
                                AddressListTile(
                                  index: index,
                                  addressListResultData: addressController
                                      .addressList[index],
                                  backContext: backContext,
                                ),
                                SizedBox(
                                  height: 5,
                                )
                              ],
                            );
                          })),
            )
          ],
        ),
      ),
    )
        : Scaffold(
      body: NoInternetScreenView(),
    ));
  }
}
