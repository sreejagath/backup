import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/models/user/addressModel.dart';
import 'package:pexa_customer/view/screens/address_edit/addressEditPage.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddressListTile extends StatelessWidget {
  AddressListTile(
      {Key key, this.addressListResultData, this.index, this.backContext})
      : super(key: key);
  final AddressListResultData addressListResultData;
  final int index;
  final BuildContext backContext;

  @override
  Widget build(BuildContext context) {
    final addressController = Get.find<AddressControllerFile>();
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 1,
            offset: Offset(4, 4), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * .50,
                  child: Text(
                    addressListResultData.name,
                    style: mediumFont(Colors.black),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Expanded(
                  child: SizedBox(
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: (addressListResultData.isDefault != null &&
                            addressListResultData.isDefault)
                            ? Container(
                            padding: EdgeInsets.symmetric(vertical: 2.5),
                            width: 100,
                            decoration: BoxDecoration(
                                color: botAppBarColor,
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: Colors.grey.withOpacity(0.5),
                                //     blurRadius: 7,
                                //     offset: Offset(
                                //         0, 3), // changes position of shadow
                                //   ),
                                // ],
                                borderRadius:
                                BorderRadius.all(Radius.circular(5))),
                            child: Center(
                                child: Text(
                                  'Default',
                                  style: mediumFont(Colors.black),
                                )))
                            : Bouncing(
                          onPress: () {
                            print(addressListResultData.id);
                            addressController
                                .setDefaultAddress(
                                addressListResultData.id)
                                .then((value) =>
                                addressController.getAddress());
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 2.5),
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                                borderRadius:
                                BorderRadius.all(Radius.circular(5))),
                            child: Center(
                              child: Text('Set Default',
                                style: mediumFont(Colors.black),),
                            ),
                          ),
                        )),
                  ),
                ),
                SizedBox(
                  width: 5,
                )
              ],
            ),
          ),
           SizedBox(
            height:10,
          ),
          SizedBox(
            width: 10,
          ),
          Container(
              padding: EdgeInsets.symmetric(vertical: 1, horizontal: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              child: Text(addressListResultData.type,
                style: mediumFont(Colors.black),)),
          SizedBox(
            height:15,
          ),
          SizedBox(
            width: MediaQuery
                .of(context)
                .size
                .width * .65,
            child: Text(
              addressListResultData.house +
                  ', ' +
                  addressListResultData.street +
                  ', ' +
                  addressListResultData.pincode.toString(),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: mediumFont(Colors.black),
            ),
          ),
          SizedBox(
            height: 2.5,
          ),
          Text(
            addressListResultData.mobile.toString(),
            style: mediumFont(Colors.black),
          ),
          SizedBox(
            height: 2.5,
          ),
          SizedBox(
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() =>
                          AddressEditView(
                            addressListResultData: addressListResultData,
                            backContext: backContext,
                          ));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 2.5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          // boxShadow: [
                          //   BoxShadow(
                          //     color: Colors.grey.withOpacity(0.5),
                          //     blurRadius: 7,
                          //     offset: Offset(
                          //         0, 3), // changes position of shadow
                          //   ),
                          // ],
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.edit,
                            size: 15,
                            color: Colors.blue[900],
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Edit',
                            style: mediumFont(Colors.blue[900]),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  width: 1,
                  height: 30,
                  color: Colors.grey[300],
                ),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      (addressListResultData.isDefault != null &&
                          addressListResultData.isDefault)
                          ? Get.snackbar(
                          'Error', 'Default address can not be deleted...!',
                          snackPosition: SnackPosition.TOP,
                          duration: Duration(seconds: 1),
                          backgroundColor: Colors.red[900],
                          colorText: Colors.white,
                          snackStyle: SnackStyle.FLOATING)
                          : addressController
                          .deleteAddress(addressListResultData.id);
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 2.5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // boxShadow: [
                            //   BoxShadow(
                            //     color: Colors.grey.withOpacity(0.5),
                            //     blurRadius: 7,
                            //     offset: Offset(
                            //         0, 3), // changes position of shadow
                            //   ),
                            // ],
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.delete,
                              size: 15,
                              color: Colors.red[900],
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                                'Delete',
                                style: mediumFont(Colors.red[900])
                            )
                          ],
                        )),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
