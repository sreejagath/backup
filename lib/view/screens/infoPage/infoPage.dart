import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/view/screens/infoPage/widgets/pnp.dart';
import 'package:pexa_customer/view/screens/infoPage/widgets/tnc.dart';
import 'package:pexa_customer/view/screens/infoPage/widgets/aboutUs.dart';
import 'package:flutter/material.dart';

class InformationPage extends StatelessWidget {
  InformationPage({Key key,@required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title,
            style: mediumFont(Colors.black),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios,size: 15,),
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
      ),
      body: title=='About Us'?
      AboutUs()
      :title=='Privacy and Policy'?
      PrivacyAndPolicy()
      :title=='Terms and Conditions'?
      TnC()
      :SizedBox()
    );
  }
}
