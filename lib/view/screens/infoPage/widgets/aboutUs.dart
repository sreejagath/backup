import 'package:carousel_slider/carousel_slider.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';

class AboutUs extends StatelessWidget {
  AboutUs({Key key}) : super(key: key);
  final urlImages = [
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner.jpg?alt=media&token=d8b1f633-f6ad-4be9-ad20-ff66e05c00a9',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner1.jpg?alt=media&token=50899398-3301-4866-96ef-45ba81634de9',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner2.jpg?alt=media&token=03f3fa83-f586-4172-845e-f95f2bb58189',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner3.jpg?alt=media&token=72fa24b0-044a-4d7b-9c0d-c4c7be0fd4aa',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner4.jpg?alt=media&token=a1bb8c8c-f2bf-4f02-8de4-5468f86c708a'
  ];

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Container(
      width: _width,
      height: _height,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: _width,
                  child: Center(
                    child: Image.asset(
                      "assets/carSpa/latestlogo.png",
                      height: 100,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          We are dedicated to providing quality service ensuring customer satisfaction to the core. "
                  "Our professional services are of great value in multiple locations offering convenient hours.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),

                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          Carclenx movable auto spa was the creative idea of a sparkling, young, and dynamic business personality,"
                  " Mr. Syed Ali & shafi back in 2016. He was backed up with a team of experts who had more than 10 years of "
                  "professional car wash experience. Business foresight blended with industry knowledge, we are beaming with "
                  "pride today as the most sought service providers in the market. Our goal is to provide our customers with "
                  "the friendliest, most convenient hand car wash experience possible. We are the first to provide both steam "
                  "washing and foam washing services for cars in a single package at your doorstep! Feel the transformation of "
                  "your vehicle from our dual washing services in a single touch.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),

                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          We employ only trained and skilled professional cleaning team with good experience. We make use of international "
                  "standard software facilities to provide the best out of your payment.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),

                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Our Mission",
                  style: largeFont(Colors.grey),

                ),
                SizedBox(height: 10,),
                Container(
                  width: _width,
                  height: 200,
                  child: CarouselSlider.builder(
                    options: CarouselOptions(
                      autoPlay: true,
                      viewportFraction: 1.0,
                      enlargeCenterPage: true,
                      height: 200,
                      disableCenter: true,
                      autoPlayInterval: Duration(seconds: 6),
                      onPageChanged: (index, reason) {},
                    ),
                    itemCount: urlImages.length,
                    itemBuilder: (context, index, _) {
                      return InkWell(
                          onTap: () {},
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).cardColor,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey[Get.isDarkMode ? 800 : 200],
                                    spreadRadius: 1,
                                    blurRadius: 5)
                              ],
                            ),
                            child: ClipRRect(
                              child: Image.network(urlImages[index],fit: BoxFit.fill,),
                            ),
                          ));
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          We are dedicated to providing quality service, customer satisfaction at a great value in multiple locations offering"
                  " convenient hours. We can deliver the best results.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),

                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          Our mission is to full fill customer service all over india and provide franchisees to those who hire at the service station,"
                  "mobile wash sectors etc.What we create through our business is we provide our business rights to those who have own service stations "
                  "because its is a new type of business innovation to their existing venture.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),

                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
