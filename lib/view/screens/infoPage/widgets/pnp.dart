import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:flutter/material.dart';

class PrivacyAndPolicy extends StatelessWidget {
  PrivacyAndPolicy({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Container(
      height: _height,
      width: _width,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: _width,
                  child: Center(
                    child: Image.asset(
                      "assets/carSpa/latestlogo.png",
                      height: 100,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          This statement of privacy (“Privacy Policy”) describes how Carclenx Pvt Ltd (hereinafter referred "
                  "to as “we”, “us” “our”) collect, use, and disclose information pertaining to you- the user (hereinafter "
                  "referred to as “you”, “your”) obtained via this website cloud.carclenx.com (“Website”).",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),
                  
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          Please read the following to learn about our practice of secure collection, use, disclosure and "
                  "dissemination of information practices.",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),
                  
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "          By visiting this Website you agree to be bound by the terms and conditions of this Privacy Policy. "
                  "If you do not agree, please do not use or access our Website. This Privacy Policy is incorporated into "
                  "and subject to our Terms of Use (“Terms of Use”).",
                  textAlign: TextAlign.justify,
                  style: smallFont(Colors.grey),
                  
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
