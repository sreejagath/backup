import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_place/google_place.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class LocationSearchPage extends StatefulWidget {
  const LocationSearchPage({Key key}) : super(key: key);

  @override
  State<LocationSearchPage> createState() => _LocationSearchPageState();
}

TextEditingController searchController = TextEditingController();

class _LocationSearchPageState extends State<LocationSearchPage> {
  GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  bool noValue = true;

  @override
  void initState() {
    super.initState();
    googlePlace = GooglePlace("AIzaSyCcT9L1qGXL7RE-UP7qML3_U8bLRgUahyw");
    initialSet();
  }

  initialSet() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        searchController.clear();
        predictions.clear();
      });
    });
  }

  void autoCompleteSearch(String value) async {
    if (value.length > 0) {
      var result = await googlePlace.autocomplete.get(value);
      if (result != null && result.predictions != null && mounted) {
        setState(() {
          predictions = result.predictions;
        });
      }
    } else {
      setState(() {
        predictions.clear();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Search Location",
            style: mediumFont(Colors.black),
          ),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Theme.of(context).textTheme.bodyText1.color,
              onPressed: () => Get.offAllNamed('/')),
          backgroundColor: Theme.of(context).cardColor,
          elevation: 0,
        ),
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Obx(
            () => Get.find<CurrentLocationController>().isLoading.value
                ? Center(
                    child: Image.asset(
                      'assets/carSpa/loading.gif',
                      height: 50,
                    ),
                  )
                : Column(
                    children: [
                      Center(
                          child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Row(children: [
                                Expanded(
                                  child: Container(
                                    height: 45,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5),
                                    margin: EdgeInsets.symmetric(horizontal: 5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        border:
                                            Border.all(color: Colors.black)),
                                    child: TextField(
                                      controller: searchController,
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          suffixIcon: Icon(
                                            Icons.search,
                                            size: 20,
                                          )),
                                      onChanged: (val) {
                                        autoCompleteSearch(val);
                                        if (val.length != 0) {
                                          
                                          setState(() {
                                            noValue = false;
                                          });
                                        } else {
                                          
                                          initialSet();
                                          setState(() {
                                            // predictions.clear();
                                            noValue = true;
                                          });
                                        }
                                      },
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () => Get.offAllNamed('/'),
                                  child: Text(
                                    "Cancel",
                                    style: mediumFont(Colors.black),
                                  ),
                                )
                              ]))),
                      SizedBox(
                        height: 10,
                      ),
                      noValue
                          ? SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: InkWell(
                                onTap: () {
                                  Get.find<CurrentLocationController>()
                                      .userChanged
                                      .value = false;
                                  Get.find<ProductCategoryController>()
                                      .userChanged
                                      .value = false;
                                  Get.find<CurrentLocationController>()
                                      .getUserLocation()
                                      .then((value) => Get.offAllNamed('/'));
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.location_searching_sharp,
                                            size: 25,
                                            color: botAppBarColor,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Current Location',
                                                style: largeFont(Colors.black),
                                              ),
                                              Text(
                                                'Using GPS',
                                                style: mediumFont(Colors.grey),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                      Divider(
                                        color: Colors.grey,
                                        thickness: 1,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      SizedBox(height: noValue ? 10 : 0),
                      !noValue
                          ? Expanded(
                              child: SingleChildScrollView(
                              child: Padding(
                                  padding: EdgeInsets.only(top: 10),
                                  child: ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      physics: NeverScrollableScrollPhysics(),
                                      padding: EdgeInsets.only(top: 0),
                                      shrinkWrap: true,
                                      itemCount: predictions.length,
                                      itemBuilder: (context, index) {
                                        return ListTile(
                                          title: Text(
                                            predictions[index].description,
                                            style: mediumFont(Colors.black),
                                          ),
                                          onTap: () => Get.find<
                                                  CurrentLocationController>()
                                              .getCoordinatesFromAddress(
                                                  predictions[index]
                                                      .description)
                                              .then((value) =>
                                                  Get.offAllNamed('/')),
                                        );
                                      })),
                            ))
                          : Get.find<CurrentLocationController>()
                                  .recentSearchList
                                  .isNotEmpty
                              ? Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Recent Searches',
                                            style: largeFont(Colors.grey),
                                          ),
                                          Bouncing(
                                            onPress: () => Get.find<
                                                    CurrentLocationController>()
                                                .clearRecentSearch(),
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.red[900],
                                            ),
                                          )
                                        ],
                                      ),
                                      Divider(
                                        color: Colors.grey[200],
                                        thickness: 1,
                                      )
                                    ],
                                  ))
                              : SizedBox(),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  padding: EdgeInsets.only(top: 0),
                                  shrinkWrap: true,
                                  itemCount:
                                      Get.find<CurrentLocationController>()
                                          .recentSearchList
                                          .length,
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      title: Text(
                                        Get.find<CurrentLocationController>()
                                            .recentSearchList[index],
                                        style: mediumFont(Colors.black),
                                      ),
                                      onTap: () => Get.find<
                                              CurrentLocationController>()
                                          .getCoordinatesFromAddress(Get.find<
                                                  CurrentLocationController>()
                                              .recentSearchList[index])
                                          .then(
                                              (value) => Get.offAllNamed('/')),
                                    );
                                  })),
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
