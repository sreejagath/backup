import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/view/screens/carSpa/carSpaOfferPage.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class ApplyOfferButton extends StatelessWidget {
  ApplyOfferButton({Key key, this.carSpaServiceResultData}) : super(key: key);
  final CarSpaServiceResultData carSpaServiceResultData;

  @override
  Widget build(BuildContext context) {
    return Bouncing(
      onPress: () {
        Get.to(() => CarSpaOfferPage(
              carSpaServiceResultData: carSpaServiceResultData,
            ));
        Get.find<CarSpaController>().getCarSpaAvailableOffers(
            carSpaServiceResultData.id,
            Get.find<CarSpaController>().carSpaAddOnTotal.value);
        Get.find<CouponController>().controller.value.text = '';
      },
      child: Container(
        alignment: Alignment.center,
        height: 40,
        // width: 150,
        decoration: BoxDecoration(
          color: botAppBarColor,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: Text(
            'Apply Offer',
            style: mediumFont(Colors.black),
          ),
        ),
      ),
    );
  }
}
