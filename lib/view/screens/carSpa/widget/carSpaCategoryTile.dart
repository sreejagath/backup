import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/models/carSpa/carSpaModel.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/carSpa/carSpaServices.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaCategoryTile extends StatelessWidget {
  CarSpaCategoryTile({Key key, this.carSpaResultData, this.index})
      : super(key: key);
  final CarSpaResultData carSpaResultData;
  final int index;
  final carSpaController = Get.find<CarSpaController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          
          
          if (index != 7 && index != 8) {
            carSpaController
                .getCarSpaServiceWithCatId(carSpaResultData.id)
                .then((value) => Get.to(() => CarSpaServices(
                      title: carSpaResultData.name,
                      index: index,
                    )));
          } else {
            carSpaController
                .getCarSpaServiceWithoutCatId(carSpaResultData.id)
                .then((value) => Get.to(() => CarSpaServices(
                      title: carSpaResultData.name,
                      index: index,
                    )));
          }
        },
        child: Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                Container(
                  height: 75,
                  width: 75,
                  clipBehavior: Clip.hardEdge,
                  margin: EdgeInsets.only(top: 3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: CustomImage(
                    image: carSpaResultData.thumbUrl[0],
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(height: 10),
                Flexible(
                  child: SizedBox(
                    width: 90,
                    child: Text(carSpaResultData.name,
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: verySmallFontW600(Colors.black)),
                  ),
                ),
              ],
            )));
  }
}
