import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/view/screens/carSpa/carSpaAddOnView.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<dynamic> carSpaBottomUpView(BuildContext context, int index, List data,
    CarSpaServiceResultData carSpaServiceResultData) {
  return showModalBottomSheet(
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery
              .of(context)
              .size
              .height * .85,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              )),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              SizedBox(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Pexa',
                            style: verySmallFontW600(Colors.black),
                          ),
                          SizedBox(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width * .75,
                            child: Text(
                              carSpaServiceResultData.name,
                              style: largeFont(Colors.black),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          )
                        ],
                      ),
                      IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(Icons.clear))
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              SizedBox(
                width: MediaQuery
                    .of(context)
                    .size
                    .width * .95,
                child: Text(
                  'Pexa ${carSpaServiceResultData
                      .name} consists of the services listed below....',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: smallFontW600(Colors.black),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: ListView.builder(
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(vertical: 5),
                            child: Row(
                              children: [
                                Container(
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                      color: botAppBarColor),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(data[index],
                                  style: smallFontW600(Colors.black),)
                              ],
                            ),
                          );
                        }),
                  )),
              Container(
                height: 100,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "total",
                              style: smallFontW600(Colors.black),
                            ),
                            Row(
                              children: [
                                Text(
                                  "₹ ${carSpaServiceResultData.price
                                      .toString()}",
                                  style: largeFont(Colors.black),
                                ),
                                Text(
                                  " (inc. tax)",
                                  style: smallFont(Colors.black),
                                ),
                              ],
                            )
                          ],
                        ),
                        Bouncing(
                          onPress: () {
                            Navigator.pop(context);
                            Get
                                .find<CarSpaController>()
                                .carSpaAddOns
                                .clear();
                            Get.to(() =>
                                CarSpaServiceAddOnView(
                                  carSpaServiceResultData: carSpaServiceResultData,
                                ));
                          },
                          child: Container(
                            height: 40,
                            width: 200,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(7)),
                              color: botAppBarColor,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Center(
                              child: Text(
                                  'Next',
                                  style: mediumFont(Colors.black)
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        );
      });
}
