import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/bottomAppBarRow1.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/bottomAppBarRow2.dart';
import 'package:flutter/material.dart';

class CarSpaBottomAppBar extends StatelessWidget {
  CarSpaBottomAppBar({Key key, this.carSpaServiceResultData}) : super(key: key);
  final CarSpaServiceResultData carSpaServiceResultData;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: Get.find<CouponController>().isApplied.value
            ? 140
            : Get.find<CarSpaController>().offerApplicable.value
                ? 130
                : 100,
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [CarSpaBottomAppBarRow1(), CarSpaBottomAppBarRow2()],
        ),
      ),
    );
  }
}
