import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaTimeSlot extends StatelessWidget {
  CarSpaTimeSlot({Key key, this.index}) : super(key: key);
  final int index;
  final carSpaTimeController = Get.find<CarSpaTimeSlotController>();
  final carSpaController = Get.find<CarSpaController>();
  final checkOutController = Get.find<ServiceCheckOutController>();

  @override
  Widget build(BuildContext context) {
    
    return Obx(
      () => Bouncing(
        onPress: (carSpaController.carSpaTimeSlot
                    .contains(carSpaTimeController.timeList[index]['slot']) &&
                DateTime.now().isBefore(DateTime.parse(
                    "${carSpaTimeController.dateShow} ${carSpaTimeController.timeList[index]['time']}.000")))
            ? () {
                carSpaTimeController.index.value = index;
                checkOutController.timeSlot.value =
                    carSpaTimeController.timeList[index]['slot'];
                
                
              }
            : null,
        child: Container(
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: (carSpaController.carSpaTimeSlot.contains(
                        carSpaTimeController.timeList[index]['slot']) &&
                    DateTime.now().isBefore(DateTime.parse(
                        "${carSpaTimeController.dateShow} ${carSpaTimeController.timeList[index]['time']}.000")))
                ? (carSpaTimeController.index.value == index)
                    ? blackPrimary
                    : botAppBarColor
                : Colors.grey[300],
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
              child: carSpaTimeController.dateLoad.value
                  ? Text(carSpaTimeController.timeList[index]['text'],
                      style: (carSpaTimeController.index.value == index)
                          ? smallFontW600(Colors.white)
                          : smallFont(Colors.black))
                  : SizedBox(
                      height: 25,
                      width: 25,
                      child: Image.asset(Images.spinner, fit: BoxFit.fill))),
        ),
      ),
    );
  }
}
