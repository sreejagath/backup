import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaShowDate extends StatelessWidget {
  CarSpaShowDate({Key key}) : super(key: key);
  final carSpaTimeSlotController = Get.find<CarSpaTimeSlotController>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: 40,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: Text(
            carSpaTimeSlotController.dateShow.value,
            style: mediumFont(Colors.black),
          ),
        ),
      ),
    );
  }
}
