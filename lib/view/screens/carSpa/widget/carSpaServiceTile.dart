import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaBottumUpView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaServiceTile extends StatelessWidget {
  CarSpaServiceTile({Key key, this.carSpaServiceResultData, this.index})
      : super(key: key);
  final CarSpaServiceResultData carSpaServiceResultData;
  final int index;
  final craSpaController = Get.find<CarSpaController>();

  @override
  Widget build(BuildContext context) {
    // bool check = true;
    
    return GestureDetector(
      onTap: () {
        
        craSpaController.carSpaAddOnTotal.value = carSpaServiceResultData.price;
        craSpaController
            .setRadioStatusList(carSpaServiceResultData.addOns.length);
        carSpaBottomUpView(
            context,
            index,
            craSpaController.carSpaServiceProperty[index],
            carSpaServiceResultData);
      },
      child: Stack(children: [
        Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            margin: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
            height: MediaQuery.of(context).size.height * 0.17,
            clipBehavior: Clip.antiAlias,
            child: Stack(children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.12,
                      child: Stack(children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.25,
                          height: MediaQuery.of(context).size.height * 0.10,
                          clipBehavior: Clip.hardEdge,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey.withOpacity(0.2),
                              width: 1,
                            ),
                          ),
                          child: CustomImage(
                            image: carSpaServiceResultData.thumbUrl[0],
                            height: 70,
                            width: 70,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ]),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Text(
                            carSpaServiceResultData.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: mediumFont(Colors.black)
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: ListView.builder(
                                itemCount: (craSpaController
                                            .carSpaServiceProperty[index]
                                            .length >
                                        3)
                                    ? 3
                                    : craSpaController
                                        .carSpaServiceProperty[index].length,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.only(top: 0),
                                itemBuilder: (context, i) {
                                  return Text(
                                    '◍ ' +
                                        craSpaController
                                            .carSpaServiceProperty[index][i],
                                    style: smallFont(Colors.grey)
                                  );
                                })),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          '₹ ' + carSpaServiceResultData.price.toString(),
                          style: mediumFont(Colors.black),
                        ),
                      ]),
                ],
              ),
              ((carSpaServiceResultData.description != 'test') &&
                      (carSpaServiceResultData.description != null))
                  ? Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.03,
                        padding: EdgeInsets.symmetric(horizontal:10),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.only(
                            topLeft:
                                Radius.circular(Dimensions.RADIUS_EXTRA_LARGE),
                          ),
                        ),
                        child: Center(
                          child: SizedBox(
                            width: 110,
                            child: Text(
                              carSpaServiceResultData.description.toString(),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style: verySmallFontW600(Colors.white),
                            ),
                          ),
                        ),
                      ))
                  : SizedBox(),
            ])),
      ]),
    );
  }
}
