import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaAddressBottomUpView.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaBottomAppBarRow2 extends StatelessWidget {
  CarSpaBottomAppBarRow2({Key key, this.carSpaServiceResultData})
      : super(key: key);
  final CarSpaServiceResultData carSpaServiceResultData;
  final addressController = Get.find<AddressControllerFile>();
  final carSpaController = Get.find<CarSpaController>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        height: Get.find<CouponController>().isApplied.value
            ? 56
            : Get.find<CarSpaController>().offerApplicable.value
                ? 66
                : 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Total",
                      style: smallFont(Colors.black),
                    ),
                    Get.find<CouponController>().isApplied.value
                        ? Text(
                            " : ₹ " +
                                carSpaController.carSpaAddOnTotal.toString(),
                            style: smallFontW600(Colors.black),
                          )
                        : Get.find<CarSpaController>().offerApplicable.value
                            ? Text(
                                " : ₹ " +
                                    carSpaController.carSpaAddOnTotal
                                        .toString(),
                                style: smallFontW600(Colors.black),
                              )
                            : SizedBox()
                  ],
                ),
                Get.find<CouponController>().isApplied.value
                    ? Row(
                        children: [
                          Text(
                            "Discount : ",
                            style: smallFont(Colors.black),
                          ),
                          Text(
                            "₹ " +
                                Get.find<CouponController>()
                                    .discount
                                    .toString(),
                            style: smallFontW600(Colors.green),
                          )
                        ],
                      )
                    : Get.find<CarSpaController>().offerApplicable.value
                        ? Row(
                            children: [
                              Text(
                                "Discount : ",
                                style: smallFont(Colors.black),
                              ),
                              Text(
                                "₹ " +
                                    Get.find<CarSpaController>()
                                        .discount
                                        .toString(),
                                style: smallFontW600(Colors.green),
                              )
                            ],
                          )
                        : SizedBox(),
                Row(
                  children: [
                    Get.find<CouponController>().isApplied.value
                        ? Text(
                            "₹ " +
                                Get.find<CouponController>()
                                    .finalAmount
                                    .toString(),
                            style: largeFont(Colors.black),
                          )
                        : Get.find<CarSpaController>().offerApplicable.value
                            ? Text(
                                "₹ " +
                                    Get.find<CarSpaController>()
                                        .discountedAmount
                                        .toString(),
                                style: largeFont(Colors.black),
                              )
                            : Text(
                                "₹ " +
                                    carSpaController.carSpaAddOnTotal
                                        .toString(),
                                style: largeFont(Colors.black),
                              ),
                    Text(
                      "(inc. tax)",
                      style: smallFont(Colors.black),
                    ),
                  ],
                )
              ],
            ),
            Bouncing(
              onPress: () {
                if (Get.find<ServiceCheckOutController>().timeSlot.value ==
                    "") {
                  Get.snackbar('Warning', 'Time Slot not Selected...!',
                      snackPosition: SnackPosition.TOP,
                      duration: Duration(milliseconds: 1000),
                      backgroundColor: Colors.red,
                      colorText: Colors.white,
                      snackStyle: SnackStyle.FLOATING);
                } else {
                  addressController.getAddress().then((value) => {
                        if (addressController.addressList.isNotEmpty)
                          {
                            addressController.getDefaultAddress().then(
                                (value) => carSpaAddressBottomUpView(
                                    context, carSpaServiceResultData))
                          }
                        else
                          {
                            carSpaAddressBottomUpView(
                                context, carSpaServiceResultData)
                          }
                      });
                }
              },
              child: Container(
                height: 40,
                // width: 200,
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  color: botAppBarColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(
                  child: Text('Proceed to Payment',
                      style: mediumFont(Colors.black)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
