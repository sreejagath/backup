import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/address_edit/addressDetailsPage.dart';
import 'package:pexa_customer/view/screens/success/success.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<dynamic> carSpaAddressBottomUpView(
    BuildContext context, CarSpaServiceResultData carSpaServiceResultData) {
  final addressController = Get.find<AddressControllerFile>();
  final checkOutController = Get.find<ServiceCheckOutController>();
  final carSpaController = Get.find<CarSpaController>();
  return showModalBottomSheet(
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return IntrinsicHeight(
          child: Container(
            // height: MediaQuery.of(context).size.height * .40,
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                )),
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Bouncing(
                  onPress: () {
                    Navigator.pop(context);
                    Get.find<AddressControllerFile>().fromPexaShoppe.value =
                        false;
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddressDetailsPage(
                                backContext: context,
                              )),
                    );
                    // Get.to(() => AddressDetailsPage(backContext: context,));
                  },
                  child: IntrinsicWidth(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                        color: botAppBarColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Add / Change Address',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                addressController.addressList.isEmpty
                    ? SizedBox(
                        height: 80,
                        child: Center(
                          child: (addressController.isNoAddress.value)
                              ? Text(
                                  'No Saved Address',
                                  style: mediumFont(Colors.black),
                                )
                              : SizedBox(
                                  height: 35,
                                  width: 35,
                                  child: Image.asset(Images.spinner,
                                      fit: BoxFit.fill)),
                        ),
                      )
                    : Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  addressController.defaultAddress['name'],
                                  style: mediumFont(Colors.black),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 0.1, horizontal: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.grey[200],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: Text(
                                        addressController
                                            .defaultAddress['type'],
                                        style: mediumFont(Colors.black))),
                              ],
                            ),
                            SizedBox(
                              height: 2.5,
                            ),
                            Text(
                                addressController.defaultAddress['house'] +
                                    ', ' +
                                    addressController.defaultAddress['street'] +
                                    ', ' +
                                    addressController.defaultAddress['pincode']
                                        .toString(),
                                style: smallFont(Colors.black)),
                            SizedBox(
                              height: 2.5,
                            ),
                            Text(
                              addressController.defaultAddress['mobile']
                                  .toString(),
                              style: smallFontW600(Colors.black),
                            ),
                          ],
                        ),
                      ),
                SizedBox(
                  height: 20,
                ),
                Bouncing(
                  onPress: addressController.addressList.isEmpty
                      ? null
                      : () {
                          if (Get.find<CouponController>().isApplied.value) {
                            Map<String, dynamic> data = {
                              "id": checkOutController.serviceId.value,
                              "coordinate":
                                  Get.find<CurrentLocationController>()
                                      .currentPosition,
                              "timeSlot": checkOutController.timeSlot.value,
                              "addOns": carSpaController.carSpaAddOns,
                              "date": Get.find<CarSpaTimeSlotController>()
                                  .dateShow
                                  .toString(),
                              "couponCode":
                                  Get.find<CouponController>().couponName.value
                            };
                            
                            checkOutController
                                .placeOrder(data)
                                .then((value) => Get.to(() => SuccessPage()));
                          } else if (Get.find<CarSpaController>()
                              .offerApplicable
                              .value) {
                            Map<String, dynamic> data = {
                              "id": checkOutController.serviceId.value,
                              "coordinate":
                                  Get.find<CurrentLocationController>()
                                      .currentPosition,
                              "timeSlot": checkOutController.timeSlot.value,
                              "addOns": carSpaController.carSpaAddOns,
                              "date": Get.find<CarSpaTimeSlotController>()
                                  .dateShow
                                  .toString(),
                              "offerId": Get.find<CarSpaController>()
                                  .offerCouponId
                                  .value
                            };
                            
                            checkOutController
                                .placeOrder(data)
                                .then((value) => Get.to(() => SuccessPage()));
                          } else {
                            Map<String, dynamic> data = {
                              "id": checkOutController.serviceId.value,
                              "coordinate":
                                  Get.find<CurrentLocationController>()
                                      .currentPosition,
                              "timeSlot": checkOutController.timeSlot.value,
                              "addOns": carSpaController.carSpaAddOns,
                              "date": Get.find<CarSpaTimeSlotController>()
                                  .dateShow
                                  .toString()
                            };
                            
                            checkOutController
                                .placeOrder(data)
                                .then((value) => Get.to(() => SuccessPage()));
                          }
                        },
                  child: Container(
                    height: 40,
                    width: 150,
                    margin: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      color: addressController.addressList.isEmpty
                          ? Colors.grey[300]
                          : botAppBarColor,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                      child: Text(
                        'Proceed',
                        style: mediumFont(Colors.black),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      });
}
