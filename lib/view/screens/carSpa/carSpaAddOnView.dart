import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carSpa/carSpaTimeSlot.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaServiceAddOnView extends StatelessWidget {
  CarSpaServiceAddOnView({Key key, @required this.carSpaServiceResultData})
      : super(key: key);
  final carModelController = Get.find<CarModelController>();
  final craSpaController = Get.find<CarSpaController>();
  final carSpaTimeSlotController = Get.find<CarSpaTimeSlotController>();
  final checkOutController = Get.find<ServiceCheckOutController>();
  final CarSpaServiceResultData carSpaServiceResultData;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // craSpaController.carSpaAddOns.clear();
      Get.find<CurrentLocationController>().getUserLocation();
    });
    return Obx(() =>
    (Get
        .find<ConnectivityController>()
        .status
        .value)
        ? Scaffold(
      appBar: AppBar(
        title: Text("Add Services", style: mediumFont(Colors.black)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 15,
          ),
          color: Theme
              .of(context)
              .textTheme
              .bodyText1
              .color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme
            .of(context)
            .cardColor,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(7)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                children: [
                  Container(
                    height: 70,
                    width: 70,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                                carSpaServiceResultData.thumbUrl[0]),
                            fit: BoxFit.fitWidth)),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        carModelController.carBrandName.value +
                            ' ' +
                            carModelController.carModelName.value,
                        style: verySmallFontW600(Colors.grey),
                      ),
                      SizedBox(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * .70,
                        child: Text(
                          carSpaServiceResultData.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: smallFontW600(Colors.black),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * .60,
                        child: Text(
                          carSpaServiceResultData.list,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: verySmallFont(Colors.grey),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 40,
                            width: 40,
                            child: Center(
                                child: Image.asset(
                                  'assets/carSpa/addmore.png',
                                )),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'ADD MORE SERVICES',
                                style: mediumFont(Colors.black),
                              ),
                              Text(
                                'Select the service you\nwish to add with your package',
                                style: verySmallFont(Colors.grey),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Expanded(
                        child: carSpaServiceResultData.addOns.isNotEmpty
                            ? ListView.builder(
                            shrinkWrap: true,
                            itemCount:
                            carSpaServiceResultData.addOns.length,
                            itemBuilder: (context, index) {
                              return Column(
                                children: [
                                  Divider(
                                    color: Colors.grey[300],
                                    thickness: 1,
                                  ),
                                  InkWell(
                                    splashColor: Colors.yellow,
                                    onTap: () {
                                      craSpaController
                                          .carSpaAddOnRadioState[
                                      index] = !craSpaController
                                          .carSpaAddOnRadioState[
                                      index];
                                      craSpaController.changeTotal(
                                          craSpaController
                                              .carSpaAddOnRadioState[
                                          index],
                                          carSpaServiceResultData
                                              .addOns[index].price);
                                      craSpaController.addOnADDorRemove(
                                          craSpaController
                                              .carSpaAddOnRadioState[
                                          index],
                                          {
                                            "name":
                                            carSpaServiceResultData
                                                .addOns[index]
                                                .name,
                                            "price":
                                            carSpaServiceResultData
                                                .addOns[index]
                                                .price,
                                          });
                                    },
                                    child: Row(
                                      children: [
                                        Obx(() =>
                                            SizedBox(
                                              height: 30,
                                              width: 30,
                                              child: Center(
                                                child: Container(
                                                  height: 17,
                                                  width: 17,
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors
                                                              .grey[
                                                          600]),
                                                      borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10)),
                                                      color: craSpaController
                                                          .carSpaAddOnRadioState[
                                                      index]
                                                          ? Colors
                                                          .black
                                                          .withOpacity(
                                                          0.7)
                                                          : Colors
                                                          .white),
                                                  child: Center(
                                                    child: Icon(
                                                      Icons.check,
                                                      color: Colors
                                                          .white,
                                                      size: 10,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                            child: Text(
                                              carSpaServiceResultData
                                                  .addOns[index].name,
                                              style:
                                              smallFont(Colors.black),
                                            )),
                                        Text(
                                            '₹ ' +
                                                carSpaServiceResultData
                                                    .addOns[index]
                                                    .price
                                                    .toString(),
                                            style: smallFontW600(
                                                Colors.black))
                                      ],
                                    ),
                                  )
                                ],
                              );
                            })
                            : Center(
                          child: Text(
                            "No addons added..!",
                            style: mediumFont(Colors.black),
                          ),
                        )),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 5,
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Container(
          height: 100,
          width: MediaQuery
              .of(context)
              .size
              .width,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.8),
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25))),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "total",
                        style: smallFontW600(Colors.black),
                      ),
                      Row(
                        children: [
                          Text(
                            "₹ ${craSpaController.carSpaAddOnTotal.toString()}",
                            style: largeFont(Colors.black),
                          ),
                          Text(
                            " (inc. tax)",
                            style: smallFont(Colors.black),
                          ),
                        ],
                      )
                    ],
                  ),
                  Bouncing(
                    onPress: () async {
                      if (Get
                          .find<AuthFactorsController>()
                          .isLoggedIn) {
                        if (Get
                            .find<CurrentLocationController>()
                            .currentPosition
                            .isNotEmpty) {
                          Get.find<CouponController>().clearValue();
                          Get.find<CarSpaController>().clearOffer();
                          Get.to(() =>
                              CarSpaTimeSlotView(
                                carSpaServiceResultData:
                                carSpaServiceResultData,
                              ));
                        } else {
                          Get.find<CurrentLocationController>()
                              .determinePosition();
                        }
                      } else {
                        Get.snackbar(
                            'Failed', 'Please Login to Continue...!',
                            snackPosition: SnackPosition.TOP,
                            duration: Duration(seconds: 2),
                            backgroundColor: Colors.red,
                            colorText: Colors.white,
                            snackStyle: SnackStyle.FLOATING);
                      }
                    },
                    child: Container(
                      height: 40,
                      width: 200,
                      decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.all(Radius.circular(7)),
                        color: botAppBarColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 7,
                            offset: Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Next',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    )
        : Scaffold(
      body: NoInternetScreenView(),
    ));
  }
}
