import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaServiceTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaServices extends StatelessWidget {
  CarSpaServices({Key key, @required this.title, this.index}) : super(key: key);
  final String title;
  final int index;
  final carSpaController = Get.find<CarSpaController>();

  @override
  Widget build(BuildContext context) {
    
    
    
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              title: IntrinsicWidth(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: white.withOpacity(0.4),
                  ),
                  child: Center(
                      child: Text(title.toUpperCase(),
                          style: mediumFont(Colors.black))),
                ),
              ),
              centerTitle: true,
              leading: SizedBox(
                child: Center(
                  child: InkWell(
                    radius: 10,
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        color: white.withOpacity(0.4),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.black,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            backgroundColor: Colors.white,
            body: carSpaController.carSpaService.isEmpty
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Services not listed',
                          style: smallFont(Colors.black),
                        ),
                        Get.find<AuthFactorsController>().isLoggedIn
                            ? SizedBox()
                            : Text(
                                'Please select the Car Model...!',
                                style: mediumFont(Colors.red),
                              )
                      ],
                    ),
                  )
                : CustomScrollView(
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    slivers: [
                      SliverToBoxAdapter(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 250,
                          child: Stack(
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: 250,
                                child: Image.network(
                                  carSpaController.carSpaBannerImages[index],
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                child: Container(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        topRight: Radius.circular(20)),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(List.generate(
                            carSpaController.carSpaService.length, (index) {
                          return CarSpaServiceTile(
                            index: index,
                            carSpaServiceResultData:
                                carSpaController.carSpaService[index],
                          );
                        })),
                      ),
                      SliverToBoxAdapter(
                          child: SizedBox(
                        height: 10,
                      ))
                    ],
                  ))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
