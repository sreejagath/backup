import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/tab_controller.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaCategoryTile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaSection extends StatelessWidget {
  const CarSpaSection({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final control = Get.find<TabControllerMethod>();
    final carSpaController = Get.find<CarSpaController>();

    return Obx(
      () => SliverToBoxAdapter(
        child: Container(
          decoration: BoxDecoration(
              image: control.tabIndex.value == 1
                  ? DecorationImage(
                      image: AssetImage(
                        "assets/carSpa/logo2.png",
                      ),
                      fit: BoxFit.cover,
                    )
                  : null),
          child: CustomScrollView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              primary: false,
              slivers: [
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
                      mainAxisSpacing: 10.0,
                      crossAxisSpacing: 10.0,
                      childAspectRatio: 1.0),
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      return Obx(
                        () => carSpaController.carSpaCategory.length == 0
                            ? Container(
                                child: Center(
                                child: Text('No Products'),
                              ))
                            : CarSpaCategoryTile(
                                index: index,
                                carSpaResultData:
                                    carSpaController.carSpaCategory[index],
                              ),
                      );
                    },
                    childCount: carSpaController.carSpaCategory.length != 0
                        ? control.tabIndex.value == 6
                            ? carSpaController.carSpaCategory.length > 6
                                ? 6
                                : carSpaController.carSpaCategory.length
                            : carSpaController.carSpaCategory.length
                        : 0,
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
