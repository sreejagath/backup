import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/TimeSlotSelection/widgets/applyCoupen.dart';

import 'widget/offerTile.dart';

class CarSpaOfferPage extends StatelessWidget {
  CarSpaOfferPage({Key key, this.carSpaServiceResultData}) : super(key: key);
  final CarSpaServiceResultData carSpaServiceResultData;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CarSpa Offers", style: mediumFont(Colors.black)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 15,
          ),
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 10,
            ),
            Text(
              'Coupon Code',
              style: mediumFont(Colors.black),
            ),
            SizedBox(
              height: 10,
            ),
            ApplyCoupenTimeSlot(
              route: 'carSpa',
              carSpaServiceResultData: carSpaServiceResultData,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Other Offers',
              style: mediumFont(Colors.black),
            ),
            Obx(
              () => Expanded(
                  child: Get.find<CarSpaController>().carSpaOffers.isNotEmpty
                      ? SingleChildScrollView(
                          child: Column(
                            children: [
                              ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: Get.find<CarSpaController>()
                                      .carSpaOffers
                                      .length,
                                  itemBuilder: (context, index) {
                                    return OfferTile(
                                      carSpaOfferModelData:
                                          Get.find<CarSpaController>()
                                              .carSpaOffers[index],
                                      carSpaServiceResultData:
                                          carSpaServiceResultData,
                                    );
                                  })
                            ],
                          ),
                        )
                      : Center(
                          child: Get.find<CarSpaController>().offerEmpty.value
                              ? Text(
                                  'No Offers Found..!',
                                  style: smallFontW600(Colors.grey),
                                )
                              : LoadingAnimationWidget.twistingDots(
                                  leftDotColor: const Color(0xFF4B4B4D),
                                  rightDotColor: const Color(0xFFf7d417),
                                  size: 50,
                                ),
                        )),
            )
          ],
        ),
      ),
    );
  }
}
