import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/applyOfferCarSpa.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaBottomAppBar.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaShowDate.dart';
import 'package:pexa_customer/view/screens/carSpa/widget/carSpaTimeSlotTile.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CarSpaTimeSlotView extends StatelessWidget {
  CarSpaTimeSlotView({Key key, this.carSpaServiceResultData}) : super(key: key);
  final carSpaTimeSlotController = Get.find<CarSpaTimeSlotController>();
  final carSpaController = Get.find<CarSpaController>();
  final checkOutController = Get.find<ServiceCheckOutController>();
  final CarSpaServiceResultData carSpaServiceResultData;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (Get.find<CurrentLocationController>().currentPosition.isEmpty) {
        Get.find<CurrentLocationController>().getUserLocation().then((value) =>
            carSpaTimeSlotController.initialLoad(carSpaServiceResultData.id));
      } else {
        carSpaTimeSlotController.initialLoad(carSpaServiceResultData.id);
      }
      checkOutController.serviceId.value = carSpaServiceResultData.id;
    });
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text("Order Details", style: mediumFont(Colors.black)),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  size: 15,
                ),
                color: Theme.of(context).textTheme.bodyText1.color,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
            ),
            body: Padding(
              padding: EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Obx(
                  () => Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Service Date',
                        style: mediumFont(Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Bouncing(
                        onPress: () {
                          carSpaTimeSlotController.changeDate(
                              context, carSpaServiceResultData);
                        },
                        child: CarSpaShowDate(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Service Slots',
                        style: mediumFont(Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      carSpaTimeSlotController.isLoad.value
                          ? SizedBox(
                              height: 200,
                              child: Center(
                                child: SizedBox(
                                    height: 35,
                                    width: 35,
                                    child: Image.asset(Images.spinner,
                                        fit: BoxFit.fill)),
                              ),
                            )
                          : carSpaController.carSpaTimeSlot.isEmpty &&
                                  carSpaTimeSlotController.dateLoad.value
                              ? SizedBox(
                                  height: 200,
                                  child: Center(
                                    child: Text(
                                      "Time Slots are not available..!",
                                      style: mediumFont(Colors.red),
                                    ),
                                  ),
                                )
                              : GridView.builder(
                                  shrinkWrap: true,
                                  itemCount:
                                      carSpaTimeSlotController.timeList.length,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          childAspectRatio: 2),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return CarSpaTimeSlot(
                                      index: index,
                                    );
                                  }),
                      SizedBox(
                        height: 20,
                      ),
                      // Text('Offer Coupons', style: mediumFont(Colors.black)),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                              child: ApplyOfferButton(
                            carSpaServiceResultData: carSpaServiceResultData,
                          ))),
                      Get.find<CouponController>().isApplied.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      (Get.find<CouponController>()
                                          .couponName
                                          .value),
                                      style: mediumFont(Colors.black),
                                    ),
                                    Text(
                                      ' coupon is applied..!',
                                      style: smallFontW600(Colors.green),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Bouncing(
                                        onPress: () {
                                          Get.find<CouponController>()
                                              .clearValue();
                                        },
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.red[900],
                                          size: 20,
                                        ))
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                    'You have saved ${Get.find<CouponController>().discount} INR',
                                    style: smallFontW600(Colors.black))
                              ],
                            )
                          : SizedBox(),
                      Get.find<CouponController>().showDetails.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'this Coupon will be applicable only for the minimum purchase '
                                  'amount of ${Get.find<CouponController>().minAmount.value}',
                                  style: verySmallFontW600(Colors.red),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            )
                          : SizedBox(),
                      Get.find<CarSpaController>().offerApplicable.value
                          ? SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('Offer Applied',
                                          style: mediumFont(Colors.green)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Bouncing(
                                          onPress: () {
                                            Get.find<CarSpaController>()
                                                .clearOffer();
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            color: Colors.red[900],
                                            size: 20,
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                      'You have saved ${Get.find<CarSpaController>().discount} INR',
                                      style: mediumFont(Colors.black))
                                ],
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar:
                BottomAppBar(elevation: 0, child: CarSpaBottomAppBar()))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
