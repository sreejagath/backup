import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/models/user/carBrand_model.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/home/widget/banner.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class GridImageCarBrands extends StatelessWidget {
  final CarBrandResultData carBrandModel;
  final int index;

  GridImageCarBrands({Key key, this.index, this.carBrandModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var carModelController = Get.find<CarModelController>();
    return GestureDetector(
      onTap: () {
        carModelController.carBrandIdChange(carBrandModel.id);
        carModelController.selectedBrand(index);
        carModelController.selectedModel.value = 1500;
        if (carModelController.selectedBrand.value != 1500) {
          Navigator.pop(context);
          carModelController.isModelSearch.value = false;
          carModelController
              .fetchCarModelVarients(carModelController.carBrandId.toString());
          modelVarientBottomSheet(
            context,
          );
        } else {
          Get.snackbar('Place select car model',
              'Your did not select your car brand please select a car brand',
              snackPosition: SnackPosition.TOP,
              duration: Duration(seconds: 2),
              backgroundColor: Colors.red,
              colorText: Colors.white,
              snackStyle: SnackStyle.FLOATING);
        }
      },
      child: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.width / 6,
            width: MediaQuery.of(context).size.width / 6,
            child: CustomImage(
                image: carBrandModel.thumbUrl[0].toString(),
                fit: BoxFit.contain),
          ),
          Obx(
            () => Visibility(
              visible: carModelController.selectedBrand.value == index,
              child: Container(
                height: MediaQuery.of(context).size.width / 6,
                width: MediaQuery.of(context).size.width / 6,
                color: Colors.white.withAlpha(200),
                child: Center(
                  child: Icon(
                    Icons.check,
                    color: Colors.green,
                    size: 50,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
