import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/models/user/carModels_model.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pexa_customer/view/base/custom_image.dart';

class GridImageCarModels extends StatelessWidget {
  GridImageCarModels({Key key, this.index, this.carModelsResultData})
      : super(key: key);
  final carModelController = Get.find<CarModelController>();
  final CarModelsResultData carModelsResultData;
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        
        
        carModelController.carModelIdChange(
            carModelsResultData.id, carModelsResultData.carType);
        carModelController.selectedModel(index);
      },
      child: Stack(
        children: [
          SizedBox(
            height: 100,
            width: 100,
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: CustomImage(
                    image: carModelsResultData.thumbUrl[0].toString(),
                    fit: BoxFit.contain,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: SizedBox(
                    width: 100,
                    child: Text(
                      carModelsResultData.name.toString(),
                      style: TextStyle(
                        fontSize: 12,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                )
              ],
            ),
          ),
          Obx(
            () => Visibility(
              visible: carModelController.selectedModel.value == index,
              child: Container(
                height: 100,
                width: 100,
                color: Colors.white.withAlpha(200),
                child: Center(
                  child: Icon(
                    Icons.check,
                    color: Colors.green,
                    size: 50,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
