import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/menu_model.dart';
import 'package:pexa_customer/helper/responsive_helper.dart';
import 'package:pexa_customer/helper/route_helper.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/menu/widget/menu_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

class MenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool _isLoggedIn = Get.find<AuthFactorsController>().isLoggedIn;
    double _ratio = ResponsiveHelper.isDesktop(context) ? 1.1 : ResponsiveHelper.isTab(context) ? 1.1 : 1.2;

    final List<MenuModel> _menuList = [
      MenuModel(icon: Images.user, title: 'Profile', backgroundColor: Theme.of(context).primaryColor, route: RouteHelper.getProfileRoute()),
      MenuModel(icon: Images.support, title: 'Help & Support', backgroundColor: Color(0xFF00C7B2), route: RouteHelper.getSupportRoute()),
      MenuModel(icon: Images.policy, title: 'Privacy Policy', backgroundColor: Color(0xFF6165D7), route: RouteHelper.getInfoPage('Privacy and Policy')),
      MenuModel(icon: Images.about_us, title: 'About Us', backgroundColor: Color(0xFF6AB5FF), route: RouteHelper.getInfoPage('About Us')),
      MenuModel(icon: Images.terms, title: 'Terms & Conditions', backgroundColor: Color(0xFFFFC444), route: RouteHelper.getInfoPage('Terms and Conditions')),
      MenuModel(icon: Images.log_out, title: _isLoggedIn ? 'Logout' : 'Signin', backgroundColor: Color(0xFFFF3A45), route: ''),
    ];

    return PointerInterceptor(
      child: Container(
        width: Dimensions.WEB_MAX_WIDTH,
        padding: EdgeInsets.symmetric(horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
          color: Theme.of(context).cardColor,
        ),
        child: Column(mainAxisSize: MainAxisSize.min, children: [

          InkWell(
            onTap: () => Get.back(),
            child: Icon(Icons.keyboard_arrow_down_rounded, size: 30),
          ),
          SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),

          GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: ResponsiveHelper.isDesktop(context) ? 8 : ResponsiveHelper.isTab(context) ? 6 : 4,
              childAspectRatio: (1/_ratio),
              crossAxisSpacing: Dimensions.PADDING_SIZE_EXTRA_SMALL, mainAxisSpacing: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            ),
            itemCount: _menuList.length,
            itemBuilder: (context, index) {
              return MenuButton(menu: _menuList[index], isProfile: index == 0, isLogout: index == _menuList.length-1);
            },
          ),
          SizedBox(height: ResponsiveHelper.isMobile(context) ? Dimensions.PADDING_SIZE_SMALL : 0),

        ]),
      ),
    );
  }
}
