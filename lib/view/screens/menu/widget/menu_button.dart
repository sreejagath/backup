import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/guestController.dart';
import 'package:pexa_customer/controller/myController/imageCacheController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/menu_model.dart';
import 'package:pexa_customer/helper/responsive_helper.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/screens/login/login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MenuButton extends StatelessWidget {
  final AuthFactorsController controller = Get.find();
  final MenuModel menu;
  final bool isProfile;
  final bool isLogout;

  MenuButton(
      {@required this.menu, @required this.isProfile, @required this.isLogout});

  @override
  Widget build(BuildContext context) {
    int _count = ResponsiveHelper.isDesktop(context)
        ? 8
        : ResponsiveHelper.isTab(context)
            ? 6
            : 4;
    double _size = ((context.width > Dimensions.WEB_MAX_WIDTH
                ? Dimensions.WEB_MAX_WIDTH
                : context.width) /
            _count) -
        Dimensions.PADDING_SIZE_DEFAULT;

    return InkWell(
      onTap: () {
        Navigator.pop(context);
        if (isLogout) {
          if (Get.find<AuthFactorsController>().isLoggedIn) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(
                    'Alert..!',
                    style: largeFont(Colors.red),
                  ),
                  content: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        Text(
                          'Are you sure to logout?',
                          style: mediumFont(Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      child: Text(
                        'Yes',
                        style: largeFont(Colors.black),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        Get.find<ImageCacheController>().clearCache();
                        controller
                            .logOut()
                            .then((value) => Get.off(LoginPage()));
                      },
                    ),
                  ],
                );
              },
            );
          } else {
            Get.find<ImageCacheController>().clearCache();
            controller.deleteGuestData().then((value) =>
                Get.find<GuestController>().guestLogout(
                    Get.find<AuthFactorsController>().phoneNumber.value));
            Get.off(LoginPage());
          }
        } else {
          Get.toNamed(menu.route);
        }
      },
      child: Column(children: [
        Container(
          height: _size - (_size * 0.2),
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
          margin:
              EdgeInsets.symmetric(horizontal: Dimensions.PADDING_SIZE_SMALL),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimensions.RADIUS_SMALL),
            color: isLogout
                ? Get.find<AuthFactorsController>().isLoggedIn
                    ? Colors.red
                    : Colors.green
                :Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[Get.isDarkMode ? 800 : 200],
                  spreadRadius: 1,
                  blurRadius: 5)
            ],
          ),
          alignment: Alignment.center,
          child: Image.asset(menu.icon,
              width: _size, height: _size,
               color:
               isProfile?
                Colors.yellow:
               Colors.white),
        ),
        SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
        Text(menu.title,
            style: verySmallFontW600(Colors.black),
            textAlign: TextAlign.center),
      ]),
    );
  }
}

// class ProfileImageWidget extends StatelessWidget {
//   final double size;
//
//   ProfileImageWidget({@required this.size});
//
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox();
//     // return GetBuilder<UserController>(builder: (userController) {
//     //   return Container(
//     //     decoration: BoxDecoration(
//     //         shape: BoxShape.circle,
//     //         border: Border.all(width: 2, color: Colors.white)),
//     //     child: ClipOval(
//     //       child: CustomImage(
//     //         image: 'assets/images/about_us.png',
//     //         //image: '${Get.find<SplashController>().configModel.baseUrls.customerImageUrl}'
//     //         //  '/${(userController.userInfoModel != null && Get.find<AuthController>().isLoggedIn()) ? userController.userInfoModel.image ?? '' : ''}',
//     //         width: size, height: size, fit: BoxFit.cover,
//     //       ),
//     //     ),
//     //   );
//     // });
//   }
// }
