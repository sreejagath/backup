import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/models/carShoppe/productListModel.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/products/productDetailedView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProductTile extends StatelessWidget {
  ProductTile({Key key, this.productListResultData}) : super(key: key);
  final ProductListResultData productListResultData;

  @override
  Widget build(BuildContext context) {
    final categoryModelController = Get.find<ProductCategoryController>();
    return GestureDetector(
      onTap: () async {
        print(productListResultData.id);
        categoryModelController
            .fetchProductDetails(productListResultData.id)
            .then((value) => Get.to(() => ProductDetailedView()));
      },
      child: Card(
        elevation: 2,
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  CustomImage(
                    image: productListResultData.imageUrl[0],
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(100),
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          productListResultData.offerPrice ==
                                  productListResultData.price
                              ? Container():
                          Text('₹ ' + productListResultData.price.toString(),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: verySmallFontwithLine(Colors.grey)),
                          Text(
                            '₹ ' + productListResultData.offerPrice.toString(),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: smallFont(Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                productListResultData.name.toUpperCase(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: smallFontW600(Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
