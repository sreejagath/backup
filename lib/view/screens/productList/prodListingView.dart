import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/view/base/cart_widget.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/cart/cart_screen.dart';
import 'package:pexa_customer/view/screens/productList/widgets/productTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:pexa_customer/util/images.dart';

class ProductListView extends StatelessWidget {
  ProductListView({Key key, this.title, this.subCatId}) : super(key: key);
  final String title;
  final String subCatId;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  int pageNumber = 1;

  @override
  Widget build(BuildContext context) {
    final categoryModelController = Get.find<ProductCategoryController>();
    final cartController = Get.find<CartControllerFile>();
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            appBar: AppBar(
              title: Text(title, style: mediumFont(Colors.black)),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  size: 15,
                ),
                color: Theme.of(context).textTheme.bodyText1.color,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
              actions: [
                IconButton(
                  onPressed: () {
                    cartController
                        .getCartDetails(true)
                        .then((value) => Get.to(() => CartScreen(
                              fromNav: false,
                              fromMain: false,
                            )));
                  },
                  icon: CartWidget(
                      color: Theme.of(context).textTheme.bodyText1.color,
                      size: 20),
                ),
              ],
            ),
            backgroundColor: Colors.white,
            body: Obx(
              () => SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: (categoryModelController.productListTemp.length == 0)
                      ? Center(
                          child: (categoryModelController.prodAvailable.value)
                              ? Text("No Products Listed")
                              : SizedBox(
                                  height: 35,
                                  width: 35,
                                  child: Image.asset(Images.spinner,
                                      fit: BoxFit.fill)),
                        )
                      : SingleChildScrollView(
                          child: Container(
                            padding: EdgeInsets.only(bottom: 20),
                            height: MediaQuery.of(context).size.height,
                            child: SmartRefresher(
                              controller: _refreshController,
                              enablePullDown: false,
                              enablePullUp: true,
                              onLoading: _onLoading,
                              child: gridView(
                                  categoryModelController.productListTemp),
                            ),
                          ),
                        )),
            ))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }

  GridView gridView(products) {
    return GridView.builder(
      shrinkWrap: true,
      itemCount: products.length,
      padding: EdgeInsets.all(10),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.0,
      ),
      itemBuilder: (context, index) {
        return ProductTile(
          productListResultData: products[index],
        );
      },
    );
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 500));
    pageNumber++;
    
    if (Get.find<ProductCategoryController>().productList.length == 20) {
      changePage(pageNumber);
    }
    _refreshController.loadComplete();
  }

  changePage(int page) async {
    Get.find<ProductCategoryController>().fetchProductListData(
        subCatId,
        Get.find<ProductCategoryController>().productLocationList,
        page.toString());
  }
}
