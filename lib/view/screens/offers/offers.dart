import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/packageOfferController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/view/screens/offers/widget/offerTile.dart';
import 'package:pexa_customer/view/screens/offers/widget/offer_container.dart';

class OfferScreen extends StatefulWidget {
  @override
  _OfferScreenState createState() => _OfferScreenState();
}

class _OfferScreenState extends State<OfferScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: _width,
                height: 270,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Foffer%20page%2Foffer-bann.jpeg?alt=media&token=9b52d1a2-742b-4f4d-baf0-e2dd9b0f4398'
                                  // 'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Foffer%'
                                  // '20page%2Foffer-banner.jpg?alt=media&token=0aad28c4-91d4-4b95-9a57-20d9894b0023'
                                  ),
                              fit: BoxFit.cover
                              )),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        height: 15,
                        width: _width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20))),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                      child: Text(
                    ' Car Spa Trending Offers',
                    style: largeFont(Colors.black),
                  ))),
              SizedBox(
                height: 10,
              ),
              //<-----OFFER SECTION----->
              // SizedBox(
              //   height: 200,
              //   width: MediaQuery.of(context).size.width,
              //   child: ListView.builder(
              //       scrollDirection: Axis.horizontal,
              //       physics: AlwaysScrollableScrollPhysics(),
              //       itemCount:
              //           Get.find<PackageOfferController>().offerList.length,
              //       itemBuilder: (context, index) {
              //         return MainOfferTile(
              //           data: Get.find<PackageOfferController>().offerList,
              //           index: index,
              //         );
              //       }),
              // ),
              //<-----OFFER SECTION ENDS----->
              Expanded(
                child: ListView.builder(
                      physics: AlwaysScrollableScrollPhysics(),
                      itemCount:
                          Get.find<PackageOfferController>().offerList.length,
                    itemBuilder: (context, index) {
                    return OfferTile(
                      index: index,
                      data: Get.find<PackageOfferController>().offerList,
                    );
                  }),
              ),
              


              // Expanded(
              //   child: SingleChildScrollView(
              //     child: Padding(
              //       padding: EdgeInsets.only(top: 10),
              //       child: ListView.builder(
              //           scrollDirection: Axis.vertical,
              //           physics: NeverScrollableScrollPhysics(),
              //           padding: EdgeInsets.only(top: 0),
              //           shrinkWrap: true,
              //           itemCount: Get.find<PackageOfferController>()
              //               .offerList
              //               .length,
              //           itemBuilder: (context, index) {
              //             return Container(
              //               width: _width,
              //               padding: EdgeInsets.all(10),
              //               margin: EdgeInsets.only(
              //                   bottom: 10, left: 10, right: 10),
              //               decoration: BoxDecoration(
              //                 borderRadius: BorderRadius.circular(5),
              //                 color: Colors.white,
              //                 boxShadow: [
              //                   BoxShadow(
              //                     color: Colors.grey.withOpacity(0.5),
              //                     blurRadius: 7,
              //                     offset: Offset(
              //                         0, 3), // changes position of shadow
              //                   ),
              //                 ],
              //               ),
              //               child: Column(
              //                 children: [
              //                   Text(
              //                     Get.find<PackageOfferController>()
              //                         .offerList[index]['title'],
              //                     style: TextStyle(
              //                         fontSize: 16,
              //                         color: Colors.black,
              //                         fontWeight: FontWeight.bold),
              //                   ),
              //                   Text(
              //                     Get.find<PackageOfferController>()
              //                         .offerList[index]['description'],
              //                     style: TextStyle(
              //                         fontSize: 14,
              //                         color: Colors.black,
              //                         fontWeight: FontWeight.w500),
              //                   )
              //                 ],
              //               ),
              //             );
              //           }),
              //     ),
              //   ),
              // ),
            ],
          ),
        )
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
