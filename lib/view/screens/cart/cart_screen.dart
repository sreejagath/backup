import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/productController.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/not_logged_in_screen.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carShoppee/shoppeCheckout.dart';
import 'package:pexa_customer/view/screens/cart/widget/cartPrdouctTile.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:pexa_customer/util/images.dart';

class CartScreen extends StatelessWidget {
  final fromNav;
  final fromMain;
  final String prodId;
  final cartController = Get.find<CartControllerFile>();

  CartScreen({@required this.fromNav, @required this.fromMain, this.prodId});

  Future goBack(BuildContext context) async {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey.withAlpha(0),
              elevation: 0,
              leading: !fromNav
                  ? IconButton(
                      onPressed: () {
                        if (fromMain) {
                          goBack(context).then((value) =>
                              Get.find<ProductDetailsController>()
                                  .checkProductInCart(prodId));
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.black,
                        size: 15,
                      ))
                  : SizedBox(),
              title: Text(
                "My Cart",
                style: mediumFont(Colors.black),
              ),
              centerTitle: true,
            ),
            body: Get.find<AuthFactorsController>().isLoggedIn
                ? Obx(
                    () => cartController.cartList.isEmpty
                        ? Center(
                            child: (cartController.noItem.value)
                                ? Container(
                                    height: 300,
                                    width: 200,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                            'assets/carSpa/cartEmpty.png'),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          'No items in cart',
                                          style: mediumFont(Colors.black),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Get.offAllNamed('/');
                                          },
                                          child: Container(
                                            height: 40,
                                            width: 200,
                                            decoration: BoxDecoration(
                                              color: botAppBarColor,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Center(
                                              child: Text(
                                                'Go to Shop',
                                                style: mediumFont(Colors.black),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ))
                                // Text(
                                //     'Cart is Empty',
                                //     style: mediumFont(Colors.black),
                                //   )
                                : LoadingAnimationWidget.twistingDots(
                                    leftDotColor: const Color(0xFF4B4B4D),
                                    rightDotColor: const Color(0xFFf7d417),
                                    size: 50,
                                  ),
                          )
                        : Column(
                            children: [
                              Expanded(
                                child: Scrollbar(
                                  child: SingleChildScrollView(
                                    padding: EdgeInsets.all(10),
                                    physics: BouncingScrollPhysics(),
                                    child: Center(
                                      child: SizedBox(
                                        width: Dimensions.WEB_MAX_WIDTH,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              ListView.builder(
                                                physics:
                                                    NeverScrollableScrollPhysics(),
                                                shrinkWrap: true,
                                                itemCount: cartController
                                                    .cartList.length,
                                                itemBuilder: (context, index) {
                                                  return CartProductTile(
                                                    index: index,
                                                    cartItem: cartController
                                                        .cartList[index],
                                                  );
                                                },
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              // Container(
                                              //   padding: EdgeInsets.symmetric(
                                              //       vertical: 5,
                                              //       horizontal: 2.5),
                                              //   decoration: BoxDecoration(
                                              //       color: Colors.white,
                                              //       boxShadow: [
                                              //         BoxShadow(
                                              //           color: Colors.grey
                                              //               .withOpacity(0.8),
                                              //           blurRadius: 7,
                                              //           offset: Offset(0,
                                              //               3), // changes position of shadow
                                              //         ),
                                              //       ],
                                              //       borderRadius:
                                              //           BorderRadius.all(
                                              //               Radius.circular(
                                              //                   5))),
                                              //   child: Center(
                                              //     child: Text('Enter Coupon'),
                                              //   ),
                                              // ),
                                              // SizedBox(
                                              //   height: 10,
                                              // ),
                                              Container(
                                                padding: EdgeInsets.all(15),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.8),
                                                        blurRadius: 7,
                                                        offset: Offset(0,
                                                            3), // changes position of shadow
                                                      ),
                                                    ],
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5))),
                                                child: Column(
                                                  children: [
                                                    SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      child: Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                          "Price Details",
                                                          style: mediumFont(
                                                              Colors.black),
                                                        ),
                                                      ),
                                                    ),
                                                    Divider(
                                                      color: Colors.grey,
                                                      thickness: 1,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          "Price ( ${cartController.cartList.length} items )",
                                                          style: smallFont(
                                                              Colors.grey),
                                                        ),
                                                        Text(
                                                          cartController
                                                              .cartTotalPrice
                                                              .toString(),
                                                          style: smallFontW600(
                                                              Colors.black),
                                                        )
                                                      ],
                                                    ),
                                                    // SizedBox(
                                                    //   height: 5,
                                                    // ),
                                                    // Row(
                                                    //   mainAxisAlignment:
                                                    //       MainAxisAlignment
                                                    //           .spaceBetween,
                                                    //   children: [
                                                    //     Text("Discount",
                                                    //         style: smallFont(Colors.grey)),
                                                    //     Text("0",style: smallFontW600(Colors.black))
                                                    //   ],
                                                    // ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text("Delivery Charge",
                                                            style: smallFont(
                                                                Colors.grey)),
                                                        Text(
                                                            cartController
                                                                .cartShipping
                                                                .toString(),
                                                            style:
                                                                smallFontW600(
                                                                    Colors
                                                                        .black))
                                                      ],
                                                    ),
                                                    Divider(
                                                      color: Colors.grey,
                                                      thickness: 1,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          "Total Amount",
                                                          style: mediumFont(
                                                              Colors.black),
                                                        ),
                                                        Text(
                                                          cartController
                                                              .cartGrandTotal
                                                              .toString(),
                                                          style: mediumFont(
                                                              Colors.black),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ]),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Bouncing(
                                onPress: () {
                                  Get.find<CouponController>().clearValue();
                                  Get.find<ProductCategoryController>()
                                      .getShippingDetails(cartController
                                          .cartGrandTotal.value
                                          .toInt()
                                          .toString());
                                  Get.find<AddressControllerFile>()
                                      .getAddress()
                                      .then((value) => {
                                            if (Get.find<
                                                    AddressControllerFile>()
                                                .addressList
                                                .isNotEmpty)
                                              {
                                                Get.find<
                                                        AddressControllerFile>()
                                                    .getDefaultAddress()
                                                    .then((value) => Get.to(
                                                        () => ShoppeCheckOut(
                                                              amount: cartController
                                                                  .cartGrandTotal
                                                                  .value,
                                                            )))
                                              }
                                            else
                                              {
                                                Get.to(() => ShoppeCheckOut(
                                                      amount: cartController
                                                          .cartGrandTotal.value,
                                                    ))
                                              }
                                          });
                                },
                                child: Container(
                                  height: 40,
                                  width: 200,
                                  margin: EdgeInsets.only(bottom: 10),
                                  decoration: BoxDecoration(
                                    color: blackPrimary,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.8),
                                        blurRadius: 7,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                  ),
                                  child: Center(
                                    child: Text("Proceed to Checkout",
                                        style: mediumFont(Colors.white)),
                                  ),
                                ),
                              )
                            ],
                          ),
                  )
                : NotLoggedInScreen())
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
