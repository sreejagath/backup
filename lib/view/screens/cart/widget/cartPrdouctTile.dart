import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/productController.dart';
import 'package:pexa_customer/models/carShoppe/cartListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/view/base/custom_image.dart';

class CartProductTile extends StatefulWidget {
  CartProductTile({Key key, this.cartItem, this.index}) : super(key: key);
  final CartItem cartItem;
  final int index;

  @override
  _CartProductTileState createState() => _CartProductTileState();
}

class _CartProductTileState extends State<CartProductTile> {
  String prodQty = '';
  final cartController = Get.find<CartControllerFile>();

  @override
  void initState() {
    super.initState();
    initialSet();
  }

  initialSet() {
    setState(() {
      prodQty = widget.cartItem.count.toString();
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Get.find<ProductDetailsController>().calculatePricePercentage(
          widget.cartItem.product.price, widget.cartItem.product.offerPrice);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.8),
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Row(
        children: [
          SizedBox(
            height: 100,
            width: 100,
            child: Stack(
              children: [
                Center(
                  child: Container(
                    clipBehavior: Clip.hardEdge,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    height: 95,
                    width: 95,
                    child: CustomImage(
                        image: widget.cartItem.product.thumbUrl[0].toString(),
                        fit: BoxFit.cover),
                  ),
                ),
                Positioned(
                    top: 0,
                    right: 0,
                    child: GestureDetector(
                      onTap: () {
                        cartController
                            .removeProdFromCart(widget.cartItem.product.id)
                            .then((value) => cartController
                                .getCartDetails(false)
                                .then((value) =>
                                    Get.find<ProductDetailsController>()
                                        .deletePercentage(widget.index)));
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        height: 25,
                        width: 25,
                        child: Center(
                          child: Icon(
                            Icons.delete,
                            color: Colors.red[900],
                            size: 20,
                          ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: SizedBox(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: SizedBox(
                      height: 100,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            width: 120,
                            child: Text(
                                widget.cartItem.product.name.toUpperCase(),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                softWrap: false,
                                style: mediumFont(Colors.black)),
                          ),
                          SizedBox(
                            height: 2.5,
                          ),
                          Row(
                            children: [
                              Text(
                                  '₹ ' +
                                      widget.cartItem.product.price.toString(),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: verySmallFontwithLine(Colors.grey)),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                '₹ ' +
                                    widget.cartItem.product.offerPrice
                                        .toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: smallFontW600(Colors.black),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Obx(
                                () => Text(
                                  '(${Get.find<ProductDetailsController>().pricePercentage[widget.index]}% off)',
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: smallFontW600(Colors.green),
                                ),
                              ),
                            ],
                          ),
                          // SizedBox(
                          //   height: 2.5,
                          // ),
                          // Container(
                          //   padding: EdgeInsets.all(2.5),
                          //   decoration: BoxDecoration(
                          //       color: Colors.green[500],
                          //       borderRadius:
                          //           BorderRadius.all(Radius.circular(5))),
                          //   child: Center(
                          //       child: Text(
                          //     "4.0 *",
                          //     maxLines: 1,
                          //     overflow: TextOverflow.ellipsis,
                          //     style: robotoRegular.copyWith(
                          //         fontSize: 12,
                          //         fontWeight: FontWeight.bold,
                          //         color: Colors.white),
                          //   )),
                          // ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(color: Colors.grey)),
                    child: Center(
                      child: DropdownButton(
                        hint: Text("1"),
                        value: prodQty,
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            decoration: TextDecoration.none),
                        icon: SizedBox(),
                        items:
                            ['1', '2', '3', '4', '5', '6'].map((String items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Text(items),
                          );
                        }).toList(),
                        onChanged: (String newValue) {
                          setState(() {
                            if (int.parse(newValue) >
                                widget.cartItem.product.quantity) {
                              Get.snackbar('Error',
                                  'Quantity should be less than the stock..!',
                                  snackPosition: SnackPosition.TOP,
                                  duration: Duration(seconds: 2),
                                  backgroundColor: Colors.red,
                                  colorText: Colors.black,
                                  snackStyle: SnackStyle.FLOATING);
                              return;
                            } else {
                              prodQty = newValue;
                            }
                            cartController
                                .addOrUpdateToCart(widget.cartItem.product.id,
                                    int.parse(prodQty), 'update')
                                .then((value) =>
                                    cartController.getCartDetails(false));
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
