import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NoInternetScreenView extends StatelessWidget {
  final Widget child;

  NoInternetScreenView({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/carSpa/offline.png',
              height: 100,
            ),
            SizedBox(
              height: 20,
            ),
            DefaultTextStyle(
              style: TextStyle(
                  color: Colors.red, fontSize: 17, fontWeight: FontWeight.w500),
              child: AnimatedTextKit(
                animatedTexts: [
                  WavyAnimatedText('No Internet...!'),
                ],
                repeatForever: true,
              ),
            ),
            (child != null)
                ? SizedBox(
                    height: 30,
                  )
                : SizedBox(),
            (child != null)
                ? Bouncing(
                    onPress: () async {
                      if (Get.find<ConnectivityController>().status.value) {
                        if (Get.find<AuthFactorsController>().token.value != null) {
                          Get.find<UserDetailsController>().getUserModelId().then(
                                  (value) => Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (_) => child)));
                        } else {
                          Navigator.pushReplacement(
                              context, MaterialPageRoute(builder: (_) => child));
                        }
                      }
                    },
                    child: Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                        color: botAppBarColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Center(
                          child: Text(
                        "Retry",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15),
                      )),
                    ))
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
