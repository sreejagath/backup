import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class NoInternetSplashView extends StatelessWidget {
  final Widget child;

  NoInternetSplashView({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/carSpa/offline.png',height: 100,),
            SizedBox(height: 20,),
            DefaultTextStyle(
              style:TextStyle(color:Colors.red,fontSize: 17,fontWeight: FontWeight.w500),
              child: AnimatedTextKit(
                animatedTexts: [
                  WavyAnimatedText('No Internet...!'),
                ],
                repeatForever: true,
              ),
            )
          ],
        ),
      ),
    );
  }
}
