import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/mechanicalTimeSLotController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalServiceModel.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/applyOfferMechanical.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalBottomAppBar.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalShowDate.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalTimeSlotTile.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MechanicalTimeSlotView extends StatelessWidget {
  MechanicalTimeSlotView({Key key, this.mechanicalServiceResultData})
      : super(key: key);
  final mechanicalTimeSlotController = Get.find<MechanicalTimeSlotController>();
  final mechanicalController = Get.find<MechanicalController>();
  final checkOutController = Get.find<ServiceCheckOutController>();
  final MechanicalServiceResultData mechanicalServiceResultData;

  @override
  Widget build(BuildContext context) {
    if (Get.find<CurrentLocationController>().currentPosition.isEmpty) {
      Get.find<CurrentLocationController>().getUserLocation().then((value) =>
          mechanicalTimeSlotController
              .initialLoad(mechanicalServiceResultData.id));
    } else {
      mechanicalTimeSlotController.initialLoad(mechanicalServiceResultData.id);
    }
    checkOutController.serviceId.value = mechanicalServiceResultData.id;
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text("Order Details", style: mediumFont(Colors.black)),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 15,
                ),
                color: Theme.of(context).textTheme.bodyText1.color,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
            ),
            body: Padding(
              padding: EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Obx(
                  () => Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Service Date',
                        style: mediumFont(Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Bouncing(
                        onPress: () {
                          mechanicalTimeSlotController.changeDate(
                              context, mechanicalServiceResultData);
                        },
                        child: MechanicalShowDate(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Service Slots',
                        style: mediumFont(Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      mechanicalTimeSlotController.isLoad.value
                          ? SizedBox(
                              height: 200,
                              child: Center(
                                child: SizedBox(
                                    height: 35,
                                    width: 35,
                                    child: Image.asset(Images.spinner,
                                        fit: BoxFit.fill)),
                              ),
                            )
                          : mechanicalController.mechanicalTimeSlot.isEmpty &&
                                  mechanicalTimeSlotController.dateLoad.value
                              ? SizedBox(
                                  height: 200,
                                  child: Center(
                                    child: Text(
                                      "Time Slots are not available..!",
                                      style: mediumFont(Colors.red),
                                    ),
                                  ),
                                )
                              : GridView.builder(
                                  shrinkWrap: true,
                                  itemCount: mechanicalTimeSlotController
                                      .timeList.length,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          childAspectRatio: 2),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return MechanicalTimeSlotTile(
                                      index: index,
                                    );
                                  }),
                      SizedBox(
                        height: 20,
                      ),
                      ApplyMechanicalOffer(
                        mechanicalServiceResultData:
                            mechanicalServiceResultData,
                      ),
                      Get.find<CouponController>().isApplied.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      (Get.find<CouponController>()
                                          .couponName
                                          .value),
                                      style: mediumFont(Colors.black),
                                    ),
                                    Text(
                                      ' coupon is applied..!',
                                      style: smallFontW600(Colors.green),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Bouncing(
                                        onPress: () {
                                          Get.find<CouponController>()
                                              .clearValue();
                                        },
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.red[900],
                                          size: 20,
                                        ))
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                    'saved ${Get.find<CouponController>().discount} INR',
                                    style: smallFontW600(Colors.black))
                              ],
                            )
                          : SizedBox(),
                      Get.find<CouponController>().showDetails.value
                          ? Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'this Coupon will be applicable only for the minimum purchase '
                                  'amount of ${Get.find<CouponController>().minAmount.value}',
                                  style: verySmallFontW600(Colors.red),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            )
                          : SizedBox(),
                      Get.find<MechanicalController>().offerApplicable.value
                          ? SizedBox(
                              width: MediaQuery.of(context).size.width,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('Offer Applied',
                                          style: smallFontW600(Colors.green)),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Bouncing(
                                          onPress: () {
                                            Get.find<MechanicalController>()
                                                .clearOffer();
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            color: Colors.red[900],
                                            size: 20,
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                      'saved ${Get.find<MechanicalController>().discount} INR',
                                      style: smallFontW600(Colors.black))
                                ],
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar:
                BottomAppBar(elevation: 0, child: MechanicalBottomAppBar()))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
