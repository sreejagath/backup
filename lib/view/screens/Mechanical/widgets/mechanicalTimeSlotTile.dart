import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/mechanicalTimeSLotController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MechanicalTimeSlotTile extends StatelessWidget {
  MechanicalTimeSlotTile({Key key, this.index}) : super(key: key);
  final int index;
  final mechanicalTimeSlotController = Get.find<MechanicalTimeSlotController>();
  final mechanicalController = Get.find<MechanicalController>();
  final checkOutController = Get.find<ServiceCheckOutController>();

  @override
  Widget build(BuildContext context) {
    
    return Obx(
      () => Bouncing(
        onPress: (mechanicalController.mechanicalTimeSlot.contains(
                    mechanicalTimeSlotController.timeList[index]['slot']) &&
                DateTime.now().isBefore(DateTime.parse(
                    "${mechanicalTimeSlotController.dateShow} ${mechanicalTimeSlotController.timeList[index]['time']}.000")))
            ? () {
                mechanicalTimeSlotController.index.value = index;
                checkOutController.timeSlot.value =
                    mechanicalTimeSlotController.timeList[index]['slot'];
                
                
              }
            : null,
        child: Container(
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: (mechanicalController.mechanicalTimeSlot.contains(
                        mechanicalTimeSlotController.timeList[index]['slot']) &&
                    DateTime.now().isBefore(DateTime.parse(
                        "${mechanicalTimeSlotController.dateShow} ${mechanicalTimeSlotController.timeList[index]['time']}.000")))
                ? (mechanicalTimeSlotController.index.value == index)
                    ? blackPrimary
                    : botAppBarColor
                : Colors.grey[300],
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
              child: mechanicalTimeSlotController.dateLoad.value
                  ? Text(
                      mechanicalTimeSlotController.timeList[index]['text'],
                      style: (mechanicalTimeSlotController.index.value == index)
                          ? smallFontW600(Colors.white)
                          : smallFont(Colors.black),
                    )
                  : SizedBox(
                      height: 25,
                      width: 25,
                      child: Image.asset(Images.spinner, fit: BoxFit.fill))),
        ),
      ),
    );
  }
}
