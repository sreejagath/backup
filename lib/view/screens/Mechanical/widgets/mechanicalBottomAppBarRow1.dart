import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/mechanicalTimeSLotController.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MechanicalBottomAppBarRow1 extends StatelessWidget {
  MechanicalBottomAppBarRow1({Key key}) : super(key: key);
  final mechanicalTimeSlotController = Get.find<MechanicalTimeSlotController>();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      width: MediaQuery.of(context).size.width,
      child: Obx(
        () => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Payment Mode", style: mediumFont(Colors.black)),
            Container(
              height: 38,
              width: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Bouncing(
                    onPress: () {
                      mechanicalTimeSlotController.paymentIndex.value = 2;
                    },
                    child: Container(
                      height:
                          (mechanicalTimeSlotController.paymentIndex.value == 1)
                              ? 37
                              : 34,
                      width: 74,
                      decoration: BoxDecoration(
                          color: (mechanicalTimeSlotController
                                      .paymentIndex.value ==
                                  1)
                              ? blackPrimary
                              : Colors.grey[300],
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                              topLeft: Radius.circular(5))),
                      child: Center(
                        child: Text('Online',
                            style: (mechanicalTimeSlotController
                                        .paymentIndex.value ==
                                    1)
                                ? mediumFont(Colors.white)
                                : mediumFont(Colors.black)),
                      ),
                    ),
                  ),
                  Bouncing(
                    onPress: () {
                      mechanicalTimeSlotController.paymentIndex.value = 2;
                    },
                    child: Container(
                      height:
                          (mechanicalTimeSlotController.paymentIndex.value == 2)
                              ? 37
                              : 34,
                      width: 74,
                      decoration: BoxDecoration(
                        color:
                            (mechanicalTimeSlotController.paymentIndex.value ==
                                    2)
                                ? Colors.black
                                : Colors.grey[300],
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(5),
                            topRight: Radius.circular(5)),
                      ),
                      child: Center(
                        child: Text('Cash',
                            style: (mechanicalTimeSlotController
                                        .paymentIndex.value ==
                                    2)
                                ? mediumFont(Colors.white)
                                : mediumFont(Colors.black)),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
