import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalServiceModel.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalBottomAppBarRow1.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalBottomAppBarRow2.dart';
import 'package:flutter/material.dart';

class MechanicalBottomAppBar extends StatelessWidget {
  MechanicalBottomAppBar({Key key, this.mechanicalServiceResultData})
      : super(key: key);
  final MechanicalServiceResultData mechanicalServiceResultData;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: Get.find<CouponController>().isApplied.value
            ? 110
            : Get.find<MechanicalController>().offerApplicable.value
                ? 110
                : 100,
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MechanicalBottomAppBarRow1(),
            MechanicalBottomAppBarRow2()
          ],
        ),
      ),
    );
  }
}
