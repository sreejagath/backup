import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalServiceModel.dart';
import 'package:pexa_customer/view/screens/Mechanical/widgets/mechanicalAddressBottomUpView.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MechanicalBottomAppBarRow2 extends StatelessWidget {
  MechanicalBottomAppBarRow2({Key key, this.mechanicalServiceResultData})
      : super(key: key);
  final MechanicalServiceResultData mechanicalServiceResultData;
  final addressController = Get.find<AddressControllerFile>();
  final mechanicalController = Get.find<MechanicalController>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        height: Get.find<CouponController>().isApplied.value
            ? 56
            : Get.find<MechanicalController>().offerApplicable.value
                ? 56
                : 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Total",
                      style: smallFont(Colors.black),
                    ),
                    Get.find<CouponController>().isApplied.value
                        ? Text(
                            " : ₹ " +
                                mechanicalController.mechanicalAddOnTotal
                                    .toString(),
                            style: smallFontW600(Colors.black),
                          )
                        : Get.find<MechanicalController>().offerApplicable.value
                            ? Text(
                                " : ₹ " +
                                    mechanicalController.mechanicalAddOnTotal
                                        .toString(),
                                style: smallFontW600(Colors.black),
                              )
                            : SizedBox()
                  ],
                ),
                Get.find<CouponController>().isApplied.value
                    ? Row(
                        children: [
                          Text(
                            "Discount : ",
                            style: smallFont(Colors.black),
                          ),
                          Text(
                            "₹ " +
                                Get.find<CouponController>()
                                    .discount
                                    .toString(),
                            style: smallFontW600(Colors.green),
                          )
                        ],
                      )
                    : Get.find<MechanicalController>().offerApplicable.value
                        ? Row(
                            children: [
                              Text(
                                "Discount : ",
                                style: smallFont(Colors.black),
                              ),
                              Text(
                                "₹ " +
                                    Get.find<MechanicalController>()
                                        .discount
                                        .toString(),
                                style: smallFontW600(Colors.green),
                              )
                            ],
                          )
                        : SizedBox(),
                Row(
                  children: [
                    Get.find<CouponController>().isApplied.value
                        ? Text(
                            "₹ " +
                                Get.find<CouponController>()
                                    .finalAmount
                                    .toString(),
                            style: largeFont(Colors.black),
                          )
                        : Get.find<MechanicalController>().offerApplicable.value
                            ? Text(
                                "₹ " +
                                    Get.find<MechanicalController>()
                                        .discountedAmount
                                        .toString(),
                                style: largeFont(Colors.black),
                              )
                            : Text(
                                "₹ " +
                                    mechanicalController.mechanicalAddOnTotal
                                        .toString(),
                                style: largeFont(Colors.black),
                              ),
                    Text(
                      "(inc. tax)",
                      style: smallFont(Colors.black),
                    ),
                  ],
                )
              ],
            ),
            Bouncing(
              onPress: () {
                
                if (Get.find<ServiceCheckOutController>().timeSlot.value ==
                    "") {
                  Get.snackbar('Warning', 'Time Slot not Selected...!',
                      snackPosition: SnackPosition.TOP,
                      duration: Duration(milliseconds: 1000),
                      backgroundColor: Colors.red,
                      colorText: Colors.white,
                      snackStyle: SnackStyle.FLOATING);
                } else {
                  addressController.getAddress().then((value) => {
                        if (addressController.addressList.isNotEmpty)
                          {
                            addressController.getDefaultAddress().then(
                                (value) => mechanicalAddressBottomUpView(
                                    context, mechanicalServiceResultData))
                          }
                        else
                          {
                            mechanicalAddressBottomUpView(
                                context, mechanicalServiceResultData)
                          }
                      });
                }
              },
              child: Container(
                height: 40,
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  color: botAppBarColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(
                  child: Text('Proceed to Payment',
                      style: mediumFont(Colors.black)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
