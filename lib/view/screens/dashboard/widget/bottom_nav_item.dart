import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';

class BottomNavItem extends StatelessWidget {
  final String path;
  final String title;
  final Function onTap;
  final bool isSelected;

  BottomNavItem(
      {@required this.path, this.onTap, this.isSelected = false, this.title});

  @override
  Widget build(BuildContext context) {
    return isSelected
        ? Bouncing(
            onPress: onTap,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.black.withOpacity(0.3)),
                  padding:
                  EdgeInsets.only(top: 0, bottom: 0, left: 20, right: 20),
                  child: ImageIcon(
                      AssetImage(
                        path,
                      ),
                      color: Theme.of(context).primaryColor
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  title,
                  style: verySmallFontW600(Colors.black),
                )
              ],
            ),
          )
        : Bouncing(
            onPress: onTap,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: ImageIcon(
                    AssetImage(
                      path,
                    ),
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  title,
                  style: verySmallFontW600(Colors.black)
                )
              ],
            ),
          );
  }
}

class BottomNavs extends StatelessWidget {
  final String path;
  final Function onTap;

  BottomNavs({@required this.path, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(),
    );
  }
}
