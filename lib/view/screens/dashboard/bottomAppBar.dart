import 'dart:async';

import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/screens/cart/cart_screen.dart';
import 'package:pexa_customer/view/screens/dashboard/widget/bottom_nav_item.dart';
import 'package:pexa_customer/view/screens/home/home_screen.dart';
import 'package:pexa_customer/view/screens/menu/menu_screen.dart';
import 'package:pexa_customer/view/screens/offers/offers.dart';
import 'package:pexa_customer/view/screens/order/order_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewDashBoard extends StatefulWidget {
  NewDashBoard({@required this.pageIndex});

  final int pageIndex;

  @override
  _NewDashBoardState createState() => _NewDashBoardState();
}

class _NewDashBoardState extends State<NewDashBoard> {
  final cartController = Get.find<CartControllerFile>();
  PageController _pageController;
  int _pageIndex = 0;
  List<Widget> _screens;
  GlobalKey<ScaffoldMessengerState> _scaffoldKey = GlobalKey();
  bool _canExit = GetPlatform.isWeb ? true : false;

  @override
  void initState() {
    super.initState();
    _pageIndex = widget.pageIndex;

    _pageController = PageController(initialPage: widget.pageIndex);

    _screens = [
      HomeScreen(),
      OfferScreen(),
      //FavouriteScreen(),
      CartScreen(fromNav: true,fromMain: false),
      OrderScreen(),
      Container(),
    ];

    // Future.delayed(Duration(seconds: 1), () {
    //   setState(() {});
    // });
  }

  void _setPage(int pageIndex) {
    setState(() {
      _pageController.jumpToPage(pageIndex);
      _pageIndex = pageIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_pageIndex != 0) {
          _setPage(0);
          return false;
        } else {
          if (_canExit) {
            return true;
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Back press again to exit.',
                  style: TextStyle(color: Colors.white)),
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.green,
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
            ));
            _canExit = true;
            Timer(Duration(seconds: 2), () {
              _canExit = false;
            });
            return false;
          }
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            height: 55,
            decoration: BoxDecoration(
                color: botAppBarColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BottomNavItem(
                    path: 'assets/image/icons/home.png',
                    title: 'Home',
                    isSelected: _pageIndex == 0,
                    onTap: () => _setPage(0)),
                BottomNavItem(
                    path: 'assets/image/icons/coin.png',
                    title: 'Benefits',
                    isSelected: _pageIndex == 1,
                    onTap: () => _setPage(1)),
                BottomNavItem(
                    path: 'assets/image/icons/cart.png',
                    title: 'Cart',
                    isSelected: _pageIndex == 2,
                    onTap: () {
                      cartController.getCartDetails(true);
                      _setPage(2);
                    }),
                BottomNavItem(
                    path: 'assets/image/icons/history.png',
                    title: 'Orders',
                    isSelected: _pageIndex == 3,
                    onTap: () => _setPage(3)),
                BottomNavItem(
                    path: 'assets/image/icons/sidemenu.png',
                    title: 'Menu',
                    isSelected: _pageIndex == 4,
                    onTap: () {
                      Get.bottomSheet(MenuScreen(),
                          backgroundColor: Colors.transparent,
                          isScrollControlled: true);
                    }),
              ],
            ),
          ),
        ),
        body: PageView.builder(
          controller: _pageController,
          itemCount: _screens.length,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return _screens[index];
          },
        ),
      ),
    );
  }
}
