import 'dart:async';

import 'package:pexa_customer/controller/myController/appBarColorController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/controller/tab_controller.dart';
import 'package:pexa_customer/view/base/title_widget.dart';
import 'package:pexa_customer/view/screens/Mechanical/mechanicalSection.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/carSpa/carspa_section.dart';
import 'package:pexa_customer/view/screens/dashboard/bottomAppBar.dart';
import 'package:pexa_customer/view/screens/home/widget/banner.dart';
import 'package:pexa_customer/view/screens/carShoppee/car_shoppe.dart';
import 'package:pexa_customer/view/screens/home/widget/category_view.dart';
import 'package:pexa_customer/view/screens/profile/userDetailsUpdatePage.dart';
import 'package:pexa_customer/view/screens/quickHelp/quickHelpSection.dart';
import 'package:pexa_customer/view/screens/home/widget/homeAppBar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  Future<void> _loadData(bool reload) async {
    if (Get.find<ProductCategoryController>().categoryList.isEmpty) {
      await Get.find<ProductCategoryController>().fetchCategoryData();
    }
    await Get.find<UserDetailsController>().getUserModelId();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Get.find<ProductCategoryController>().setLocationForProductFetch();
      Get.find<CurrentLocationController>().getUserLocation();
    });
  }

  void checkData() {
    var userDetails = UserDetailsController();
    userDetails.getUserModelId();
    Future.delayed(const Duration(seconds: 20), () async{
      print('5 seconds');
      await Get.find<UserDetailsController>()
              .getUserModelId()
              .then((value) {
      if (userDetails.mailUpdated == false) {
        Get.off(() => UserDetailsUpdate(
              isEdit: false,
            ));
      }
      print(userDetails.mailUpdated);
    });
    });
  }

  @override
  Widget build(BuildContext context) {
    //checkData();
    double _width = MediaQuery.of(context).size.width;
    if (Get.find<ProductCategoryController>().categoryList.isEmpty) {
      Get.find<ProductCategoryController>().fetchCategoryData();
    }
    Get.find<UserDetailsController>().getUserModelId();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Get.find<ProductCategoryController>().setLocationForProductFetch();
      Get.find<CurrentLocationController>().getUserLocation();
    });
    final control = Get.find<TabControllerMethod>();

    final homeAppBarColorController = Get.find<HomeAppBarColorController>();

    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              toolbarHeight: 150,
              elevation: 0,
              backgroundColor: Colors.transparent,
              flexibleSpace: Column(
                children: [HomeAppbar()],
              ),
            ),
            backgroundColor: Theme.of(context).cardColor,
            body: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                ),
                RefreshIndicator(
                  onRefresh: () async {
                    await _loadData(true);
                  },
                  child: CustomScrollView(
                    shrinkWrap: true,
                    controller: homeAppBarColorController.scrollController,
                    physics: AlwaysScrollableScrollPhysics(),
                    slivers: [
                      SliverToBoxAdapter(
                        child: Container(
                          width: _width,
                          // height: 260,
                          height: 370,
                          child: BannerPage(),
                        ),
                      ),
                      // SliverToBoxAdapter(
                      //   child: CategoryView(),
                      // ),
                      Obx(
                        () => control.tabIndex.value == 0 ||
                                control.tabIndex.value == 6
                            ? SliverToBoxAdapter(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Container(
                                    height: 30,
                                    child: TitleWidget(
                                      title: 'Car Shoppe',
                                      onTap: () {
                                        control.tabIndex.value = 0;
                                      },
                                    ),
                                  ),
                                ),
                              )
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 0 ||
                                control.tabIndex.value == 6
                            ? CarShoppe()
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 1 ||
                                control.tabIndex.value == 6
                            ? SliverToBoxAdapter(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 50,
                                    //color: Theme.of(context).cardColor,
                                    child: control.tabIndex.value == 1
                                        ? TitleWidget(
                                            title: 'Car Spa',
                                            onTap: () {
                                              control.tabIndex.value = 1;
                                            },
                                          )
                                        : TitleWidget(
                                            title: 'Car Spa',
                                            onTap: () {
                                              control.tabIndex.value = 1;
                                            },
                                          ),
                                  ),
                                ),
                              )
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 1 ||
                                control.tabIndex.value == 6
                            ? CarSpaSection()
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 2 ||
                                control.tabIndex.value == 6
                            ? SliverToBoxAdapter(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 50,
                                    child: control.tabIndex.value == 2
                                        ? TitleWidget(
                                            title: 'Mechanical',
                                            onTap: () {
                                              control.tabIndex.value = 2;
                                            },
                                          )
                                        : TitleWidget(
                                            title: 'Mechanical',
                                            onTap: () {
                                              control.tabIndex.value = 2;
                                            },
                                          ),
                                  ),
                                ),
                              )
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 2 ||
                                control.tabIndex.value == 6
                            ? MechanicalSectionNew()
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 3 ||
                                control.tabIndex.value == 6
                            ? SliverToBoxAdapter(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 50,
                                    child: control.tabIndex.value == 3
                                        ? TitleWidget(
                                            title: 'Quick Help',
                                            onTap: () {
                                              control.tabIndex.value = 3;
                                            },
                                          )
                                        : TitleWidget(
                                            title: 'Quick Help',
                                            onTap: () {
                                              control.tabIndex.value = 3;
                                            },
                                          ),
                                  ),
                                ),
                              )
                            : SliverToBoxAdapter(),
                      ),
                      Obx(
                        () => control.tabIndex.value == 3 ||
                                control.tabIndex.value == 6
                            ? QuickHelpSectionNew()
                            : SliverToBoxAdapter(),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}

class SliverDelegate extends SliverPersistentHeaderDelegate {
  Widget child;

  SliverDelegate({@required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => 50;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverDelegate oldDelegate) {
    return oldDelegate.maxExtent != 50 ||
        oldDelegate.minExtent != 50 ||
        child != oldDelegate.child;
  }
}
