import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/ApiServices/carModelApi.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/appVersionController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/screens/car/widgets/grid_images.dart';
import 'package:pexa_customer/view/screens/car/widgets/grid_images_Varients.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/view/screens/home/widget/category_view.dart';
import 'package:pexa_customer/view/screens/home/widget/newCategoryView.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:url_launcher/url_launcher.dart';

class BannerPage extends StatelessWidget {
  const BannerPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    final urlImages = [
      'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner.jpg?alt=media&token=d8b1f633-f6ad-4be9-ad20-ff66e05c00a9',
      'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner1.jpg?alt=media&token=50899398-3301-4866-96ef-45ba81634de9',
      'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner2.jpg?alt=media&token=03f3fa83-f586-4172-845e-f95f2bb58189',
      'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner3.jpg?alt=media&token=72fa24b0-044a-4d7b-9c0d-c4c7be0fd4aa',
      'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2FAPP-banner4.jpg?alt=media&token=a1bb8c8c-f2bf-4f02-8de4-5468f86c708a'
    ];
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (!Get.find<UserDetailsController>().isMIdAvailable.value) {
        final carModelController = Get.find<CarModelController>();
        carModelController.isMakeSearch.value = false;
        if (carModelController.carModel.isEmpty) {
          carModelController.fetchData();
        }
        carSelectionBottomSheet(context);
      }
    });
    final carModelController = Get.find<CarModelController>();
    return Obx(
      () => Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          width: _width,
          // height: 280,
          height: 360,
          child: Stack(children: [
            Container(
              width: _width,
              height: 260,
              color: Colors.black,
              child: CarouselSlider.builder(
                options: CarouselOptions(
                  autoPlay: true,
                  viewportFraction: 1.0,
                  enlargeCenterPage: true,
                  height: 260,
                  disableCenter: true,
                  autoPlayInterval: Duration(seconds: 6),
                  onPageChanged: (index, reason) {},
                ),
                itemCount: urlImages.length,
                itemBuilder: (context, index, _) {
                  return InkWell(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.5),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[Get.isDarkMode ? 800 : 200],
                                spreadRadius: 1,
                                blurRadius: 5)
                          ],
                        ),
                        child: ClipRRect(
                          child: Image.network(
                            urlImages[index],
                            fit: BoxFit.fill,
                          ),
                        ),
                      ));
                },
              ),
            ),
            Positioned(
              top: 206,
              child: Container(
                width: _width,
                height: 170,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16)),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[
                      Colors.black.withOpacity(0.2),
                      Colors.black,
                      Colors.black,
                      Colors.black
                    ],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        carModelController.isMakeSearch.value = false;
                        if (carModelController.carModel.isEmpty) {
                          carModelController.fetchData();
                        }
                        carSelectionBottomSheet(context);
                      },
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 0, left: 10),
                            child: Container(
                              height: 50,
                              width: _width * 0.8,
                              child: Obx(
                                () => Row(
                                  children: [
                                    Image.network(
                                        'https://res.cloudinary.com/carclenx-pvt-ltd/image/upload/v1640598024/Icons/caricon_txtue3.png',
                                        height: 40,
                                        width: 60),
                                    (!Get.find<UserDetailsController>()
                                            .isMIdAvailable
                                            .value)
                                        ? Container(
                                            height: 30,
                                            padding: EdgeInsets.only(
                                                left: 5, right: 5),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Theme.of(context)
                                                      .cardColor,
                                                  width: 1),
                                            ),
                                            child: GestureDetector(
                                              onTap: () {
                                                carModelController
                                                    .isMakeSearch.value = false;
                                                if (carModelController
                                                    .carModel.isEmpty) {
                                                  carModelController
                                                      .fetchData();
                                                }
                                                carSelectionBottomSheet(
                                                  context,
                                                );
                                              },
                                              child: Text(
                                                'Enter Car Details >>' +
                                                    carModelController
                                                        .nullString
                                                        .toString(),
                                                textAlign: TextAlign.center,
                                                style: smallFont(Colors.white),
                                              ),
                                            ))
                                        : GestureDetector(
                                            onTap: () {
                                              carModelController
                                                  .isMakeSearch.value = false;
                                              if (carModelController
                                                  .carModel.isEmpty) {
                                                carModelController.fetchData();
                                              }
                                              carSelectionBottomSheet(
                                                context,
                                              );
                                            },
                                            child: Text(
                                              " ${carModelController.carBrandName}"
                                              " ${carModelController.carModelName}",
                                              style: smallFont(Colors.white),
                                            ),
                                          )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    NewCategoryView(),
                    SizedBox(
                      height: 0,
                    )
                  ],
                ),
                // child: Stack(
                //   children: [
                //     Positioned(
                //       top: 3,
                //       child: Container(
                //         height: 40,
                //         child: Row(
                //           children: [
                //             Padding(
                //               padding: EdgeInsets.only(top: 0, left: 10),
                //               child: Container(
                //                 height: 60,
                //                 width: _width * 0.8,
                //                 child: Obx(
                //                   () => Row(
                //                     children: [
                //                       Image.network(
                //                           'https://res.cloudinary.com/carclenx-pvt-ltd/image/upload/v1640598024/Icons/caricon_txtue3.png',
                //                           height: 40,
                //                           width: 60),
                //                       (!Get.find<UserDetailsController>()
                //                               .isMIdAvailable
                //                               .value)
                //                           ? GestureDetector(
                //                               onTap: () {
                //                                 carModelController
                //                                     .isMakeSearch
                //                                     .value = false;
                //                                 if (carModelController
                //                                     .carModel.isEmpty) {
                //                                   carModelController
                //                                       .fetchData();
                //                                 }
                //                                 carSelectionBottomSheet(
                //                                   context,
                //                                 );
                //                               },
                //                               child: Container(
                //                                   height: 30,
                //                                   padding: EdgeInsets.only(
                //                                       left: 5, right: 5),
                //                                   alignment:
                //                                       Alignment.center,
                //                                   decoration: BoxDecoration(
                //                                     border: Border.all(
                //                                         color: Theme.of(
                //                                                 context)
                //                                             .cardColor,
                //                                         width: 1),
                //                                   ),
                //                                   child: Text(
                //                                     'Enter Car Details >>' +
                //                                         carModelController
                //                                             .nullString
                //                                             .toString(),
                //                                     textAlign:
                //                                         TextAlign.center,
                //                                     style: smallFont(
                //                                         Colors.white),
                //                                   )),
                //                             )
                //                           : GestureDetector(
                //                               onTap: () {
                //                                 carModelController
                //                                     .isMakeSearch
                //                                     .value = false;
                //                                 if (carModelController
                //                                     .carModel.isEmpty) {
                //                                   carModelController
                //                                       .fetchData();
                //                                 }
                //                                 carSelectionBottomSheet(
                //                                   context,
                //                                 );
                //                               },
                //                               child: Text(
                //                                 " ${carModelController.carBrandName}"
                //                                 " ${carModelController.carModelName}",
                //                                 style:
                //                                     smallFont(Colors.white),
                //                               ),
                //                             )
                //                     ],
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           ],
                //         ),
                //       ),
                //     ),
                //     Positioned(
                //       top: 43,
                //       child:
                //           NewCategoryView(),
                //     )
                //   ],
                // )
              ),
            ),
            Positioned(
              // top: 250,
              top: 360,
              child: Container(
                width: _width,
                height: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16)),
                ),
              ),
            ),
            Get.find<AppVersionController>().isOutDated.value
                ? Bouncing(
                    onPress: () async {
                      if (Platform.isAndroid) {
                        if (!await launch(
                            'https://play.google.com/store/apps/details?id=com.carclenx.motor.shoping'))
                          throw 'Could not launch URL';
                      } else if (Platform.isIOS) {
                        if (!await launch(
                            'https://apps.apple.com/in/app/pexa-shoppe/id1613868591'))
                          throw 'Could not launch URL';
                      }
                    },
                    child: SizedBox(
                      width: _width,
                      height: 200,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: IntrinsicWidth(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 15),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Colors.yellow,
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.warning_amber_outlined,
                                  color: Colors.red,
                                  size: 15,
                                ),
                                Text(
                                  ' You are using the old version. Please update..!!',
                                  style: smallFontW600(Colors.black),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox()
          ])),
    );
  }
}

Future<dynamic> carSelectionBottomSheet(
  BuildContext context,
) {
  var carModelController = Get.find<CarModelController>();
  TextEditingController brandController = TextEditingController();
  return showModalBottomSheet(
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * .85,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              )),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              Image.network(
                                'https://res.cloudinary.com/carclenx-pvt-ltd/image/upload/v1640598024/Icons/caricon_txtue3.png',
                                color: Colors.black,
                                width: 50,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              carModelController.isMakeSearch.value
                                  ? Expanded(
                                      flex: 6,
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 5, left: 10, right: 10),
                                        height: 40,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                          border: Border.all(
                                              color: botAppBarColor2),
                                        ),
                                        child: Center(
                                          child: TextField(
                                            onTap: () {
                                              carModelController
                                                  .selectedBrand.value = 1500;
                                            },
                                            onChanged: (value) {
                                              carModelController.searchBrand(
                                                  value.toUpperCase());
                                            },
                                            controller: brandController,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ))
                                  : Text(
                                      'Select your Car Brand',
                                      style: mediumFont(Colors.black),
                                    ),
                              Expanded(
                                  flex: 1,
                                  child: SizedBox(
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          carModelController
                                                  .isMakeSearch.value =
                                              !carModelController
                                                  .isMakeSearch.value;
                                        },
                                        child: Icon(
                                          Icons.search,
                                          size: 25,
                                          color: botAppBarColor2,
                                        ),
                                      ),
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    // color: Colors.yellow,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: Obx(() => carModelController.carModel.isEmpty
                        ? Center(
                            child: carModelController.noMakeFound.value
                                ? Text(
                                    'No Brand Found..!',
                                    style: mediumFont(Colors.black),
                                  )
                                : LoadingAnimationWidget.twistingDots(
                                    leftDotColor: const Color(0xFF4B4B4D),
                                    rightDotColor: const Color(0xFFf7d417),
                                    size: 50,
                                  ),
                          )
                        : GridView.builder(
                            itemCount: carModelController.carModel.length - 1,
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width / 18),
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              // crossAxisSpacing: 57,
                              crossAxisSpacing:
                                  MediaQuery.of(context).size.width / 6.316,
                              mainAxisSpacing: 10,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              return GridImageCarBrands(
                                carBrandModel:
                                    carModelController.carModel[index],
                                index: index,
                              );
                            })),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}

Future<dynamic> modelVarientBottomSheet(BuildContext context) {
  var carModelController = Get.find<CarModelController>();
  var userModelController = Get.find<UserDetailsController>();
  final categoryModelController = Get.find<ProductCategoryController>();
  TextEditingController modelController = TextEditingController();
  return showModalBottomSheet(
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * .85,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              )),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Obx(
                          () => Row(
                            children: [
                              Image.asset(
                                'assets/carSpa/car_cartoon.png',
                                color: Colors.white,
                                width: 50,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              carModelController.isModelSearch.value
                                  ? Expanded(
                                      flex: 6,
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 5, left: 10, right: 10),
                                        height: 40,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                          border: Border.all(
                                              color: botAppBarColor2),
                                        ),
                                        child: Center(
                                          child: TextField(
                                            onTap: () {
                                              carModelController
                                                  .selectedModel.value = 1500;
                                            },
                                            onChanged: (value) {
                                              carModelController.searchModel(
                                                  value.toUpperCase(),
                                                  carModelController
                                                      .carBrandAPIId.value);
                                            },
                                            controller: modelController,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ))
                                  : Text(
                                      'Select your Car Model',
                                      style: mediumFont(Colors.black),
                                    ),
                              Expanded(
                                  flex: 1,
                                  child: SizedBox(
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          carModelController
                                                  .isModelSearch.value =
                                              !carModelController
                                                  .isModelSearch.value;
                                        },
                                        child: Icon(
                                          Icons.search,
                                          size: 25,
                                          color: botAppBarColor2,
                                        ),
                                      ),
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: Obx(() => carModelController.carModelVarients.isEmpty
                        ? Center(
                            child: carModelController.noModelFound.value
                                ? Text(
                                    "No Model Found..!",
                                    style: mediumFont(Colors.black),
                                  )
                                : LoadingAnimationWidget.twistingDots(
                                    leftDotColor: const Color(0xFF4B4B4D),
                                    rightDotColor: const Color(0xFFf7d417),
                                    size: 50,
                                  ),
                          )
                        : GridView.builder(
                            itemCount: carModelController
                                .carModelVarients.length,
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    MediaQuery.of(context).size.width / 36),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    // crossAxisSpacing: 4.0,
                                    crossAxisSpacing:
                                        MediaQuery.of(context).size.width /
                                            6.316,
                                    mainAxisSpacing: 10),
                            itemBuilder: (BuildContext context, int index) {
                              return GridImageCarModels(
                                carModelsResultData:
                                    carModelController.carModelVarients[index],
                                index: index,
                              );
                            })),
                  ),
                ),
                Obx(
                  () => InkWell(
                    onTap: (carModelController.selectedModel.value != 1500)
                        ? () {
                            userModelController
                                .updateUserModelId(
                                    carModelController.carModelId.value,
                                    carModelController.carModelType.value)
                                .then((value) => ApiData()
                                    .fetchCarModelDetail(
                                        carModelController.carModelId.value)
                                    .then((value) => ApiData()
                                        .fetchCarBrandDetail(carModelController
                                            .carBrandId.value))
                                    .then((value) => categoryModelController
                                        .fetchCategoryData()));
                            Navigator.pop(context);
                          }
                        : null,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: Center(
                        child: Text(
                          'Select',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: (carModelController.selectedModel.value != 1500)
                            ? botAppBarColor
                            : Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}
