import 'package:pexa_customer/controller/tab_controller.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryView extends StatelessWidget {

  final control = Get.find<TabControllerMethod>();
  final List categoryDetails = [
    'assets/image/menu/shoppe_btn.png',
    'assets/image/menu/carspa_btn.png',
    'assets/image/menu/mechanical_btn.png',
    'assets/image/menu/quick_btn.png',
  ];
  final List categories = [
    'assets/image/menu/shoppe.png',
    'assets/image/menu/carspa.png',
    'assets/image/menu/mechanical.png',
    'assets/image/menu/quick.png',
  ];

  final List categoryTitle = [
    'Shoppe',
    'Car Spa',
    'Mechanical',
    'Quick Service',
  ];

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10,),
                  child: Container(
                    alignment: Alignment.center,
                    height: 95,
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.8),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child:
                    Obx(() => Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Bouncing(
                                    onPress: () {
                                      if (control.tabIndex.value != 0) {
                                        control.tabIndex.value = 0;
                                      } else {
                                        control.tabIndex.value = 6;
                                      }
                                    },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                          image: control.tabIndex.value == 0
                                              ? AssetImage(
                                                  categoryDetails[0],
                                                )
                                              : AssetImage(
                                                  categories[0],
                                                ),
                                          fit: BoxFit.cover,
                                        )),
                                      ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Bouncing(
                                    onPress: () {
                                      if (control.tabIndex.value != 1) {
                                        control.tabIndex.value = 1;
                                      } else {
                                        control.tabIndex.value = 6;
                                      }
                                    },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                          image: control.tabIndex.value == 1
                                              ? AssetImage(
                                                  categoryDetails[1],
                                                )
                                              : AssetImage(
                                                  categories[1],
                                                ),
                                          fit: BoxFit.cover,
                                        )),
                                      ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Bouncing(
                                    onPress: () {
                                      if (control.tabIndex.value != 2) {
                                        control.tabIndex.value = 2;
                                      } else {
                                        control.tabIndex.value = 6;
                                      }
                                    },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            // color: Colors.white,
                                            image: DecorationImage(
                                          image: control.tabIndex.value == 2
                                              ? AssetImage(
                                                  categoryDetails[2],
                                                )
                                              : AssetImage(
                                                  categories[2],
                                                ),
                                          fit: BoxFit.cover,
                                        )),
                                      ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Bouncing(
                                    onPress: () {
                                      if (control.tabIndex.value != 3) {
                                        control.tabIndex.value = 3;
                                      } else {
                                        control.tabIndex.value = 6;
                                      }
                                    },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                          image: control.tabIndex.value == 3
                                              ? AssetImage(
                                                  categoryDetails[3],
                                                )
                                              : AssetImage(
                                                  categories[3],
                                                ),
                                          fit: BoxFit.cover,
                                        )),
                                      ),
                                  ),
                                ),
                              ],
                            ),
                          )
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
