import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/appBarColorController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/homeSearchBoxController.dart';
import 'package:pexa_customer/controller/myController/newSearchController.dart';
import 'package:pexa_customer/view/screens/location/newLocationSearch.dart';
import 'package:pexa_customer/view/screens/search/searchScreenNew.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class HomeAppbar extends StatelessWidget {
  const HomeAppbar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery
        .of(context)
        .size
        .width;
    final homeAppBarColorController = Get.find<HomeAppBarColorController>();
    final homeSearchBoxController = Get.find<HomeSearchBoxController>();
    homeSearchBoxController.changeWidth(60, false);
    homeSearchBoxController.isTimer.value = false;
    return Obx(
          () =>
          Container(
            padding: EdgeInsets.all(10),
            width: _width,
            color: Colors.black.withOpacity(
                double.parse(
                    homeAppBarColorController.opacity.value.toString())),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 25,
                ),
                SizedBox(
                  width: _width,
                  height: 40,
                  child: Stack(
                    children: [
                      Positioned(
                        left: 0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              'assets/carSpa/latestlogo.png',
                              height: 40,
                            ),
                            SizedBox(width: 5),
                            Image.asset(
                              'assets/carSpa/pexaNameOnly.png',
                              height: 10,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                          right: 0,
                          child: Row(
                            children: [
                              Bouncing(
                                  onPress: () {
                                    Get.to(() => LocationSearchPage());
                                  },
                                  child: Get
                                      .find<CurrentLocationController>()
                                      .userLocationString
                                      .value !=
                                      null &&
                                      Get
                                          .find<CurrentLocationController>()
                                          .userLocationString
                                          .value !=
                                          ''
                                      ? Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                      color: blackPrimary.withOpacity(0.5),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.end,
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          color: Colors.white,
                                          size: 20,
                                        ),
                                        Obx(
                                                () =>
                                            Get
                                                .find<
                                                CurrentLocationController>()
                                                .userLocationString
                                                .toString()
                                                .contains(',') ?
                                            Text(
                                              (Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .value
                                                  .split(',')[0] +
                                                  ',' +
                                                  Get
                                                      .find<
                                                      CurrentLocationController>()
                                                      .userLocationString
                                                      .value
                                                      .split(',')[1])
                                                  .length >
                                                  20
                                                  ? (Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .value
                                                  .split(',')[0] +
                                                  ',' +
                                                  Get
                                                      .find<
                                                      CurrentLocationController>()
                                                      .userLocationString
                                                      .value
                                                      .split(',')[1])
                                                  .substring(0, 19) +
                                                  '...'
                                                  : Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .value
                                                  .split(',')[0] ==
                                                  ''
                                                  ? 'Unknown Place'
                                                  : Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .value
                                                  .split(',')[0] +
                                                  ',' +
                                                  Get
                                                      .find<
                                                      CurrentLocationController>()
                                                      .userLocationString
                                                      .value
                                                      .split(',')[1],
                                              style: mediumFont(Colors.white),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              // textAlign: TextAlign.end,
                                            ) : Text(
                                              Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .toString()
                                                  .length > 20 ?
                                              Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .toString()
                                                  .substring(0, 10) :
                                              Get
                                                  .find<
                                                  CurrentLocationController>()
                                                  .userLocationString
                                                  .toString(),
                                              style: mediumFont(Colors.white),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                      : SizedBox()),
                              Bouncing(
                                onPress: () {},
                                child: Icon(
                                  Icons.notifications,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Stack(children: [
                    SizedBox(
                      width: _width,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: AnimatedContainer(
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.fastOutSlowIn,
                          width: homeSearchBoxController.width.value,
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: homeSearchBoxController.searchBoxColor
                                  .value,
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)),
                              border: homeSearchBoxController.isActive.value
                                  ? Border.all(color: Colors.white)
                                  : Border.all(color: Colors.transparent)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    if (homeSearchBoxController.isActive
                                        .value) {
                                      homeSearchBoxController.changeWidth(
                                          60, false);
                                    } else {
                                      homeSearchBoxController.changeWidth(
                                          _width, true);
                                    }
                                  },
                                  child: Icon(Icons.search,
                                      size: 25, color: Colors.white)),
                              homeSearchBoxController.isActive.value
                                  ? SizedBox(
                                width: 5,
                              )
                                  : SizedBox(),
                              homeSearchBoxController.isActive.value
                                  ? Expanded(
                                  child: InkWell(
                                    onTap: () =>
                                        Get.to(() => NewSearchScreen())
                                            .then((value) =>
                                        Get
                                            .find<PexaSearchController>()
                                            .isLoad
                                            .value = false),
                                    child: Text(
                                      'Search for products, categories etc.'.tr,
                                      maxLines: 1,
                                      style: verySmallFont(Colors.white),
                                    ),
                                  ))
                                  : SizedBox()
                            ],
                          ),
                        ),
                      ),
                    ),
                  ]),
                )
              ],
            ),
          ),
    );
  }
}
