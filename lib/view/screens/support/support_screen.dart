import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/custom_app_bar.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/support/widget/support_button.dart';
import 'package:flutter/material.dart';

class SupportScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        onBackPressed: (){
          Navigator.pop(context);
        },
        title: "Help & Support",
      ),
      body: Scrollbar(child: SingleChildScrollView(
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
        physics: BouncingScrollPhysics(),
        child: Center(child: SizedBox(width: Dimensions.WEB_MAX_WIDTH, child: Column(children: [
          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
          CustomImage(image: 'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/'
              'Pexa%20Shoppe%2Fhelp%20logo%2Fsupport_image.png?alt=media&token=b84c3d0c-e8d7-4063-8f8e-f1bd11776471',
          height: 110,),
          SizedBox(height: 30),
          SupportButton(
            icon: Icons.location_on, title: 'Address',
            info: 'Carclenx Pvt.Ltd, Mariyam Tower, Mamam, Attingal, Trivandrum, Kerala',
            onTap: () {},
          ),
          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
          SupportButton(
            icon: Icons.call, title: 'Call',
            info: '9633244456',
            onTap: () async {
              // if(await canLaunch('tel:${Get.find<SplashController>().configModel.phone}')) {
              //   launch('tel:9633244456}');
              // }else {
              //   showCustomSnackBar('${'can_not_launch'.tr} ${Get.find<SplashController>().configModel.phone}');
              // }
            },
          ),
          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
          SupportButton(
            icon: Icons.mail_outline, title: 'Email Us',
            //info: Get.find<SplashController>().configModel.email,
            info: 'support@pexaapp.in',
            onTap: () {
              // final Uri emailLaunchUri = Uri(
              //   scheme: 'mailto',
              //   path: Get.find<SplashController>().configModel.email,
              // );
              // launch(emailLaunchUri.toString());
            },
          ),

        ]))),
      )),
    );
  }
}
