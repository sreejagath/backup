import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/productController.dart';
import 'package:pexa_customer/models/carShoppe/subCategory_model.dart';
import 'package:pexa_customer/util/images.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/productList/prodListingView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubCategoryTile extends StatelessWidget {
  SubCategoryTile({Key key, this.subCategoryResultData}) : super(key: key);
  final SubCategoryResultData subCategoryResultData;

  Future goToProductListPage() async {
    Get.to(() => ProductListView(
          title: subCategoryResultData.name,
          subCatId: subCategoryResultData.id,
        ));
  }

  @override
  Widget build(BuildContext context) {
    final categoryModelController = Get.find<ProductCategoryController>();
    Get.find<ProductDetailsController>().oneTouch.value = true;
    return GestureDetector(
      onTap: () async {
        if (Get.find<ProductCategoryController>()
            .productLocationList
            .isNotEmpty) {
          if (Get.find<ProductDetailsController>().oneTouch.value) {
            Get.find<ProductDetailsController>().oneTouch.value = false;
            categoryModelController
                .fetchProductListData(
                    subCategoryResultData.id,
                    Get.find<ProductCategoryController>().productLocationList,
                    '1')
                .then((value) => goToProductListPage().then((value) => {
                      Get.find<ProductDetailsController>().oneTouch.value = true
                    }));
          }
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                Future.delayed(Duration(seconds: 3), () {
                  Navigator.of(context).pop();
                });
                return AlertDialog(
                  title: Column(
                    children: [
                      Text(
                        'Configuring your location',
                        textAlign: TextAlign.center,
                        style: mediumFont(Colors.grey),
                      ),
                      Text(
                        'please try again..!',
                        style: smallFontW600(Colors.green),
                      ),
                      LoadingAnimationWidget.twistingDots(
                        leftDotColor: const Color(0xFF4B4B4D),
                        rightDotColor: const Color(0xFFf7d417),
                        size: 50,
                      ),
                    ],
                  ),
                );
              });
          Get.find<CurrentLocationController>().determinePosition();
        }
      },
      child: Card(
        elevation: 2,
        child: Column(
          children: [
            Expanded(
              child: CustomImage(
                image: subCategoryResultData.imageUrl[0],
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                subCategoryResultData.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: smallFont(Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
