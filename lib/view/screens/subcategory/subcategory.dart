import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/subcategory/widget/subCategoryTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/util/images.dart';

class SubCategory extends StatelessWidget {
  SubCategory({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    final categoryModelController = Get.find<ProductCategoryController>();
    Get.find<ProductCategoryController>().setLocationForProductFetch();
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            appBar: AppBar(
              title: Text(title, style: mediumFont(Colors.black)),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios, size: 15, color: Colors.black),
                color: Theme.of(context).textTheme.bodyText1.color,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
            ),
            backgroundColor: Colors.white,
            body: Obx(
              () => SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: (categoryModelController.subCategoryList.length == 0)
                      ? Center(
                          child: (categoryModelController.subCatAvailable.value)
                              ? Text('Sub Category not listed')
                              : LoadingAnimationWidget.twistingDots(
                                  leftDotColor: const Color(0xFF4B4B4D),
                                  rightDotColor: const Color(0xFFf7d417),
                                  size: 50,
                                ),
                        )
                      : SingleChildScrollView(
                          child: Column(
                            children: [
                              gridView(categoryModelController.subCategoryList)
                            ],
                          ),
                        )),
            ))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }

  GridView gridView(products) {
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: products.length,
      padding: EdgeInsets.all(10),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.0,
      ),
      itemBuilder: (context, index) {
        return SubCategoryTile(
          subCategoryResultData: products[index],
        );
      },
    );
  }
}
