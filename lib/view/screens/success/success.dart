import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SuccessPage extends StatelessWidget {
  const SuccessPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Get.offAllNamed('/');
          return false;
        },
        child: Obx(() => (Get.find<ConnectivityController>().status.value)
            ? Scaffold(
                backgroundColor: Colors.white,
                body: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    width: Get.width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: Get.height * 0.1),
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage('assets/image/tick.png'),
                                height: Get.height * 0.12,
                              ),
                              SizedBox(
                                height: 15
                              ),
                              Text(
                                'Order Placed Successfully !',
                                style: largeFont(Colors.black),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            Bouncing(
                              onPress: () {
                                Get.offAllNamed('/');
                              },
                              child: Container(
                                height:40,
                                width: 150,
                                child: Center(
                                    child: Text(
                                  'Go Home',
                                  style:mediumFont(Colors.black),
                                )),
                                decoration: BoxDecoration(
                                    color: botAppBarColor,
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            : Scaffold(
                body: NoInternetScreenView(),
              )));
  }
}
