import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/serverController.dart';
import 'package:pexa_customer/controller/splash_controller.dart';
import 'package:pexa_customer/view/base/custom_snackbar.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  //final String orderID;

  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _route();
  }

  void _route() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      customSnackBar(context, 'Enable Location Services to continue');
      Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    } else {}

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        customSnackBar(
            context, 'Enable location services for better optimisation');
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      customSnackBar(
          context, 'Enable location services for better optimisation');
      Timer(Duration(seconds: 2), () async {
        permission = await Geolocator.requestPermission();
      });

      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      if (Get.find<ConnectivityController>().status.value) {
        Get.find<ServerTest>().serverStatus();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blackPrimary,
      key: _globalKey,
      body: GetBuilder<SplashController>(builder: (splashController) {
        _route();
        return Center(
            child: Get.find<ConnectivityController>().status.value
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/carSpa/latestlogo.png',
                        width: 180,
                        height: 180,
                      ),
                      Image.asset(
                        'assets/carSpa/pexaNameOnly.png',
                        width: 100,
                        color: Colors.white,
                        alignment: Alignment.centerRight,
                      ),
                    ],
                  )
                : NoInternetScreenView(
                    child: SplashScreen()));
      }),
    );
  }
}
