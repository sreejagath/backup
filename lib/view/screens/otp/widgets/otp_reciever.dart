import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpReceiverField extends StatelessWidget {
  const OtpReceiverField({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // OTPController controller = Get.put(OTPController());
    return PinCodeTextField(
        appContext: context,
        length: 6,
        keyboardType: TextInputType.phone,        
        cursorColor: Colors.black,
        textInputAction: TextInputAction.done,
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.box,
          //borderRadius: BorderRadius.circular(5),
          fieldHeight: 50,
          fieldWidth: 50,
          activeFillColor: Colors.black,
          inactiveFillColor: Colors.black,
          activeColor: Colors.black,
          inactiveColor: Colors.black,
          selectedColor: Colors.black,
          selectedFillColor: Colors.black,
         
        ),
        //backgroundColor: Colors.white,
        
        onChanged: (value) {},
        onCompleted: (pin) {
              // controller.onComplete(pin);
              // pin == '123456' ? Get.offAllNamed("/") : Container();
        });
    // return OTPTextField(
    //   length: 4,
    //   width: MediaQuery.of(context).size.width,
    //   fieldWidth: 50,
    //   keyboardType: TextInputType.phone,
    //   style: TextStyle(fontSize: 27),
    //   textFieldAlignment: MainAxisAlignment.spaceBetween,
    //   outlineBorderRadius: 0,
    //   otpFieldStyle: OtpFieldStyle(
    //     errorBorderColor: Colors.red,
    //     focusBorderColor: Colors.black,
    //   ),
    //   fieldStyle: FieldStyle.box,
    //   onChanged: (otp) {
    //     //controller.onComplete(otp);
    //     //
    //   },
    //   onCompleted: (pin) {
    //     controller.onComplete(pin);
    //     pin == '1234' ? Get.offAllNamed("/") : Container();
    //   },
    // );
  }
}
