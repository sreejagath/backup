import 'package:pexa_customer/ApiServices/loginApi.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/notificationController.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/view/base/custom_snackbar.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/profile/userDetailsUpdatePage.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpScreen extends StatefulWidget {
  const OtpScreen({Key key}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  var userDetailsController = Get.find<UserDetailsController>();
  final loginController = Get.find<AuthFactorsController>();
  TextEditingController pinController = TextEditingController();

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      customSnackBar(context, 'Enable Location Services to continue');
      Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    } else {}

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        customSnackBar(
            context, 'Enable location services for better optimisation');
        userDetailsController.getUserModelId().then(
              (value) => (value)
                  ? {
                      Get.offAllNamed('/'),
                      Get.find<ProductCategoryController>().fetchCategoryData(),
                      Get.find<CarSpaController>().getAllCarSpaCategory(),
                      Get.find<MechanicalController>().getMechanicalCategory(),
                      Get.find<QuickHelpController>()
                          .getQuickHelpCategoryData(),
                      Get.find<CurrentLocationController>().userChanged.value =
                          false,
                      Get.find<CurrentLocationController>().getUserLocation()
                    }
                  : {
                      Get.off(() => UserDetailsUpdate(
                            isEdit: false,
                          )),
                      Get.find<ProductCategoryController>().fetchCategoryData(),
                      Get.find<CarSpaController>().getAllCarSpaCategory(),
                      Get.find<MechanicalController>().getMechanicalCategory(),
                      Get.find<QuickHelpController>()
                          .getQuickHelpCategoryData(),
                      Get.find<CurrentLocationController>().userChanged.value =
                          false,
                      Get.find<CurrentLocationController>().getUserLocation()
                    },
            );
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      customSnackBar(
          context, 'Enable location services for better optimisation');
      userDetailsController.getUserModelId().then(
            (value) => (value)
                ? {
                    Get.offAllNamed('/'),
                    Get.find<ProductCategoryController>().fetchCategoryData(),
                    Get.find<CarSpaController>().getAllCarSpaCategory(),
                    Get.find<MechanicalController>().getMechanicalCategory(),
                    Get.find<QuickHelpController>().getQuickHelpCategoryData(),
                    Get.find<CurrentLocationController>().userChanged.value =
                        false,
                    Get.find<CurrentLocationController>().getUserLocation()
                  }
                : {
                    Get.off(() => UserDetailsUpdate(
                          isEdit: false,
                        )),
                    Get.find<ProductCategoryController>().fetchCategoryData(),
                    Get.find<CarSpaController>().getAllCarSpaCategory(),
                    Get.find<MechanicalController>().getMechanicalCategory(),
                    Get.find<QuickHelpController>().getQuickHelpCategoryData(),
                    Get.find<CurrentLocationController>().userChanged.value =
                        false,
                    Get.find<CurrentLocationController>().getUserLocation()
                  },
          );
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      userDetailsController.getUserModelId().then(
        (value) {
          if (userDetailsController.mailUpdated) {
            Get.offAllNamed('/');
            Get.find<ProductCategoryController>().fetchCategoryData();
            Get.find<CarSpaController>().getAllCarSpaCategory();
            Get.find<MechanicalController>().getMechanicalCategory();
            Get.find<QuickHelpController>().getQuickHelpCategoryData();
            Get.find<CurrentLocationController>().userChanged.value = false;
            Get.find<CurrentLocationController>().getUserLocation();
          } else {
            Get.off(() => UserDetailsUpdate(
                  isEdit: false,
                ));
            Get.find<ProductCategoryController>().fetchCategoryData();
            Get.find<CarSpaController>().getAllCarSpaCategory();
            Get.find<MechanicalController>().getMechanicalCategory();
            Get.find<QuickHelpController>().getQuickHelpCategoryData();
            Get.find<CurrentLocationController>().userChanged.value = false;
            Get.find<CurrentLocationController>().getUserLocation();
          }
        },
      );
    }
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    final number = Get.arguments;
    final controller = Get.find<AuthFactorsController>();
    return Scaffold(
      backgroundColor: blackPrimary,
      body: Obx(() => (Get.find<ConnectivityController>().status.value)
          ? Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 30),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/carSpa/latestlogo.png',
                        width: 180,
                        height: 180,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('OTP Verification',
                              style: largeFont(Colors.grey[400])),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('We had send you a',
                                  style: smallFont(Colors.grey[400])),
                              SizedBox(
                                width: 4,
                              ),
                              Text('One Time Password',
                                  style: mediumFont(Colors.white)),
                            ],
                          ),
                          Text('on your phone number',
                              style: smallFont(Colors.grey[400])),
                          SizedBox(
                            height: 5,
                          ),
                          Bouncing(
                            onPress: () {
                              // Get.to(LoginPage());
                              Navigator.pop(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(number, style: mediumFont(Colors.white)),
                                SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  Icons.edit_outlined,
                                  size: 15,
                                  color: Colors.white,
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Enter OTP',
                            style: mediumFont(Colors.white),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                      Container(
                        width: _width,
                        child: PinCodeTextField(
                            appContext: context,
                            controller: pinController,
                            textStyle: TextStyle(color: Colors.white),
                            length: 6,
                            keyboardType: TextInputType.phone,
                            cursorColor: Colors.white,
                            textInputAction: TextInputAction.done,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            enablePinAutofill: true,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              fieldHeight: 50,
                              fieldWidth: 50,
                              activeFillColor: Colors.grey[400],
                              inactiveFillColor: Colors.grey[400],
                              activeColor: Colors.grey[400],
                              inactiveColor: Colors.grey[400],
                              selectedColor: Colors.grey[400],
                              selectedFillColor: Colors.grey[400],
                            ),
                            onChanged: (value) {},
                            onCompleted: (pin) {
                              Authentication()
                                  .verifyOTP(phone: number, otp: pin)
                                  .then((value) {
                                if (value['status'] == 'OK') {
                                  if (controller.passwordState.value == '') {
                                    _determinePosition().then((value) =>
                                        Get.find<NotificationControllerNew>()
                                            .initializeNotification());
                                  }
                                } else {
                                  Get.snackbar('Error', value['message'],
                                      snackPosition: SnackPosition.TOP,
                                      duration: Duration(seconds: 2),
                                      backgroundColor: Colors.red,
                                      colorText: Colors.white,
                                      snackStyle: SnackStyle.FLOATING);
                                }
                              });
                            }),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Obx(() => Text(
                            controller.passwordState.value,
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Bouncing(
                          onPress: () {
                            if (pinController.text.trim() != null &&
                                pinController.text.trim() != '') {
                              Authentication()
                                  .verifyOTP(
                                      phone: number, otp: pinController.text)
                                  .then((value) {
                                if (value['status'] == 'OK') {
                                  if (controller.passwordState.value == '') {
                                    _determinePosition().then((value) =>
                                        Get.find<NotificationControllerNew>()
                                            .initializeNotification());
                                  }
                                } else {
                                  Get.snackbar('Error', value['message'],
                                      snackPosition: SnackPosition.TOP,
                                      duration: Duration(seconds: 2),
                                      backgroundColor: Colors.red,
                                      colorText: Colors.white,
                                      snackStyle: SnackStyle.FLOATING);
                                }
                              });
                            } else {
                              Get.snackbar('Warning', 'No OTP Found..!',
                                  snackPosition: SnackPosition.TOP,
                                  duration: Duration(seconds: 2),
                                  backgroundColor: Colors.red,
                                  colorText: Colors.white,
                                  snackStyle: SnackStyle.FLOATING);
                            }
                          },
                          child: Container(
                            height: 40,
                            width: 200,
                            decoration: BoxDecoration(
                              color: botAppBarColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Center(
                              child: Text(
                                'Continue',
                                style: mediumFont(Colors.black),
                              ),
                            ),
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Didn\'t receive the OTP ?',
                            style: smallFont(Colors.grey[400]),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          GestureDetector(
                            onTap: () {
                              Authentication().login(phone: number);
                              customSnackBar(context, 'We had send you the OTP',
                                  isError: false);
                              // address.position();
                            },
                            child: Text(
                              'Resend OTP',
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            )
          : NoInternetScreenView()),
    );
  }
}
