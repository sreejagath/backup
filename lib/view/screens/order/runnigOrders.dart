import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/orderScreenController.dart';
import 'package:pexa_customer/view/screens/order/widget/carShppeRunning.dart';
import 'package:pexa_customer/view/screens/order/widget/carSpaRunning.dart';
import 'package:pexa_customer/view/screens/order/widget/mechRunning.dart';
import 'package:pexa_customer/view/screens/order/widget/quickHelpRunning.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RunningOrdersView extends StatelessWidget {
  RunningOrdersView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orderController = Get.find<OrderScreenController>();
    return GetBuilder<OrderScreenController>(
      init: OrderScreenController(),
      initState: (_) {},
      builder: (_) {
        return Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Obx(
            () => Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      Bouncing(
                        onPress: (orderController.runningIndex.value > 0)
                            ? () {
                                orderController.runningIndex.value =
                                    orderController.runningIndex.value - 1;
                              }
                            : null,
                        child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: blackPrimary,
                          borderRadius: BorderRadius.circular(50),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                            child: Icon(
                          Icons.keyboard_arrow_left,
                          color: Colors.white,
                        ))),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 40,
                      // padding: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          orderController.catList[orderController.runningIndex.value],
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Bouncing(
                    onPress: (orderController.runningIndex.value < 3)
                        ? () {
                            orderController.runningIndex.value =
                                orderController.runningIndex.value + 1;
                          }
                        : null,
                    child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: blackPrimary,
                          borderRadius: BorderRadius.circular(50),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 7,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                            child: Icon(
                          Icons.keyboard_arrow_right,
                          color: Colors.white,
                        ))),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
                child: orderController.runningIndex.value == 0
                    ? CarShoppeRunningOrders()
                    : orderController.runningIndex.value == 1
                        ? CarSpaRunningOrders()
                        : orderController.runningIndex.value == 2
                            ? MechanicalRunningOrders()
                            : QuickHelpRunningOrders())
          ],
        ),
      ),
    ); 
      },
    );
  }
}
