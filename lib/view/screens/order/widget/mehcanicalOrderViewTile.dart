import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalUserOrder.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/order/mechanicalOrderDetailedView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MechanicalOrderViewTile extends StatelessWidget {
  MechanicalOrderViewTile({Key key, this.mechanicalOrdersResultData})
      : super(key: key);
  final MechanicalOrdersResultData mechanicalOrdersResultData;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(() => MechanicalOrderDetailedView(
              mechanicalOrdersResultData: mechanicalOrdersResultData,
              isRunning: (mechanicalOrdersResultData.status == "Active")
                  ? true
                  : false,
            ));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              padding: EdgeInsets.all(5),
              child: Center(
                child: CustomImage(
                  image: mechanicalOrdersResultData.serviceId.thumbUrl[0],
                  fit: BoxFit.contain,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 25,
                    child: Row(
                      children: [
                        Spacer(),
                        Container(
                          height: 25,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          decoration: BoxDecoration(
                            color: mechanicalOrdersResultData.status ==
                                'Active'
                                ? Colors.blue
                                : mechanicalOrdersResultData.status ==
                                'Cancelled'
                                ? Colors.red
                                : mechanicalOrdersResultData.status ==
                                'Completed'
                                ? Colors.green
                                : mechanicalOrdersResultData
                                .status ==
                                'Accepted'
                                ? Colors.yellow[900]
                                : blackPrimary,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              mechanicalOrdersResultData.status.toString(),
                              style: smallFontW600(Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                      width: 160,
                      child: Text(
                        mechanicalOrdersResultData.serviceId.name
                            .toString()
                            .toUpperCase(),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: mediumFont(Colors.black),
                      )),
                  SizedBox(height: 5,),
                  Row(
                    children: [
                      Text(
                        "Total : ",
                        style: smallFont(Colors.black),
                      ),
                      Text('₹ ' + mechanicalOrdersResultData.price.toString(),
                          style: smallFontW600(Colors.black))
                    ],
                  ),
                  SizedBox(height: 2,),
                  Row(
                    children: [
                      Text("Time Slot : ", style: smallFont(Colors.black)),
                      Text(
                        mechanicalOrdersResultData.timeSlot,
                        style: smallFontW600(Colors.black),
                      )
                    ],
                  ),
                  SizedBox(height: 2,),
                  Row(
                    children: [
                      Text("Date : ", style: smallFont(Colors.black)),
                      Text(
                        mechanicalOrdersResultData.date.toString().substring(0, 10),
                        style: smallFontW600(Colors.black),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
