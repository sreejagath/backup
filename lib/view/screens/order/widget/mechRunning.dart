import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/view/screens/order/widget/mehcanicalOrderViewTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MechanicalRunningOrders extends StatelessWidget {
  MechanicalRunningOrders({Key key}) : super(key: key);

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  int pageNumber = 1;

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 500));
    pageNumber++;
    print(pageNumber);
    changePage(pageNumber);
    _refreshController.loadComplete();
  }

  changePage(int page) async {
    await Get.find<MechanicalController>()
        .getMechanicalRunningOrders(page.toString());
  }

  @override
  Widget build(BuildContext context) {
    Get.find<MechanicalController>().getMechanicalRunningOrders('1');
    return Obx(
      () => Container(
          width: MediaQuery.of(context).size.width,
          child: Get.find<MechanicalController>()
                  .mechanicalOrderListTempRunning
                  .isEmpty
              ? Get.find<MechanicalController>().isFound.value
                  ? Center(
                      child: LoadingAnimationWidget.twistingDots(
                        leftDotColor: const Color(0xFF4B4B4D),
                        rightDotColor: const Color(0xFFf7d417),
                        size: 50,
                      ),
                    )
                  : Center(
                      child: Text("Not found"),
                    )
              : SmartRefresher(
                  controller: _refreshController,
                  enablePullDown: false,
                  enablePullUp: true,
                  onLoading: (Get.find<MechanicalController>()
                              .mechanicalOrderListRunning
                              .length ==
                          20)
                      ? _onLoading
                      : () async {
                          await Future.delayed(Duration(milliseconds: 500), () {
                            _refreshController.loadComplete();
                          });
                        },
                  child: ListView.builder(
                      itemCount: Get.find<MechanicalController>()
                          .mechanicalOrderListTempRunning
                          .length,
                      itemBuilder: (context, index) {
                        return MechanicalOrderViewTile(
                            mechanicalOrdersResultData:
                                Get.find<MechanicalController>()
                                    .mechanicalOrderListTempRunning[index]);
                      }))),
    );
  }
}
