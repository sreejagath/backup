import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/view/screens/order/widget/quickHelpOrderViewTile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class QuickHelpHistoryOrders extends StatelessWidget {
  QuickHelpHistoryOrders({Key key}) : super(key: key);

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  int pageNumber = 1;

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 500));
    pageNumber++;
    print(pageNumber);
    changePage(pageNumber);
    _refreshController.loadComplete();
  }

  changePage(int page) async {
    await Get.find<QuickHelpController>()
        .getQuickHelpHistoryOrders(page.toString());
  }

  @override
  Widget build(BuildContext context) {
    Get.find<QuickHelpController>().getQuickHelpHistoryOrders('1');
    return Obx(
      () => Container(
          width: MediaQuery.of(context).size.width,
          child: Get.find<QuickHelpController>().quickHelpHistoryOrders.isEmpty
              ? Get.find<QuickHelpController>().isFound.value
                  ? Center(
                      child: LoadingAnimationWidget.twistingDots(
                        leftDotColor: const Color(0xFF4B4B4D),
                        rightDotColor: const Color(0xFFf7d417),
                        size: 50,
                      ),
                    )
                  : Center(
                      child: Text("Not found"),
                    )
              : SmartRefresher(
                  controller: _refreshController,
                  enablePullDown: false,
                  enablePullUp: true,
                  onLoading: (Get.find<QuickHelpController>()
                              .quickHelpHistoryOrders
                              .length ==
                          20)
                      ? _onLoading
                      : () async {
                          await Future.delayed(Duration(milliseconds: 500), () {
                            _refreshController.loadComplete();
                          });
                        },
                  child: ListView.builder(
                      itemCount: Get.find<QuickHelpController>()
                          .quickHelpHistoryOrdersTemp
                          .length,
                      itemBuilder: (context, index) {
                        return QuickHelpOrderViewTile(
                            quickHelpResultData: Get.find<QuickHelpController>()
                                .quickHelpHistoryOrdersTemp[index]);
                      }))),
    );
  }
}
