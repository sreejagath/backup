import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/models/QucikHelp/quickHelpOrderModel.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/widgets/bouncing.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuickHelpOrderDetailedView extends StatelessWidget {
  QuickHelpOrderDetailedView(
      {Key key, this.quickHelpResultData, this.isRunning})
      : super(key: key);
  final QuickHelpResultData quickHelpResultData;
  final bool isRunning;

  Future goBack(BuildContext alertContext, BuildContext context) async {
    Navigator.pop(alertContext);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
              size: 15,
            )),
        title: Text(
          "Order Details",
          style: mediumFont(Colors.black),
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          "Ord id : ",
                          style: smallFont(Colors.grey),
                        ),
                        Text(quickHelpResultData.id,
                            style: smallFontW600(Colors.black))
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: SizedBox(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                IntrinsicWidth(
                                  child: Container(
                                    height: 25,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    decoration: BoxDecoration(
                                      color: quickHelpResultData.status ==
                                              'Active'
                                          ? Colors.blue
                                          : quickHelpResultData.status ==
                                                  'Cancelled'
                                              ? Colors.red
                                              : quickHelpResultData.status ==
                                                      'Completed'
                                                  ? Colors.green
                                                  : quickHelpResultData
                                                              .status ==
                                                          'Accepted'
                                                      ? Colors.yellow[900]
                                                      : blackPrimary,
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        quickHelpResultData.status,
                                        style: smallFontW600(Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Name : ",
                                      style: smallFont(Colors.grey),
                                    ),
                                    SizedBox(
                                        width: 160,
                                        child: Text(
                                            quickHelpResultData.serviceId.name
                                                .toUpperCase(),
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                            style: smallFontW600(Colors.black)))
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Total : ",
                                      style: smallFont(Colors.grey),
                                    ),
                                    Text(
                                        '₹ ' +
                                            quickHelpResultData.serviceId.price
                                                .toString(),
                                        style: smallFontW600(Colors.black))
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Date : ",
                                      style: smallFont(Colors.grey),
                                    ),
                                    Text(
                                        '₹ ' +
                                            quickHelpResultData.createdAt
                                                .toString().substring(0,10),
                                        style: smallFontW600(Colors.black))
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Time Slot : ",
                                      style: smallFont(Colors.grey),
                                    ),
                                    Text(
                                      quickHelpResultData.timeSlot,
                                      style: smallFontW600(Colors.black),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Bouncing(
                          onPress: () {},
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            padding: EdgeInsets.all(5),
                            child: Center(
                              child: CustomImage(
                                image:
                                    quickHelpResultData.serviceId.thumbUrl[0],
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Given Address Details : ",
                      style: smallFontW600(Colors.grey),
                    ),
                    Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 160,
                      child: Text(
                        quickHelpResultData.address.name.toUpperCase(),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16),
                        maxLines: 2,
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    SizedBox(
                      width: 160,
                      child: Text(
                        quickHelpResultData.address.house.toUpperCase(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: smallFontW600(Colors.black),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    SizedBox(
                      width: 200,
                      child: Text(
                        quickHelpResultData.address.street.toUpperCase(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 5,
                        style: smallFont(Colors.black),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(quickHelpResultData.address.pincode.toString(),
                        style: smallFontW600(Colors.black))
                  ],
                ),
              ),
              isRunning
                  ? SizedBox(
                      height: 15,
                    )
                  : SizedBox(),
              isRunning
                  ? Bouncing(
                      onPress: () {
                        showDialog<void>(
                          context: context,
                          builder: (BuildContext alertContext) {
                            return AlertDialog(
                              title: Text(
                                'Alert..!',
                                style: largeFont(Colors.red),
                              ),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text(
                                      'are you sure to cancel this order..?',
                                      style: mediumFont(Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: Text(
                                    'Yes',
                                    style: largeFont(Colors.black),
                                  ),
                                  onPressed: () {
                                    print(quickHelpResultData.id);
                                    Get.find<QuickHelpController>()
                                        .cancelQuickHelpOrder(
                                            quickHelpResultData.id)
                                        .then((value) => goBack(
                                                alertContext, context)
                                            .then((value) =>
                                                Get.find<QuickHelpController>()
                                                    .getQuickHelpRunningOrders(
                                                        '1')));
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Center(
                          child: IntrinsicWidth(
                            child: Container(
                              height: 40,
                              width: 150,
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: Text(
                                  "Cancel the Order",
                                  style: mediumFont(Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
