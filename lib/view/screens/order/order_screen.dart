import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/orderScreenController.dart';
import 'package:pexa_customer/helper/responsive_helper.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/custom_app_bar.dart';
import 'package:pexa_customer/view/base/not_logged_in_screen.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/order/pastOrder.dart';
import 'package:pexa_customer/view/screens/order/runnigOrders.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen>
    with TickerProviderStateMixin {
  TabController _tabController;
  bool _isLoggedIn;
  AuthFactorsController controller = Get.find();

  @override
  void initState() {
    super.initState();
    _isLoggedIn = controller.isLoggedIn;
    if (_isLoggedIn) {
      _tabController = TabController(length: 2, initialIndex: 0, vsync: this);
    }
  }

  @override
  Widget build(BuildContext context) {
    final orderController = Get.find<OrderScreenController>();
    orderController.runningIndex.value = 0;
    orderController.historyIndex.value = 0;
    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            appBar: CustomAppBar(
                title: 'My Orders'.tr,
                isBackButtonExist: ResponsiveHelper.isDesktop(context)),
            body: _isLoggedIn
                ? Column(
                    children: [
                      Center(
                        child: Container(
                          width: Dimensions.WEB_MAX_WIDTH,
                          color: Theme.of(context).cardColor,
                          child: TabBar(
                            controller: _tabController,
                            indicatorColor: Theme.of(context).primaryColor,
                            indicatorWeight: 3,
                            labelColor: Theme.of(context).primaryColor,
                            unselectedLabelColor:
                                Theme.of(context).disabledColor,
                            unselectedLabelStyle: smallFont(Colors.grey),
                            labelStyle: smallFontW600(Colors.black),
                            tabs: [
                              Tab(text: 'Running'.tr),
                              Tab(text: 'History'.tr),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                          child: TabBarView(
                        controller: _tabController,
                        children: [RunningOrdersView(), OrderHystoryView()],
                      )),
                    ],
                  )
                : NotLoggedInScreen(),
          )
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
