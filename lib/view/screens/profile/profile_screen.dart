import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/util/dimensions.dart';
import 'package:pexa_customer/view/base/custom_image.dart';
import 'package:pexa_customer/view/screens/No%20internet/noInternetScreen.dart';
import 'package:pexa_customer/view/screens/address_edit/addressDetailsPage.dart';
import 'package:pexa_customer/view/screens/home/widget/banner.dart';
import 'package:pexa_customer/view/screens/profile/userProfilePage.dart';
import 'package:pexa_customer/view/screens/profile/widget/profile_bg_widget.dart';
import 'package:pexa_customer/view/screens/profile/widget/profile_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final carModelController = Get.find<CarModelController>();
    final addressController = Get.find<AddressControllerFile>();
    bool _isLoggedIn = Get.find<AuthFactorsController>().isLoggedIn;

    return Obx(() => (Get.find<ConnectivityController>().status.value)
        ? Scaffold(
            backgroundColor: Theme.of(context).cardColor,
            body: ProfileBgWidget(
              backButton: true,
              circularImage: Container(
                decoration: BoxDecoration(
                  border:
                      Border.all(width: 2, color: Theme.of(context).cardColor),
                  shape: BoxShape.circle,
                ),
                alignment: Alignment.center,
                child: ClipOval(
                    child: CustomImage(
                  image:
                      'https://www.seekpng.com/png/detail/428-4287240_no-avatar-user-circle-icon-png.png',
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                )),
              ),
              mainWidget: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Center(
                      child: Container(
                    width: Dimensions.WEB_MAX_WIDTH,
                    color: Theme.of(context).cardColor,
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    child: Column(children: [
                      _isLoggedIn
                          ? Obx(
                              () => Text(
                                (Get.find<UserDetailsController>()
                                                .userName
                                                .value !=
                                            '' &&
                                        Get.find<UserDetailsController>()
                                                .userName
                                                .value !=
                                            null)
                                    ? Get.find<UserDetailsController>()
                                        .userName
                                        .value.toUpperCase()
                                    : Get.find<AuthFactorsController>()
                                        .phoneNumber
                                        .value
                                        .toString(),
                                style: mediumFont(Colors.black),
                              ),
                            )
                          : Text(
                              'Guest User',
                              style: mediumFont(Colors.black),
                            ),
                      SizedBox(height: _isLoggedIn ? 30 : 0),
                      _isLoggedIn
                          ? Obx(() => ProfileButton(
                              icon: Icons.car_repair,
                              title:
                                  " ${carModelController.carBrandName.value} ${carModelController.carModelName.value}",
                              suffixIcon: Icons.edit,
                              onTap: () {
                                if (carModelController.carModel.isEmpty) {
                                  carModelController.fetchData();
                                }
                                carSelectionBottomSheet(context);
                              }))
                          : SizedBox(),
                      // SizedBox(
                      //     height:
                      //         _isLoggedIn ? Dimensions.PADDING_SIZE_SMALL : 0),
                      // _isLoggedIn
                      //     ? ProfileButton(
                      //         icon: Icons.notifications,
                      //         title: 'NOTIFICATION',
                      //         isButtonActive: true,
                      //         onTap: () {},
                      //       )
                      //     : SizedBox(),
                      SizedBox(
                          height:
                              _isLoggedIn ? Dimensions.PADDING_SIZE_SMALL : 0),
                      _isLoggedIn
                          ? ProfileButton(
                              icon: Icons.location_city,
                              title: 'MANAGE ADDRESS',
                              onTap: () {
                                addressController.getAddress();
                                Get.find<AddressControllerFile>()
                                    .fromPexaShoppe
                                    .value = false;
                                Get.to(() => AddressDetailsPage(
                                      backContext: context,
                                    ));
                              })
                          : SizedBox(),
                      SizedBox(
                          height:
                              _isLoggedIn ? Dimensions.PADDING_SIZE_SMALL : 0),
                      _isLoggedIn
                          ? ProfileButton(
                              icon: Icons.person,
                              title: 'USER DETAILS',
                              onTap: () {
                                addressController.getAddress();
                                Get.find<AddressControllerFile>()
                                    .fromPexaShoppe
                                    .value = false;
                                Get.to(() => UserProfilePage());
                              })
                          : SizedBox()
                    ]),
                  ))),
              isFromOther: null,
            ))
        : Scaffold(
            body: NoInternetScreenView(),
          ));
  }
}
