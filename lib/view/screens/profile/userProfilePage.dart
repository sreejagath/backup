import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/view/screens/profile/userDetailsUpdatePage.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class UserProfilePage extends StatelessWidget {
  UserProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("User Details", style: mediumFont(Colors.black)),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: 15,
          ),
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).cardColor,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Center(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                      child: Image.asset(
                    'assets/carSpa/latestlogo.png',
                    height: 100,
                  ))),
              SizedBox(
                height: 20,
              ),
              Text(
                'Full Name',
                style: mediumFont(Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    border: Border.all(color: Colors.black)),
                child: Center(
                  child: Text(
                    Get.find<UserDetailsController>().userName.value,
                    style: largeFont(Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Email Id',
                style: mediumFont(Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    border: Border.all(color: Colors.black)),
                child: Center(
                  child: Text(
                    Get.find<UserDetailsController>().userEmail.value,
                    style: largeFont(Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Phone Number',
                style: mediumFont(Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    border: Border.all(color: Colors.black)),
                child: Center(
                  child: Text(
                    Get.find<AuthFactorsController>()
                        .phoneNumber
                        .value
                        .toString(),
                    style: largeFont(Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Bouncing(
                    onPress: () {
                      Get.to(() => UserDetailsUpdate(
                            isEdit: true,
                          ));
                    },
                    child: Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                          color: botAppBarColor,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: Center(
                        child: Text(
                          'Edit',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}
