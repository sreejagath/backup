import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/constants/color.dart';
import 'package:pexa_customer/constants/new_fonts.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/widgets/bouncing.dart';

class UserDetailsUpdate extends StatefulWidget {
  UserDetailsUpdate({Key key, this.isEdit}) : super(key: key);
  final bool isEdit;

  @override
  State<UserDetailsUpdate> createState() => _UserDetailsUpdateState();
}

class _UserDetailsUpdateState extends State<UserDetailsUpdate> {
  TextEditingController userNameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  bool validateEmail(String emailId) {
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(emailId);
    return emailValid;
  }

  showMessage(String title, String body, Color bgColor, Color textColor) {
    Get.snackbar(title, body,
        snackPosition: SnackPosition.TOP,
        duration: Duration(seconds: 2),
        backgroundColor: bgColor,
        colorText: textColor,
        snackStyle: SnackStyle.FLOATING);
  }

  @override
  void initState() {
    super.initState();
    if (widget.isEdit) {
      setState(() {
        userNameController.text =
            Get.find<UserDetailsController>().userName.value;
        emailController.text =
            Get.find<UserDetailsController>().userEmail.value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isEdit
          ? AppBar(
              title: Text("User Details", style: mediumFont(Colors.black)),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 15,
                ),
                color: Theme.of(context).textTheme.bodyText1.color,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              backgroundColor: Theme.of(context).cardColor,
              elevation: 0,
            )
          : null,
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Center(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                      child: Image.asset(
                    'assets/carSpa/latestlogo.png',
                    height: 100,
                  ))),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                      child: widget.isEdit
                          ? Text(
                              'Update the Profile',
                              style: largeFont(Colors.black),
                            )
                          : Text(
                              'Complete the Profile',
                              style: largeFont(Colors.black),
                            ))),
              SizedBox(
                height: 20,
              ),
              Text(
                'Full Name*',
                style: mediumFont(Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    border: Border.all(color: Colors.black)),
                child: TextField(
                  controller: userNameController,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                      border: InputBorder.none, hintText: 'Enter the Name'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Email Id*',
                style: mediumFont(Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    border: Border.all(color: Colors.black)),
                child: TextField(
                  controller: emailController,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                      border: InputBorder.none, hintText: 'Enter the Email Id'),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Bouncing(
                    onPress: () {
                      if ((emailController.text.trim() != null &&
                              emailController.text.trim() != '') &&
                          (userNameController.text.trim() != null &&
                              userNameController.text.trim() != '')) {
                        if (validateEmail(
                            emailController.text.trim().toString())) {
                          Get.find<UserDetailsController>()
                              .updateUserDetails(
                                  emailController.text.trim().toString(),
                                  userNameController.text.trim().toString())
                              .then((value) {
                            if (value) {
                              Get.offAllNamed('/');
                            } else {
                              Get.snackbar('Error', 'Something went wrong',
                                  snackPosition: SnackPosition.TOP,
                                  duration: Duration(seconds: 2),
                                  backgroundColor: Colors.red,
                                  colorText: Colors.white,
                                  snackStyle: SnackStyle.FLOATING);
                              Get.offAllNamed('/');
                            }
                          });
                        } else {
                          showMessage('Warning', 'Enter a valid Email Id...!',
                              Colors.red, Colors.white);
                        }
                      } else {
                        showMessage('Warning', 'Enter all Details...!',
                            Colors.red, Colors.white);
                      }
                    },
                    child: Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                          color: botAppBarColor,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: Center(
                        child: Text(
                          widget.isEdit ? 'Update' : 'Save',
                          style: mediumFont(Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        )),
      ),
    );
  }
}
