import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/user/userDetails.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class UserDetailsApi {
  var controller = Get.find<AuthFactorsController>();

  Future fetchUserDetails() async {
    final response = await http.get(
      Uri.parse(baseUrl + '/user/id/' + controller.userId.value),
      headers: {"Authorization": 'Bearer ' + controller.token.value},
    );
    try {
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return userDetailsFromJson(json);
      } else {
        print(response.body);
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future addModelIdToProfile(String modelId, String carType) async {
    try {
      final apiUrl = Uri.parse(baseUrl + '/user/' + controller.userId.value);
      final response = await http.put(apiUrl, body: {
        'model_id': modelId,
        'carType': carType
      }, headers: {
        "Authorization": 'Bearer ' + controller.token.value,
      });
      if (response.statusCode == 200) {
        return (response.body);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future updateDetailsUser(String email, String name) async {
    print('email: $email');
    print('name: $name');
    try {
      final response = await http.put(
        Uri.parse(baseUrl + '/user/' + controller.userId.value),
        body: jsonEncode({'email': email, 'name': name}),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
          "Content-Type": "application/json"
        },
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        print(response.body);
        return jsonDecode(response.body);
      } else {
        print(response.body);
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
