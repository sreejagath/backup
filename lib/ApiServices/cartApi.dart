import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/carShoppe/cartListModel.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class CartApi {
  final controller = Get.find<AuthFactorsController>();

  Future addOrUpdateCart(String prodId, int count) async {
    try {
      final response = await http.post(Uri.parse(baseUrl + '/cart/'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body:
              jsonEncode(<String, dynamic>{"product": prodId, "count": count}));
      if (response.statusCode == 200) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<CartListModel> getCartDetails() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/cart/'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return cartListModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future removeCart(String prodId) async {
    try {
      final response = await http.delete(
        Uri.parse(baseUrl + '/cart/product/' + prodId),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return (json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future placeOrder() async {
    var response;
    try {
      response = await http.post(
        Uri.parse(baseUrl + '/order/'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future placeOrderWithCode(String code) async {
    print("here with coupon :" + code);
    var response;
    try {
      response = await http.post(Uri.parse(baseUrl + '/order/'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{"couponCode": code}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
