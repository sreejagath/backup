import 'dart:convert';
import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalModel.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalOfferModel.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalServiceModel.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalTimeSlotModel.dart';
import 'package:pexa_customer/models/Mechanical/mechanicalUserOrder.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class MechanicalAPI {
  final controller = Get.find<AuthFactorsController>();

  Future getMechanicalCategory() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/mechanical-category'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return mechanicalModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getMechanicalService(String catId) async {
    try {
      final response = await http.get(
          Uri.parse(baseUrl + '/mechanical-service/categoryId/' + catId),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
          });
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return mechanicalServiceModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getTimeSlot(String serId, List data, String date) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/mechanical-service/buy'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "id": serId,
            "coordinate": data,
            "date": date
          }));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return mechanicalTimeSlotModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getMechanicalRunningOrderStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/mechanical-order/customer?status=Running&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return mechanicalOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getMechanicalHistoryOrderStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/mechanical-order/customer?status=History&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return mechanicalOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future cancelMechanicalOrder(String ordId) async {
    try {
      final response = await http.put(
          Uri.parse(baseUrl + '/mechanical-order/status/' + ordId),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{"status": "Cancelled"}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future getMechanicalAvailableOffer(String serId, int amount) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/mechanical-offer/availableOffers'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(
              <String, dynamic>{"serviceID": serId, "total": amount}));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return mechanicalOfferModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future applyMechanicalOffer(String serId, int amount, String offerId) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/mechanical-offer/apply'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "offerId": offerId,
            "total": amount,
            "serviceId": serId
          }));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
