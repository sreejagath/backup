import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class NotificationAPI {
  var controller = Get.find<AuthFactorsController>();

  Future initializeNotification(Map<String, dynamic> data) async {
    try {
      final response = await http.post(Uri.parse(baseUrl + '/notification'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(data));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
