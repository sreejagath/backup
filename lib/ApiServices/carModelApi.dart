import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/models/user/carBrand_model.dart';
import 'package:pexa_customer/models/user/carModels_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiData {
  var controller = Get.find<AuthFactorsController>();
  var carModelController = Get.find<CarModelController>();
  var userModelController = Get.find<UserDetailsController>();
  final categoryModelController = Get.find<ProductCategoryController>();

  Future<CarBrandModel> carBrands() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/make/'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return carBrandModelFromJson(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<CarModelsModel> fetchCarModels(String id) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/model/make/$id'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return carModelsModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future carModelUpdateUser(String id) async {
    try {
      final response = await http.post(
        Uri.parse(baseUrl + '/user/' + controller.userId.value),
        body: {'model': id},
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
      } else {
        throw Exception('Failed to load ');
      }
    } catch (e) {}
  }

  Future fetchCarModelDetail(String modelId) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/model/id/$modelId'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        carModelController.carModelName.value =
            jsonResponse['resultData']['name'];
        return jsonDecode(response.body);
      } else {
        return null;
      }
    } catch (e) {
      print(e);
    }
  }

  Future fetchCarBrandDetail(String modelId) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/make/id/$modelId'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        carModelController.carBrandName.value =
            jsonResponse['resultData']['name'];
        userModelController.isMIdAvailable.value = true;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
    }
  }
}
