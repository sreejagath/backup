import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CouponApi {
  var controller = Get.find<AuthFactorsController>();

  Future checkCoupon(double amount, String code) async {
    try {
      final response =
          await http.post(Uri.parse(baseUrl + '/coupon/code/' + code),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(<String, dynamic>{"amount": amount}));
      if (response.statusCode == 200) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
