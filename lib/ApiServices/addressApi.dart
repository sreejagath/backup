import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/user/addressModel.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AddressApi {
  var controller = Get.find<AuthFactorsController>();

  Future addAddress(Map<String, dynamic> data) async {
    try {
      final response = await http.post(
        Uri.parse(baseUrl + '/address'),
        body: jsonEncode(data),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
          "Content-Type": "application/json",
        },
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        return jsonDecode(response.body);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  Future getAllAdress() async {
    final response = await http.get(
      Uri.parse(baseUrl + '/address/all'),
      headers: {
        "Authorization": 'Bearer ' + controller.token.value,
      },
    );
    try {
      if (response.statusCode == 200) {
        var json = response.body;
        print(json);
        return addressListModelFromJson(json);
      } else {
        print(response.body);
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future deleteAddress(String addressId) async {
    try {
      final response = await http.delete(
        Uri.parse(baseUrl + '/address/' + addressId),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if (response.statusCode == 200) {
        return jsonDecode(response.body);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  Future editAddress(String addressId, Map<String, dynamic> data) async {
    try {
      final response =
          await http.put(Uri.parse(baseUrl + '/address/' + addressId),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(data));
      if (response.statusCode == 200) {
        return jsonDecode(response.body);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  Future setDefaultAddress(String addressId) async {
    try {
      final response = await http.put(
        Uri.parse(baseUrl + '/address/default/' + addressId),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
          "Content-Type": "application/json",
        },
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        return jsonDecode(response.body);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  Future getDefaultAddress() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/address'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
