import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class SplashAPI {
  final authController = Get.find<AuthFactorsController>();
  var serverEnabled = false.obs;

  Future checkServer() async {
    try {
      final response = await http.post(
        Uri.parse('https://shoppe.carclenx.com/'),
        // headers: {"Authorization": 'Bearer ' + authController.token.value},
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        serverEnabled.value = true;
        return serverEnabled.value;
      } else {
        serverEnabled.value = false;
        return null;
      }
    } catch (e) {
      print('server error');
      serverEnabled.value = false;
      return null;
    }
  }
}
