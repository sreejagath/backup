import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class GuestLoginApi {
  var controller = Get.find<AuthFactorsController>();

  Future guestLogin() async {
    try {
      final response = await http.post(
        Uri.parse(baseUrl + '/auth/guest/login'),
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future guestLogout(String phoneNumber) async {
    try {
      final response =
          await http.post(Uri.parse(baseUrl + '/auth/guest/logout'),
              headers: {
                "Content-Type": "application/json",
              },
              body: jsonEncode(<String, dynamic>{"phone": phoneNumber}));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
