import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/carShoppe/category_Model.dart';
import 'package:pexa_customer/models/carShoppe/productListModel.dart';
import 'package:pexa_customer/models/carShoppe/searchModel.dart';
import 'package:pexa_customer/models/carShoppe/shippingModel.dart';
import 'package:pexa_customer/models/carShoppe/shoppeOrdersModel.dart';
import 'package:pexa_customer/models/carShoppe/subCategory_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ProductAPI {
  var controller = Get.find<AuthFactorsController>();

  Future categoryAPI() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/product-category/'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return categoryModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future subCategoryAPI(String catId) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/product-sub-category/category/' + catId),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if ((response.statusCode == 200) | (response.statusCode == 201)) {
        var json = response.body;
        return subCategoryModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getProductList(String subId, List data, String page) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/product/getBySubCategory/' + page),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(
              <String, dynamic>{"sub_category_id": subId, "location": data}));
      if (response.statusCode == 200) {
        var json = response.body;
        return productListModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getProductDetails(String prodId) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/product/id/' + prodId),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getRunningOrderStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/order/customer?status=Running&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return shoppeOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getHistoryOrderStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/order/customer?status=History&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return shoppeOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future cancelOrder(String ordId) async {
    try {
      final response = await http.put(Uri.parse(baseUrl + '/order/id/' + ordId),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{"status": "Cancelled"}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future buyNowProduct(Map<String, dynamic> body) async {
    print(body);
    try {
      final response = await http.post(Uri.parse(baseUrl + '/order/buyNow/'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(body));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future getShippingDetails() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/shipping'),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        print(json);
        return shippingModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future searchProduct(List data, String value) async {
    try {
      final response = await http.post(Uri.parse(baseUrl + '/product/search'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body:
              jsonEncode(<String, dynamic>{"query": value, "location": data}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return pexaSearchModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
