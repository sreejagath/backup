import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ServiceCheckOutApi {
  final controller = Get.find<AuthFactorsController>();

  Future placeOrder(Map<String, dynamic> data) async {
    print(data);
    try {
      final response =
          await http.post(Uri.parse(baseUrl + '/carspa-order/checkout'),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(data));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future placeMechanicalOrder(Map<String, dynamic> data) async {
    try {
      final response =
          await http.post(Uri.parse(baseUrl + '/mechanical-order/checkout'),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(data));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
