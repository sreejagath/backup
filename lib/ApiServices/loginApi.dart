import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class Authentication {
  var controller = Get.find<AuthFactorsController>();
  var userDetailsController = Get.find<UserDetailsController>();

  Future login({String phone}) async {
    final response = await http.post(
      Uri.parse(baseUrl + '/auth/login'),
      body: {'phone': phone, 'loginType': 'customer'},
      encoding: Encoding.getByName('utf-8'),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    );
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['status'] == "OK") {
        controller.phoneNumberState.value = '';
      } else {
        controller.phoneNumberState.value = 'Invalid Number';
      }
      return jsonDecode(response.body);
    } else {
      controller.phoneNumberState.value = 'Invalid Number';
    }
  }

  Future verifyOTP({String phone, String otp}) async {
    final request = await http.post(
      Uri.parse(baseUrl + '/auth/verify-otp'),
      body: {'phone': phone, 'otpCode': otp},
      encoding: Encoding.getByName('utf-8'),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    );
    if ((request.statusCode == 200) || (request.statusCode == 201)) {
      var jsonResponse = json.decode(request.body);
      if (jsonResponse['status'] == "OK") {
        controller.passwordState.value = '';
        controller.token.value =
            jsonResponse['resultData']['userToken'].toString();
        controller.userId.value = jsonResponse['resultData']['_id'].toString();
        controller.phoneNumber.value =
            jsonResponse['resultData']['phone'].toString();
        await controller
            .saveUserToken(jsonResponse['resultData']['userToken'], true)
            .then((value) => controller
                .saveUserId(jsonResponse['resultData']['_id'], true)
                .then((value) => controller.savePhone(
                    jsonResponse['resultData']['phone'], true)));
        return jsonResponse;
      } else {
        controller.passwordState.value = 'Invalid OTP';
      }
    } else {
      return json.decode(request.body);
    }
  }
}
