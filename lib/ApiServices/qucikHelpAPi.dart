import 'dart:convert';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/QucikHelp/qucikHelpCategory.dart';
import 'package:pexa_customer/models/QucikHelp/quickHelpOrderModel.dart';
import 'package:pexa_customer/models/QucikHelp/quickHelpServiceModel.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class QuickHelpAPI {
  final controller = Get.find<AuthFactorsController>();

  Future getQucikHelpCategory() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/quickhelp-category/'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return quickHelpCategoryModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getQuickHelpService(String catId) async {
    try {
      final response = await http.get(
          Uri.parse(baseUrl + '/quickhelp-service/categoryId/' + catId),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
          });
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return quickHelpServiceModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future quickHelpBuyNow(String id, List data) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/quickhelp-service/buy'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{"id": id, "coordinate": data}));
      if (response.statusCode == 200) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode((response.body));
      }
    } catch (e) {
      return null;
    }
  }

  Future getQuickHelpOrderRunningStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/quickhelp-order/customer?status=Running&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return quickHelpOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getQuickHelpOrderHistoryStatus(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/quickhelp-order/customer?status=History&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return quickHelpOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future cancelQuickHelpOrder(String ordId) async {
    try {
      final response = await http.put(
          Uri.parse(baseUrl + '/quickhelp-order/status/' + ordId),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{"status": "Cancelled"}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
