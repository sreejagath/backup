import 'dart:async';

import 'package:pexa_customer/constants/api.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/models/carSpa/carSpaModel.dart';
import 'package:pexa_customer/models/carSpa/carSpaOfferModel.dart';
import 'package:pexa_customer/models/carSpa/carSpaOrderModel.dart';
import 'package:pexa_customer/models/carSpa/carSpaServiceModel.dart';
import 'package:pexa_customer/models/carSpa/timeSlotModel.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CarSpaApi {
  final controller = Get.find<AuthFactorsController>();

  Future<CarSpaModel> getAllCarSpaCategory() async {
    try {
      final response = await http.get(
        Uri.parse(baseUrl + '/category'),
        headers: {
          "Authorization": 'Bearer ' + controller.token.value,
        },
      );
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return carSpaModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future getServicesWithCarType(String catId) async {
    try {
      final response = await http
          .get(Uri.parse(baseUrl + '/service/categoryId/' + catId), headers: {
        "Authorization": 'Bearer ' + controller.token.value,
      });
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return carSpaServiceModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getServicesWithoutCarType(String catId) async {
    try {
      final response =
          await http.post(Uri.parse(baseUrl + '/service/getservices'),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(<String, dynamic>{
                "id": catId,
              }));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return carSpaServiceModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getTimeSlot(String serId, String date) async {
    try {
      final response = await http.post(Uri.parse(baseUrl + '/service/buy'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "id": serId,
            "coordinate": Get.find<CurrentLocationController>().currentPosition,
            "date": date
          }));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return timeSlotModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getCarSpaRunningOrderList(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/carspa-order/customer?status=Running&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return carSpaOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future getCarSpaHistoryOrderList(String page) async {
    try {
      final response = await http.get(
        Uri.parse(
            baseUrl + '/carspa-order/customer?status=History&page=' + page),
        headers: {"Authorization": 'Bearer ' + controller.token.value},
      );
      if (response.statusCode == 200) {
        var json = response.body;
        return carSpaOrderModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future cancelCarSpaOrder(String ordId) async {
    try {
      final response =
          await http.put(Uri.parse(baseUrl + '/carspa-order/status/' + ordId),
              headers: {
                "Authorization": 'Bearer ' + controller.token.value,
                "Content-Type": "application/json",
              },
              body: jsonEncode(<String, dynamic>{"status": "Cancelled"}));
      if (response.statusCode == 200 || response.statusCode == 201) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }

  Future getCarSpaAvailableOffers(String serId, int amount) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/carspa-offer/availableOffers'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(
              <String, dynamic>{"serviceID": serId, "total": amount}));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return carSpaOfferModelFromJson(json);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future applyCarSpaOffer(String serId, int amount, String offerId) async {
    try {
      final response = await http.post(
          Uri.parse(baseUrl + '/carspa-offer/apply'),
          headers: {
            "Authorization": 'Bearer ' + controller.token.value,
            "Content-Type": "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "offerId": offerId,
            "total": amount,
            "serviceId": serId
          }));
      if ((response.statusCode == 200) || (response.statusCode == 201)) {
        var json = response.body;
        return jsonDecode(json);
      } else {
        return jsonDecode(response.body);
      }
    } catch (e) {
      return null;
    }
  }
}
