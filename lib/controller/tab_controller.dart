import 'package:get/get.dart';

class TabControllerMethod extends GetxController {
  var tabIndex = 6.obs;
  changeTab(int index) {
    tabIndex.value = index;
    update();
  }
}
