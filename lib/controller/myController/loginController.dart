import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthFactorsController extends GetxController {
  var token = ''.obs;
  var userId = ''.obs;
  var isLoggedIn = false;
  var phoneNumber = ''.obs;
  var phoneNumberState = "".obs;
  final box = GetStorage();
  final boxId = GetStorage();
  final boxPhone = GetStorage();
  var passwordState = "".obs;
  @override
  void onInit() {
    super.onInit();

    token.value = box.read('userToken');

    userId.value = boxId.read('userId');

    phoneNumber.value = boxPhone.read('phone');

    isLoggedIn = token.value != null;
  }

  Future saveUserToken(String token,bool isUser) async {
    box.write("userToken", token);
    isLoggedIn = isUser;
    update();
  }
  Future saveUserId(String userId,bool isUser) async {
    boxId.write("userId", userId);
    isLoggedIn = isUser;
    update();
  }
  Future savePhone(String phoneNumber,bool isUser) async {
    boxPhone.write("phone", phoneNumber);
    isLoggedIn = isUser;
    update();
  }

  Future logOut() async {
    deleteUserToken();
    deleteUserId();
    deleteUserPhone();
    Get.find<CarModelController>().carModelName.value="";
    Get.find<CarModelController>().carBrandName.value="";
    Get.find<UserDetailsController>().isMIdAvailable.value=false;
  }

  deleteUserToken() {
    box.remove("userToken");
    isLoggedIn = false;
    update();
    token.value=null;
    update();
  }
  deleteUserId() {
    boxId.remove("userId");
    isLoggedIn = false;
    update();
    userId.value=null;
    update();
  }
  deleteUserPhone(){
    boxPhone.remove("phone");
    update();
    isLoggedIn = false;
    update();
    phoneNumber.value=null;
    update();
  }

  Future deleteGuestData() async {
    Get.find<CarModelController>().carModelName.value="";
    Get.find<CarModelController>().carBrandName.value="";
    Get.find<UserDetailsController>().isMIdAvailable.value=false;
    token.value = '';
    userId.value = '';
    isLoggedIn=false;
    update();
  }

  clearSharedData() {
    box.erase();
    boxId.erase();
    boxPhone.erase();
    isLoggedIn = false;
    update();
  }
}
