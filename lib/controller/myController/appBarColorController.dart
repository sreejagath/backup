import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeAppBarColorController extends GetxController {
  ScrollController scrollController;
  var currentPosition = 0.0;
  var opacity = 0.0.obs;

  @override
  void onInit() {
    super.onInit();
    scrollController = ScrollController().obs();
    scrollController.addListener(() {
      setOpacity(scrollController.offset);
    });
  }

  setOpacity(double value) {
    currentPosition = value;
    if (currentPosition <= 72) {
      opacity.value = 0.0;
      update();
    } else {
      opacity.value = 0.5;
      update();
    }
  }
}
