import 'package:pexa_customer/ApiServices/carSpaApi.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';

class CarSpaController extends GetxController {
  var carSpaCategory = [].obs;
  var carSpaService = [].obs;
  var carSpaServiceProperty = [].obs;
  var carSpaAddOnTotal = 0.obs;
  var carSpaAddOnRadioState = [].obs;
  var carSpaTimeSlot = [].obs;
  var carSpaAddOns = [].obs;
  var carSpaRunningOrderList = [].obs;
  var carSpaRunningOrderListTemp = [].obs;
  var carSpaHistoryOrderList = [].obs;
  var carSpaHistoryOrderListTemp = [].obs;
  var isFound = true.obs;
  var carSpaOffers = [].obs;
  var offerApplicable = false.obs;
  var offerCouponId = ''.obs;
  var discountedAmount = 0.0.obs;
  var discount = 0.0.obs;
  var offerEmpty = false.obs;
  List carSpaBannerImages = [
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fcar-washing.jpg?alt=media&token=528d3f14-91bb-4919-b88c-84fe8f380719',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fsteaming.jpg?alt=media&token=fc6dda54-c939-44c9-ae2d-285e542eb901',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fsanitization.jpg?alt=media&token=c92a1974-f13f-41d1-a362-82000dfcbeb7',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Finterior-cleaning.jpg?alt=media&token=d252d23d-b42d-4f1a-9d8d-0b289db9652d',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fscratch-removal.jpg?alt=media&token=69189643-936e-4dc7-8614-a7ccde6af4e4',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fwatermark-remo.jpg?alt=media&token=86f23205-3e90-479b-8063-23bf2bbc99a8',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fpoloshing%26-wax.jpg?alt=media&token=8e504e61-15be-45a5-b749-d89536da6003',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fbike.jpg?alt=media&token=4120b145-e27d-4044-a13d-80a77d3453e0',
    'https://firebasestorage.googleapis.com/v0/b/carclenx.appspot.com/o/Pexa%20Shoppe%2Fcarspacategory%2Fhouse-keeping.jpg?alt=media&token=5413429b-93fa-408c-a7a9-135df01cf638'
  ];

  Future getAllCarSpaCategory() async {
    var response = await CarSpaApi().getAllCarSpaCategory();
    if (response.status == 'OK') {
      carSpaCategory.value = response.resultData.toList();
    }
  }

  Future getCarSpaServiceWithCatId(String catId) async {
    if (carSpaService.isNotEmpty) {
      carSpaService.clear();
      update();
    }
    var response = await CarSpaApi().getServicesWithCarType(catId);
    if (response.status == 'OK') {
      carSpaService.value = response.resultData.toList();
      setList();
    }
  }

  Future getCarSpaServiceWithoutCatId(String catId) async {
    if (carSpaService.isNotEmpty) {
      carSpaService.clear();
      update();
    }
    var response = await CarSpaApi().getServicesWithoutCarType(catId);
    if (response.status == 'OK') {
      carSpaService.value = response.resultData.toList();
      setList();
    }
  }

  setList() {
    if (carSpaServiceProperty.isNotEmpty) {
      carSpaServiceProperty.clear();
      update();
    }
    carSpaService.forEach((element) {
      int num = ','.allMatches(element.list).length;
      List data = [];
      for (int i = 0; i <= num; i++) {
        data.add(element.list.split(',')[i]);
      }
      carSpaServiceProperty.add(data);
    });
  }

  setRadioStatusList(int length) {
    if (carSpaAddOnRadioState.isNotEmpty) {
      carSpaAddOnRadioState.clear();
      update();
    }
    for (int i = 0; i < length; i++) {
      carSpaAddOnRadioState.add(false);
    }
  }

  changeTotal(bool state, int value) {
    if (state) {
      carSpaAddOnTotal.value = carSpaAddOnTotal.value + value;
      update();
    } else {
      carSpaAddOnTotal.value = carSpaAddOnTotal.value - value;
      update();
    }
  }

  Future getTimeSlot(String serId, String date) async {
    if (carSpaTimeSlot.isNotEmpty) {
      carSpaTimeSlot.clear();
      update();
    }
    var response = await CarSpaApi().getTimeSlot(serId, date);
    if (response != null) {
      if (response.status == 'OK') {
        carSpaTimeSlot.value = response.result.toList();
        update();
      }
    }
    
    
    if (carSpaTimeSlot.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  addOnADDorRemove(bool value, Map<String, dynamic> data) {
    if (value) {
      carSpaAddOns.add(data);
      update();
      
    } else {
      if (carSpaAddOns.isEmpty) {
        return;
      }
      
      carSpaAddOns.removeWhere(
          (element) => element.toString().contains(data.toString()));
      update();
    }
  }

  Future getCarSpaRunningOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (carSpaRunningOrderListTemp.isNotEmpty) {
        carSpaRunningOrderListTemp.clear();
        update();
      }
    }
    if (carSpaRunningOrderList.isNotEmpty) {
      carSpaRunningOrderList.clear();
      update();
    }
    var response = await CarSpaApi().getCarSpaRunningOrderList(page);
    if (response != null) {
      carSpaRunningOrderList.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status == 'Active') || (element.status == 'Accepted')))
          .toList();
      update();
      if (carSpaRunningOrderList.isEmpty) {
        isFound.value = false;
        update();
      } else {
        carSpaRunningOrderList.forEach((element) {
          carSpaRunningOrderListTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future getCarSpaHistoryOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (carSpaHistoryOrderListTemp.isNotEmpty) {
        carSpaHistoryOrderListTemp.clear();
        update();
      }
    }
    if (carSpaHistoryOrderList.isNotEmpty) {
      carSpaHistoryOrderList.clear();
      update();
    }
    var response = await CarSpaApi().getCarSpaHistoryOrderList(page);
    if (response != null) {
      carSpaHistoryOrderList.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status != 'Active') && (element.status != 'Accepted')))
          .toList();
      update();
      if (carSpaHistoryOrderList.isEmpty) {
        isFound.value = false;
        update();
      } else {
        carSpaHistoryOrderList.forEach((element) {
          carSpaHistoryOrderListTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future getCarSpaAvailableOffers(String serId, int amount) async {
    offerEmpty.value = false;
    update();
    if (carSpaOffers.isNotEmpty) {
      carSpaOffers.clear();
      update();
    }
    var response = await CarSpaApi().getCarSpaAvailableOffers(serId, amount);
    if (response != null) {
      if (response.resultData.isNotEmpty) {
        carSpaOffers.value = response.resultData.toList();
        update();
      } else {
        offerEmpty.value = true;
        update();
      }
    }
  }

  Future applyOfferToService(String serId, int amount, String offerId) async {
    var response = await CarSpaApi().applyCarSpaOffer(serId, amount, offerId);
    
    if (response['status'] == 'OK') {
      if (response['resultData']['applicable']) {
        offerApplicable.value = true;
        discount.value =
            double.parse(response['resultData']['discountAmount'].toString());
        discountedAmount.value =
            double.parse(response['resultData']['total'].toString());
        offerCouponId.value = offerId;
        update();
        Get.find<CouponController>().clearValue();
      } else {
        clearOffer();
      }
      
      
      
    }
  }

  clearOffer() {
    offerApplicable.value = false;
    discountedAmount.value = 0.0;
    discount.value = 0.0;
    offerCouponId.value = '';
    update();
  }
}
