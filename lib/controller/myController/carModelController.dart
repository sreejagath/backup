import 'package:pexa_customer/ApiServices/carModelApi.dart';
import 'package:get/get.dart';

class CarModelController extends GetxController {
  var carModel = [].obs;
  var carModelVarients = [].obs;
  var selectedBrand = 1500.obs;
  var carBrandId = "".obs;
  var carBrandAPIId = "".obs;
  var carModelId = "".obs;
  var carModelType = "".obs;
  var carModelName = "".obs;
  var carBrandName = "".obs;
  var nullString = "".obs;
  var isMakeSearch = false.obs;
  var isModelSearch = false.obs;
  var noMakeFound = false.obs;
  var noModelFound = false.obs;

  void carBrandsSelected(int index) {
    selectedBrand.value = index;
  }

  var selectedModel = 1500.obs;

  void carModelSelected(int index) {
    selectedModel.value = index;
  }

  void carBrandIdChange(String id) {
    carBrandId.value = id;
    update();
  }

  void carModelIdChange(String id, String type) {
    carModelId.value = id;
    carModelType.value = type;
    update();
  }

  Future fetchData() async {
    noMakeFound.value = false;
    update();
    if (carModel.isNotEmpty) {
      carModel.clear();
    }
    var response = await ApiData().carBrands();
    if (response != null) {
      if (response.status == 'OK') {
        carModel.value = response.resultData
            .toList()
            .where((element) => (element.name != 'ALL'))
            .toList();
        if (carModel.length == 0) {
          noMakeFound.value = true;
          update();
        } else {
          noMakeFound.value = false;
          update();
        }
      }
    } else {
      noMakeFound.value = true;
      update();
    }
  }

  Future fetchCarModelVarients(String id) async {
    noModelFound.value = false;
    update();
    if (carModelVarients.isNotEmpty) {
      carModelVarients.clear();
    }
    carBrandAPIId.value = id;
    update();
    var response = await ApiData().fetchCarModels(id);
    if (response.status == 'OK') {
      carModelVarients.value = response.resultData.toList();
      if (carModelVarients.length == 0) {
        noModelFound.value = true;
        update();
      } else {
        noModelFound.value = false;
        update();
      }
    }
  }

  Future<void> searchBrand(String value) async {
    noMakeFound.value = false;
    update();
    if (value == null || value == '') {
      fetchData();
    } else {
      if (carModel.isNotEmpty) {
        carModel.clear();
      }
      var response = await ApiData().carBrands();
      if (response.status == 'OK') {
        carModel.value = response.resultData
            .toList()
            .where((element) =>
                (element.name.startsWith(value)) && (element.name != 'ALL'))
            .toList();
        if (carModel.length == 0) {
          noMakeFound.value = true;
          update();
        } else {
          noMakeFound.value = false;
          update();
        }
      }
    }
  }

  Future<void> searchModel(String value, String id) async {
    noModelFound.value = false;
    update();
    if (value == null || value == '') {
      fetchCarModelVarients(id);
    } else {
      if (carModelVarients.isNotEmpty) {
        carModelVarients.clear();
      }
      var response = await ApiData().fetchCarModels(id);
      if (response.status == 'OK') {
        carModelVarients.value = response.resultData
            .toList()
            .where((element) => (element.name.startsWith(value)))
            .toList();
        if (carModelVarients.length == 0) {
          noModelFound.value = true;
          update();
        } else {
          noModelFound.value = false;
          update();
        }
      }
    }
  }
}
