import 'package:connectivity/connectivity.dart';
import 'package:get/get.dart';

class ConnectivityController extends GetxController{
  var status = true.obs;
  var subscription;

  @override
  void onInit() {
    networkCheck();
    subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      networkCheck();
    });
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  networkCheck()async{
    var connectivityResult= await(Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      status.value=true;
      update();
    }
    else if (connectivityResult == ConnectivityResult.wifi) {
      status.value=true;
      update();
    }
    else if(connectivityResult == ConnectivityResult.none){
      status.value=false;
      update();
    }
  }
}