import 'package:pexa_customer/ApiServices/serviceCheckOutApi.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ServiceCheckOutController extends GetxController{

  var serviceId = "".obs;
  var timeSlot = "".obs;
  var tempData = [].obs;

  Future placeOrder(Map <String,dynamic> data) async{
    var response = await ServiceCheckOutApi().placeOrder(data);
    
    if(response['status']=='OK'){
      Get.snackbar('Success', '${response['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      clearAfterPlaceOrder();
    }
  }

  clearAfterPlaceOrder() {
    final carSpaController = Get.find<CarSpaController>();
    carSpaController.carSpaAddOns.clear();
    update();
    serviceId.value="";
    tempData.clear();
    timeSlot.value="";
    update();
  }

  Future placeMechanicalOrder(Map <String,dynamic> data) async{
    var response = await ServiceCheckOutApi().placeMechanicalOrder(data);
    
    if(response['status']=='OK'){
      Get.snackbar('Success', '${response['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      clearAfterMechanicalPlaceOrder();
    }
  }

  clearAfterMechanicalPlaceOrder() {
    Get.find<MechanicalController>().mechanicalAddOns.clear();
    update();
    serviceId.value="";
    tempData.clear();
    timeSlot.value="";
    update();
  }
}