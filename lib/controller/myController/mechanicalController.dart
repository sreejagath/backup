import 'package:pexa_customer/ApiServices/mechanicalAPI.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';

class MechanicalController extends GetxController {
  var mechanicalCategoryList = [].obs;
  var mechanicalServicesList = [].obs;
  var mechanicalServiceProperty = [].obs;
  var mechanicalAddOnTotal = 0.obs;
  var mechanicalAddOnRadioState = [].obs;
  var mechanicalAddOns = [].obs;
  var mechanicalTimeSlot = [].obs;
  var mechanicalOrderListRunning = [].obs;
  var mechanicalOrderListTempRunning = [].obs;
  var mechanicalOrderListHistory = [].obs;
  var mechanicalOrderListTempHistory = [].obs;
  var isFound = true.obs;
  var offerEmpty = false.obs;
  var mechanicalOffer = [].obs;
  var offerApplicable = false.obs;
  var offerCouponId = ''.obs;
  var discountedAmount = 0.0.obs;
  var discount = 0.0.obs;

  Future getMechanicalCategory() async {
    var response = await MechanicalAPI().getMechanicalCategory();
    if (response.status == 'OK') {
      mechanicalCategoryList.value = response.resultData.toList();
    }
  }

  Future getCarSpaServiceWithCatId(String catId) async {
    if (mechanicalServicesList.isNotEmpty) {
      mechanicalServicesList.clear();
      update();
    }
    var response = await MechanicalAPI().getMechanicalService(catId);
    if (response.status == 'OK') {
      
      mechanicalServicesList.value = response.resultData.toList();
      setList();
    }
  }

  setList() {
    if (mechanicalServiceProperty.isNotEmpty) {
      mechanicalServiceProperty.clear();
      update();
    }
    mechanicalServicesList.forEach((element) {
      int num = ','.allMatches(element.list).length;
      List data = [];
      for (int i = 0; i <= num; i++) {
        data.add(element.list.split(',')[i]);
      }
      mechanicalServiceProperty.add(data);
    });
  }

  setRadioStatusList(int length) {
    if (mechanicalAddOnRadioState.isNotEmpty) {
      mechanicalAddOnRadioState.clear();
      update();
    }
    for (int i = 0; i < length; i++) {
      mechanicalAddOnRadioState.add(false);
    }
    
  }

  addOnADDorRemove(bool value, Map<String, dynamic> data) {
    if (value) {
      mechanicalAddOns.add(data);
      update();
      
    } else {
      if (mechanicalAddOns.isEmpty) {
        return;
      }
      
      mechanicalAddOns.removeWhere(
          (element) => element.toString().contains(data.toString()));
      update();
      
    }
  }

  changeTotal(bool state, int value) {
    if (state) {
      mechanicalAddOnTotal.value = mechanicalAddOnTotal.value + value;
      update();
    } else {
      mechanicalAddOnTotal.value = mechanicalAddOnTotal.value - value;
      update();
    }
  }

  Future getTimeSlot(String serId, List data, String date) async {
    if (mechanicalTimeSlot.isNotEmpty) {
      mechanicalTimeSlot.clear();
      update();
    }
    var response = await MechanicalAPI().getTimeSlot(serId, data, date);
    if (response.status == 'OK') {
      mechanicalTimeSlot.value = response.result.toList();
    }
    
    
    if (mechanicalTimeSlot.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future getMechanicalRunningOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (mechanicalOrderListTempRunning.isNotEmpty) {
        mechanicalOrderListTempRunning.clear();
        update();
      }
    }
    if (mechanicalOrderListRunning.isNotEmpty) {
      mechanicalOrderListRunning.clear();
      update();
    }
    var response = await MechanicalAPI().getMechanicalRunningOrderStatus(page);
    if (response != null) {
      mechanicalOrderListRunning.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status == 'Active') || (element.status == 'Accepted')))
          .toList();
      update();
      if (mechanicalOrderListRunning.isEmpty) {
        isFound.value = false;
        update();
      } else {
        mechanicalOrderListRunning.forEach((element) {
          mechanicalOrderListTempRunning.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future getMechanicalHistoryOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (mechanicalOrderListTempHistory.isNotEmpty) {
        mechanicalOrderListTempHistory.clear();
        update();
      }
    }
    if (mechanicalOrderListHistory.isNotEmpty) {
      mechanicalOrderListHistory.clear();
      update();
    }
    var response = await MechanicalAPI().getMechanicalHistoryOrderStatus(page);
    if (response != null) {
      mechanicalOrderListHistory.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status != 'Active') && (element.status != 'Accepted')))
          .toList();
      update();
      if (mechanicalOrderListHistory.isEmpty) {
        isFound.value = false;
        update();
      } else {
        mechanicalOrderListHistory.forEach((element) {
          mechanicalOrderListTempHistory.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future cancelMechanicalOrder(String ordID) async {
    var response = await MechanicalAPI().cancelMechanicalOrder(ordID);
    
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', 'Order $ordID is cancelled...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
      }
    }
  }

  Future getMechanicalAvailableOffers(String serId, int amount) async {
    offerEmpty.value = false;
    update();
    if (mechanicalOffer.isNotEmpty) {
      mechanicalOffer.clear();
      update();
    }
    var response =
        await MechanicalAPI().getMechanicalAvailableOffer(serId, amount);
    if (response != null) {
      if (response.resultData.isNotEmpty) {
        mechanicalOffer.value = response.resultData.toList();
        update();
      } else {
        offerEmpty.value = true;
        update();
      }
    }
  }

  Future applyOfferToService(String serId, int amount, String offerId) async {
    var response =
        await MechanicalAPI().applyMechanicalOffer(serId, amount, offerId);
    
    if (response['status'] == 'OK') {
      if (response['resultData']['applicable']) {
        offerApplicable.value = true;
        discount.value =
            double.parse(response['resultData']['discountAmount'].toString());
        discountedAmount.value =
            double.parse(response['resultData']['total'].toString());
        offerCouponId.value = offerId;
        update();
        Get.find<CouponController>().clearValue();
      } else {
        clearOffer();
      }
      
      
      
    }
  }

  clearOffer() {
    offerApplicable.value = false;
    discountedAmount.value = 0.0;
    discount.value = 0.0;
    offerCouponId.value = '';
    update();
  }
}
