import 'dart:async';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class HomeSearchBoxController extends GetxController {
  var isActive = false.obs;
  var isTimer = false.obs;
  var width = 60.0.obs;
  var searchBoxColor = Colors.transparent.obs;
  Timer timer;

  void changeWidth(double val, bool status) {
    width.value = val;
    isActive.value = status;
    update();
    if(status){
      searchBoxColor.value=Colors.black.withOpacity(0.5);
      isTimer.value=true;
      update();
      setPositionBack();
    }
    else{
      searchBoxColor.value = Colors.transparent;
      update();
      if(isTimer.value){
        timer.cancel();
      }
    }
  }

  setPositionBack(){
    timer = Timer(Duration(seconds: 10), () {
      changeWidth(60.0,false);
    });
  }
}
