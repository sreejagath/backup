import 'package:pexa_customer/ApiServices/productApi.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:get/get.dart';

class PexaSearchController extends GetxController {
  var searchResultList = [].obs;
  var isLoad = true.obs;

  Future searchProducts(String value) async {
    isLoad.value = true;
    update();
    if (searchResultList.isNotEmpty) {
      searchResultList.clear();
      update();
    }
    if (Get.find<ProductCategoryController>().productLocationList.isNotEmpty) {
      var response = await ProductAPI().searchProduct(
          Get.find<ProductCategoryController>().productLocationList, value);
      
      if (response != null) {
        searchResultList.value = response.resultData.toList();
        
        if (searchResultList.isNotEmpty) {
          isLoad.value = false;
          update();
          
        } else {
          isLoad.value = false;
          update();
          
        }
      }
    }
  }
}
