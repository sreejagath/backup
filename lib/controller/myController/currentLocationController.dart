import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';

class CurrentLocationController extends GetxController {
  var currentPosition = [].obs;
  var locationList = [].obs;
  var userChanged = false.obs;
  var userLocationString = ''.obs;
  var recentSearchList = [].obs;
  var isLoading = false.obs;

  Future getUserLocation() async {
    isLoading.value = true;
    update();
    if (userChanged.value == false) {
      var position = await GeolocatorPlatform.instance.getCurrentPosition(
        locationSettings: LocationSettings(
          accuracy: LocationAccuracy.low,
          distanceFilter: 10,
        ),
      );
      currentPosition.value = [
        position.longitude.toDouble(),
        position.latitude.toDouble()
      ].toList();
      update();
      Get.find<ProductCategoryController>().productLocationList.value = [
        position.latitude.toDouble(),
        position.longitude.toDouble()
      ].toList();
      update();
      List<Placemark> placemarks = await placemarkFromCoordinates(
          position.latitude.toDouble(), position.longitude.toDouble());
      Placemark place = placemarks[0];
      userLocationString.value = place.subLocality + ',' + place.locality;
      update();
    }
    isLoading.value = false;
    update();
  }

  Future getCoordinatesFromAddress(String value) async {
    isLoading.value = true;
    update();
    recentSearchList.add(value);
    recentSearchList.value =
        LinkedHashSet<String>.from(recentSearchList).toList();
    update();
    List<Location> location = await locationFromAddress(value);
    
    currentPosition.value = [
      location[0].longitude.toDouble(),
      location[0].latitude.toDouble()
    ].toList();
    update();
    Get.find<ProductCategoryController>().productLocationList.value = [
      location[0].latitude.toDouble(),
      location[0].longitude.toDouble()
    ].toList();
    update();
    userChanged.value = true;
    update();
    Get.find<ProductCategoryController>().userChanged.value = true;
    update();
    userLocationString.value = value;
    update();
    isLoading.value = false;
    update();
  }

  clearRecentSearch() {
    isLoading.value = true;
    update();
    recentSearchList.clear();
    update();
    isLoading.value = false;
    update();
  }

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      Get.snackbar('Warning', 'Enable the location...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(milliseconds: 2500),
          backgroundColor: Colors.red,
          colorText: Colors.white,
          snackStyle: SnackStyle.FLOATING);
      Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    } else {
      
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        Get.snackbar('Warning', 'Enable the location access for Pexa...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(milliseconds: 2500),
            backgroundColor: Colors.red,
            colorText: Colors.white,
            snackStyle: SnackStyle.FLOATING);
      }
    }

    if (permission == LocationPermission.deniedForever) {
      Get.snackbar('Warning', 'Enable the location access for Pexa...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(milliseconds: 2500),
          backgroundColor: Colors.red,
          colorText: Colors.white,
          snackStyle: SnackStyle.FLOATING);
    }

    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      getUserLocation();
      Get.find<ProductCategoryController>().setLocationForProductFetch();
    }
    return await Geolocator.getCurrentPosition();
  }
}
