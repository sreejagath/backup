import 'package:pexa_customer/ApiServices/addressApi.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddressControllerFile extends GetxController {
  var addressList = [].obs;
  var isNoAddress = false.obs;
  var defaultAddress = {}.obs;
  var fromPexaShoppe = false.obs;

  Future addAdress(Map<String, dynamic> body) async {
    var res = await AddressApi().addAddress(body);
    if (res['status'] == 'OK') {
      Get.snackbar('Success', '${res['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      if (addressList.length == 0) {
        setDefaultAddress(res['resultData']['_id'].toString())
            .then((value) => getAddress());
      } else {
        getAddress();
      }
      return true;
    } else {
      Get.snackbar('Error', '${res['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      return false;
    }
  }

  Future getAddress() async {
    if (addressList.isNotEmpty) {
      addressList.clear();
      update();
    }
    isNoAddress.value = false;
    update();
    var res = await AddressApi().getAllAdress();
    if (res.resultData.isNotEmpty) {
      addressList.value = res.resultData.toList();
      update();
    } else {
      isNoAddress.value = true;
      update();
      
    }
  }

  deleteAddress(String addressId) async {
    var res = await AddressApi().deleteAddress(addressId);
    if (res['status'] == 'OK') {
      Get.snackbar('Success', '${res['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.red,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      getAddress();
    }
  }

  Future editAddress(String addressId, Map<String, dynamic> body) async {
    var res = await AddressApi().editAddress(addressId, body);
    if (res['status'] == 'OK') {
      Get.snackbar('Success', '${res['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      getAddress();
    }
  }

  Future setDefaultAddress(String addressId) async {
    var response = await AddressApi().setDefaultAddress(addressId);
    if (response['status'] == 'OK') {
      Get.snackbar('Success', '${response['message']}...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 1),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
    }
  }

  Future getDefaultAddress() async {
    if (defaultAddress.isNotEmpty) {
      defaultAddress.clear();
      update();
    }
    var response = await AddressApi().getDefaultAddress();
    
    if (response != null) {
      defaultAddress.value = response['resultData'] as Map;
      update();
      
    }
  }
}
