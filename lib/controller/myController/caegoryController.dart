import 'package:pexa_customer/ApiServices/carSpaApi.dart';
import 'package:pexa_customer/ApiServices/productApi.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class ProductCategoryController extends GetxController {
  var categoryList = [].obs;
  var subCategoryList = [].obs;
  var productList = [].obs;
  var productListTemp = [].obs;
  var product = {}.obs;
  var prodAvailable = false.obs;
  var subCatAvailable = false.obs;
  var orderDetails = [].obs;
  var orderDetailsTemp = [].obs;
  var orderHistoryDetails = [].obs;
  var orderHistoryDetailsTemp = [].obs;
  var orderId = ''.obs;
  var buyNowShipping = 0.obs;
  var productLocationList = [].obs;
  var isFound = true.obs;
  var userChanged = false.obs;

  setLocationForProductFetch() async {
    if (userChanged.value == false) {
      try {
        var position = await GeolocatorPlatform.instance.getCurrentPosition();
        productLocationList.value = [
          position.latitude.toDouble(),
          position.longitude.toDouble()
        ].toList();
        update();
      } catch (e) {
        print(e);
      }
      
      
      
    }
  }

  Future fetchCategoryData() async {
    // if (categoryList.isNotEmpty) {
    //   categoryList.clear();
    //   update();
    // }
    var response = await ProductAPI().categoryAPI();
    if (response != null) {
      categoryList.value = response.resultData.toList();
      update();
    }
  }

  Future fetchSubCategoryData(String catId) async {
    if (subCategoryList.isNotEmpty) {
      subCategoryList.clear();
      update();
    }
    subCatAvailable.value = false;
    update();
    var response = await ProductAPI().subCategoryAPI(catId);
    if (response == null) {
      subCatAvailable.value = true;
      update();
    }
    subCategoryList.value = response.resultData.toList();
    if (subCategoryList.length == 0) {
      subCatAvailable.value = true;
      update();
    } else {
      subCatAvailable.value = false;
      update();
    }
  }

  Future fetchProductListData(String subId, List data, String page) async {
    if (productList.isNotEmpty) {
      productList.clear();
      update();
    }
    if (page == '1') {
      if (productListTemp.isNotEmpty) {
        productListTemp.clear();
        update();
      }
    }
    prodAvailable.value = false;
    update();
    var response = await ProductAPI().getProductList(subId, data, page);
    if (response == null) {
      prodAvailable.value = true;
      update();
    } else {
      productList.value = response.resultData.toList();
      update();
      productList.forEach((element) {
        productListTemp.add(element);
      });
      update();
    }
    if (productList.length == 0) {
      prodAvailable.value = true;
      update();
    } else {
      prodAvailable.value = false;
      update();
    }
  }

  Future fetchProductDetails(String prodId) async {
    if (product.isNotEmpty) {
      product.clear();
      update();
    }
    var response = await ProductAPI().getProductDetails(prodId);
    if (response != null) {
      product.value = response['resultData'] as Map;
    }
  }

  Future getOrderRunningDetailsShoppe(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (orderDetailsTemp.isNotEmpty) {
        orderDetailsTemp.clear();
        update();
      }
    }
    if (orderDetails.isNotEmpty) {
      orderDetails.clear();
      update();
    }
    var response = await ProductAPI().getRunningOrderStatus(page);
    
    if (response != null) {
      orderDetails.value = response.resultData.toList();
      update();
      if (orderDetails.isEmpty) {
        isFound.value = false;
        update();
      } else {
        orderDetails.forEach((element) {
          orderDetailsTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future getOrderHistoryDetailsShoppe(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (orderHistoryDetailsTemp.isNotEmpty) {
        orderHistoryDetailsTemp.clear();
        update();
      }
    }
    if (orderHistoryDetails.isNotEmpty) {
      orderHistoryDetails.clear();
      update();
    }
    var response = await ProductAPI().getHistoryOrderStatus(page);
    if (response != null) {
      orderHistoryDetails.value = response.resultData.toList();
      update();
      if (orderHistoryDetails.isEmpty) {
        isFound.value = false;
        update();
      } else {
        orderHistoryDetails.forEach((element) {
          orderHistoryDetailsTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future cancelOrder(String ordID) async {
    var response = await ProductAPI().cancelOrder(ordID);
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', 'Order $ordID is cancelled...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        // getOrderDetailsShoppe();
      }
    }
  }

  Future buyNowProduct(Map<String, dynamic> body) async {
    var response = await ProductAPI().buyNowProduct(body);
    if (response != null) {
      if (response['status'] == "OK") {
        orderId.value = response['resultData']['_id'];
        update();
        Get.snackbar('Success', 'Order placed...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return true;
      } else {
        Get.snackbar('Error', 'Order not placed. Error occured..!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return false;
      }
    }
  }

  Future getShippingDetails(String total) async {
    print(total);
    var response = await ProductAPI().getShippingDetails();
    if (int.parse(total) >= response.resultData[0].minimum) {
      print(response.resultData[0].minimum);
      buyNowShipping.value = 0;
      update();
    } else {
      print('on else');
      buyNowShipping.value = response.resultData[0].rate;
      update();
    }
  }

  Future cancelCarSpaOrder(String ordID) async {
    var response = await CarSpaApi().cancelCarSpaOrder(ordID);
    
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', 'Order $ordID is cancelled...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
      }
    }
  }
}
