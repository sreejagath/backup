import 'package:get/get.dart';

class PackageOfferController extends GetxController {
  var offerList = [
    {
      'title':'FREE AC VENT CLEANING',
      'description':'Free AC vent Steaming for Gold Packages',
      'price':'FREE',
      'category':'GOLD'
    },
    {
      'title':'FLAT ₹ 100 FOR PLATINUM PACKAGES',
      'description':'Flat ₹ 100 for PLATINUM Packages',
      'price':'₹100',
      'category':'PLATINUM'
    },
    {
      'title':'GET UP TO 25% OFF DIAMOND PACKAGE',
      'description':'Get Up to 25% OFF on Orders above ₹ 2899',
      'price':'25%',
      'category':'DIAMOND'
    },
    {
      'title':'GET UP TO 25% OFF',
      'description':'Get up to 25% OFF for every Service above ₹ 1000',
      'price':'25%',
      'category':'ALL'
    },
    {
      'title':'GET UP TO 30% OFF',
      'description':'Get up to 30% OFF for every Service above ₹ 1500',
      'price':'30%',
      'category':'ALL'
    },
    {
      'title':'GET UP TO 35% OFF',
      'description':'Get up to 35% OFF for every Service above ₹ 2000',
      'price':'35%',
      'category':'ALL'
    },
    {
      'title':'FLAT ₹ 100 OFF FOR ORDER ABOVE ₹ 800',
      'description':'Offer applicable for all Packages',
      'price':'₹100',
      'category':'ALL'
    },
    {
      'title':'GET UP TO 25% OFF FOR BIKE SERVICE',
      'description':'Offer applicable for Order above ₹ 1000',
      'price':'25%',
      'category':'BIKE'
    }
  ].obs;
}