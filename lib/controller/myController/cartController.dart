import 'package:pexa_customer/ApiServices/cartApi.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/productController.dart';

class CartControllerFile extends GetxController {
  var cartList = [].obs;
  var cartTotalPrice = 0.0.obs;
  var cartGrandTotal = 0.0.obs;
  var cartShipping = 0.0.obs;
  var noItem = false.obs;

  Future addOrUpdateToCart(String prodId, int count, String type) async {
    var response = await CartApi().addOrUpdateCart(prodId, count);
    
    if (response['status'] == 'OK') {
      if (type == 'add') {
        Get.snackbar('Success', 'Item added to the Cart...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(milliseconds: 700),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
      } else {
        Get.snackbar('Success', 'Item Count updated to the Cart...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(milliseconds: 700),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
      }
    }
  }

  Future getCartDetails(bool refresh) async {
    Get.find<ProductDetailsController>().isPercentageCalculate.value = true;
    update();
    if (refresh) {
      if (cartList.isNotEmpty) {
        cartList.clear();
        cartTotalPrice.value = 0.0;
        cartGrandTotal.value = 0.0;
        cartShipping.value = 0.0;
      }
    }
    noItem.value = false;
    update();
    var response = await CartApi().getCartDetails();
    if (response != null) {
      cartList.value = response.resultData.cartItems.toList();
      if (response.resultData.total != null) {
        cartTotalPrice.value =
            double.parse(response.resultData.total.toString());
      }
      if (response.resultData.grandTotal != null) {
        cartGrandTotal.value =
            double.parse(response.resultData.grandTotal.toString());
      }
      if (response.resultData.shipping != null) {
        cartShipping.value =
            double.parse(response.resultData.shipping.toString());
      }
      if (response.resultData.cartItems.isEmpty) {
        noItem.value = true;
        update();
      } else {
        noItem.value = false;
        update();
      }
    }
  }

  Future removeProdFromCart(String prodId) async {
    var res = await CartApi().removeCart(prodId);
    if (res != null) {
      Get.snackbar('Success', 'Item deleted from the Cart...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(milliseconds: 700),
          backgroundColor: Colors.red,
          colorText: Colors.white,
          snackStyle: SnackStyle.FLOATING);
    }
  }

  Future placeOrder() async {
    var response = await CartApi().placeOrder();
    
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', '${response['message']}...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return true;
      } else if (response['status'] == "Failed") {
        Get.snackbar('Error', '${response['message']}...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return false;
      }
    } else {
      Get.snackbar('Error', 'Something went wrong...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(milliseconds: 700),
          backgroundColor: Colors.red,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      return false;
    }
  }

  Future placeOrderWithCoupon(String code) async {
    var response = await CartApi().placeOrderWithCode(code);
    
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', '${response['message']}...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return true;
      } else if (response['status'] == "Failed") {
        Get.snackbar('Error', '${response['message']}...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.red,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return false;
      }
    } else {
      Get.snackbar('Error', 'Something went wrong...!',
          snackPosition: SnackPosition.TOP,
          duration: Duration(milliseconds: 700),
          backgroundColor: Colors.red,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
      return false;
    }
  }
}
