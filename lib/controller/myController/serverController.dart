import 'package:pexa_customer/ApiServices/splashApi.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/guestController.dart';
import 'package:pexa_customer/controller/myController/imageCacheController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/notificationController.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/view/screens/login/login.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ServerTest extends GetxController {
  var splash = SplashAPI();
  var userDetails = UserDetailsController();
  serverStatus() async {
    if (Get.find<AuthFactorsController>().isLoggedIn) {
      await splash.checkServer().then((value) async {
        if (splash.serverEnabled.value) {
          Get.find<ImageCacheController>().clearCache();
          await Get.find<UserDetailsController>()
              .getUserModelId()
              .then((value) {
            Get.offAllNamed('/');
            // if (userDetails.mailUpdated) {
            //   Get.offAllNamed('/');
            //print('user is logged in');
            //Get.offAll(()=> NewDashBoard(pageIndex: 0));
            // } else {
            //   print('user is not logged in');
            // Get.off(() => UserDetailsUpdate(
            //       isEdit: false,
            //     ));
            // }

            Get.find<NotificationControllerNew>().initializeNotification();
            Get.find<ProductCategoryController>().fetchCategoryData();
            Get.find<CarSpaController>().getAllCarSpaCategory();
            Get.find<MechanicalController>().getMechanicalCategory();
            Get.find<QuickHelpController>().getQuickHelpCategoryData();
            Get.find<CurrentLocationController>().userChanged.value = false;
            Get.find<CurrentLocationController>().getUserLocation();
            Get.find<ProductCategoryController>().userChanged.value = false;
            Get.find<ProductCategoryController>().setLocationForProductFetch();
          });
        } else {
          Get.snackbar('Error', 'Server issue found...!',
              snackPosition: SnackPosition.TOP,
              duration: Duration(seconds: 3),
              backgroundColor: Colors.red,
              colorText: Colors.white,
              snackStyle: SnackStyle.FLOATING);
        }
      });
    } else {
      if (Get.find<AuthFactorsController>().phoneNumber.value.toString() !=
              'null' &&
          Get.find<AuthFactorsController>().phoneNumber.value.toString() !=
              '') {
        Get.find<GuestController>()
            .guestLogout(Get.find<AuthFactorsController>().phoneNumber.value);
      }
      // Get.find<ImageCacheController>().clearCache();
      Get.to(() => LoginPage());
    }
  }
}
