import 'package:pexa_customer/ApiServices/carModelApi.dart';
import 'package:pexa_customer/ApiServices/userDeatailsApi.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserDetailsController extends GetxController {
  var isMIdAvailable = false.obs;
  var carModel = ''.obs;
  bool isUpdate = true.obs();
  var userEmail = ''.obs;
  var userName = ''.obs;
  var mailUpdated = false;
  final loginController = Get.find<AuthFactorsController>();

  void onInit() {
    super.onInit();
    getUserModelId();
  }

  Future getUserModelId() async {
    mailUpdated = false;
    update();
    var response = await UserDetailsApi().fetchUserDetails();
    if (response != null) {
      if (response.resultData.email != null) {
        userEmail.value = response.resultData.email.toString();
        print('userEmail.value is ${userEmail.value}');
        mailUpdated = true;
        update();
      }
      if (response.resultData.name != null) {
        userName.value = response.resultData.name.toString();
        print('userEmail.value is ${userName.value}');
        mailUpdated = true;
        update();
      }
      if (response.resultData.modelId == null) {
        isMIdAvailable.value = false;
        update();
      } else {
        isMIdAvailable.value = true;
        if (response.resultData.modelId.toString() != 'null' ||
            response.resultData.modelId.toString() != null) {
          carModel.value = response.resultData.modelId.toString();
          update();
          var res = await ApiData()
              .fetchCarModelDetail(response.resultData.modelId.toString());

          if (res != null) {
            if (res['status'] == 'OK') {
              ApiData()
                  .fetchCarBrandDetail(res['resultData']['make_id'].toString());
            }
          }
        }
      }
    } else {
      isMIdAvailable.value = false;
      update();
    }

    // return mailUpdated;
  }

  Future updateUserModelId(String modelId, String carType) async {
    final carSpaController = Get.find<CarSpaController>();
    var response = await UserDetailsApi().addModelIdToProfile(modelId, carType);
    if (response != null) {
      carModel.value = modelId;
      update();
      carSpaController.getAllCarSpaCategory();
      Get.find<MechanicalController>().getMechanicalCategory();
      Get.snackbar('Success', 'Your Car Model Updated',
          snackPosition: SnackPosition.TOP,
          duration: Duration(seconds: 2),
          backgroundColor: Colors.yellow,
          colorText: Colors.black,
          snackStyle: SnackStyle.FLOATING);
    }
  }

  Future updateUserDetails(String email, String name) async {
    print('email: $email');
    print('name: $name');

    var response = await UserDetailsApi().updateDetailsUser(email, name);
    if (response != null) {
      if (response['status'] == 'OK') {
        Get.snackbar('Success', '${response['message']}..!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
        return true;
      } else if (response['status'] == 'Failed') {
        return false;
        // Get.snackbar('Error', '${response['message']}..!',
        //     snackPosition: SnackPosition.TOP,
        //     duration: Duration(seconds: 2),
        //     backgroundColor: Colors.red,
        //     colorText: Colors.white,
        //     snackStyle: SnackStyle.FLOATING);

      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
