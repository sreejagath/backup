import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:pexa_customer/ApiServices/notificationAPI.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class NotificationControllerNew extends GetxController {
  var token = ''.obs;
  var deviceId = ''.obs;
  var device = ''.obs;
  var deviceType = ''.obs;

  Future initializeNotification() async {
    token.value= await FirebaseMessaging.instance.getToken();
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try{
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        device.value = build.model;
        deviceId.value = build.androidId;
        deviceType.value='Android';
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        device.value = data.name;
        deviceId.value = data.identifierForVendor;
        deviceType.value='Ios';
      }
    }
    on PlatformException {
      
    }
    update();
    Map<String, dynamic> data = {
      "token": token.value,
      "deviceId": deviceId.value,
      "device": device.value,
      "deviceType": deviceType.value,
      "userType": "customer",
    };
    
    var response = await NotificationAPI().initializeNotification(data);
    
    
    
  }
}
