import 'package:pexa_customer/ApiServices/cartApi.dart';
import 'package:get/get.dart';

class ProductDetailsController extends GetxController {
  var outOfStock = false.obs;
  var isInCart = false.obs;
  var cartList = [].obs;
  var pricePercentage = [].obs;
  var isPercentageCalculate = false.obs;
  var oneTouch = true.obs;

  checkStockStatus(int stock) {
    if (stock <= 0) {
      outOfStock.value = true;
      update();
    } else {
      outOfStock.value = false;
      update();
    }
  }

  Future checkProductInCart(String id) async {
    if (cartList.isNotEmpty) {
      cartList.clear();
    }
    var response = await CartApi().getCartDetails();
    cartList.value = response.resultData.cartItems.toList();
    if (cartList.isNotEmpty) {
      cartList.forEach((element) {
        if (element.product.id.toString().contains(id)) {
          
          isInCart.value = true;
          update();
          return;
        } else {
          
          isInCart.value = false;
          update();
        }
      });
    } else {
      
      isInCart.value = false;
      update();
    }
  }

  Future calculatePricePercentage(int price, int offerPrice) async {
    if(isPercentageCalculate.value){
      pricePercentage.clear();
      update();
    }
    pricePercentage.add(((price - offerPrice) * 100 / price).round().toString());
    isPercentageCalculate.value=false;
    update();
    
  }
  deletePercentage(int index){
    pricePercentage.removeAt(index);
    update();
  }
}
