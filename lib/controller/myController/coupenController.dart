import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/ApiServices/coupenCheckApi.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';

class CouponController extends GetxController {
  var isApplied = false.obs;
  var showDetails = false.obs;
  var couponName = ''.obs;
  var couponData = {}.obs;
  var discount = 0.0.obs;
  var finalAmount = 0.0.obs;
  var minAmount = 0.0.obs;
  var controller = TextEditingController().obs;

  Future checkCoupon(double amount) async {
    if (couponData.isNotEmpty) {
      couponData.clear();
      update();
    }

    if (controller.value.text != null || controller.value.text != '') {
      var response = await CouponApi()
          .checkCoupon(amount, controller.value.text.toString());

      if (response != null) {
        if (response['resultData'] != null) {
          couponData.value = response['resultData'] as Map;
          update();
          minAmount.value =
              double.parse(couponData['coupon']['minOrder'].toString());
          couponName.value = couponData['coupon']['couponCode'];
          discount.value =
              double.parse(couponData['discountAmount'].toString());
          finalAmount.value =
              double.parse(couponData['discountedAmount'].toString());
          update();
          if (!couponData['applicable']) {
            showDetails.value = true;
            isApplied.value = false;
            update();
          } else {
            showDetails.value = false;
            isApplied.value = true;
            controller.value.text = '';
            update();
            Get.find<ProductCategoryController>()
                .getShippingDetails(finalAmount.value.toInt().toString());
            Get.find<CarSpaController>().clearOffer();
            Get.find<MechanicalController>().clearOffer();
          }
        } else {
          isApplied.value = false;
          showDetails.value = false;
          controller.value.text = '';
          update();
          Get.snackbar('Error', 'Invalid Coupon.!',
              snackPosition: SnackPosition.TOP,
              duration: Duration(seconds: 1),
              backgroundColor: Colors.red,
              colorText: Colors.white,
              snackStyle: SnackStyle.FLOATING);
        }
      }
    }
  }

  clearValue() {
    showDetails.value = false;
    isApplied.value = false;
    controller.value.text = '';
    update();
  }
}
