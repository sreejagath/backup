import 'package:pexa_customer/ApiServices/qucikHelpAPi.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class QuickHelpController extends GetxController {
  var quickHelpCategoryData = [].obs;
  var quickHelpServiceList = [].obs;
  var quickHelpServiceProperty = [].obs;
  var quickHelpRunningOrders = [].obs;
  var quickHelpHistoryOrders = [].obs;
  var quickHelpRunningOrdersTemp = [].obs;
  var quickHelpHistoryOrdersTemp = [].obs;
  var isFound = true.obs;

  Future getQuickHelpCategoryData() async {
    var response = await QuickHelpAPI().getQucikHelpCategory();
    if (response.status == 'OK') {
      quickHelpCategoryData.value = response.resultData.toList();
      
    }
  }

  Future getQuickHelpServiceWithCatId(String catId) async {
    if (quickHelpServiceList.isNotEmpty) {
      quickHelpServiceList.clear();
      update();
    }
    var response = await QuickHelpAPI().getQuickHelpService(catId);
    if (response.status == 'OK') {
      quickHelpServiceList.value = response.resultData.toList();
      setList();
    }
  }

  setList() {
    if (quickHelpServiceProperty.isNotEmpty) {
      quickHelpServiceProperty.clear();
      update();
    }
    quickHelpServiceList.forEach((element) {
      int num = ','.allMatches(element.list).length;
      List data = [];
      for (int i = 0; i <= num; i++) {
        data.add(element.list.split(',')[i]);
      }
      quickHelpServiceProperty.add(data);
    });
  }

  Future quickHelpBuyNow(String id, List data) async {
    var response = await QuickHelpAPI().quickHelpBuyNow(id, data);
    if (response != null) {
      
      
      if (response['status'] == "OK") {
        return true;
      } else if (response['status'] == "Failed") {
        return false;
      }
    } else {
      return false;
    }
  }

  Future getQuickHelpRunningOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (quickHelpRunningOrdersTemp.isNotEmpty) {
        quickHelpRunningOrdersTemp.clear();
        update();
      }
    }
    if (quickHelpRunningOrders.isNotEmpty) {
      quickHelpRunningOrders.clear();
      update();
    }
    var response = await QuickHelpAPI().getQuickHelpOrderRunningStatus(page);
    if (response != null) {
      quickHelpRunningOrders.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status == 'Active') || (element.status == 'Accepted')))
          .toList();
      update();
      if (quickHelpRunningOrders.isEmpty) {
        isFound.value = false;
        update();
      } else {
        quickHelpRunningOrders.forEach((element) {
          quickHelpRunningOrdersTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future getQuickHelpHistoryOrders(String page) async {
    isFound.value = true;
    update();
    if (page == '1') {
      if (quickHelpHistoryOrdersTemp.isNotEmpty) {
        quickHelpHistoryOrdersTemp.clear();
        update();
      }
    }
    if (quickHelpHistoryOrders.isNotEmpty) {
      quickHelpHistoryOrders.clear();
      update();
    }
    var response = await QuickHelpAPI().getQuickHelpOrderHistoryStatus(page);
    if (response != null) {
      quickHelpHistoryOrders.value = response.resultData
          .toList()
          .where((element) =>
              ((element.status != 'Active') && (element.status != 'Accepted')))
          .toList();
      update();
      if (quickHelpHistoryOrders.isEmpty) {
        isFound.value = false;
        update();
      } else {
        quickHelpHistoryOrders.forEach((element) {
          quickHelpHistoryOrdersTemp.add(element);
          update();
        });
      }
    } else {
      Future.delayed(Duration(seconds: 2), () {
        isFound.value = false;
        update();
      });
    }
  }

  Future cancelQuickHelpOrder(String ordID) async {
    var response = await QuickHelpAPI().cancelQuickHelpOrder(ordID);
    
    if (response != null) {
      if (response['status'] == "OK") {
        Get.snackbar('Success', 'Order $ordID is cancelled...!',
            snackPosition: SnackPosition.TOP,
            duration: Duration(seconds: 2),
            backgroundColor: Colors.yellow,
            colorText: Colors.black,
            snackStyle: SnackStyle.FLOATING);
      }
    }
  }
}
