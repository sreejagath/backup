import 'package:pexa_customer/ApiServices/guestLogin.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';

class GuestController extends GetxController {
  Future guestLogin() async {
    var response = await GuestLoginApi().guestLogin();
    
    if (response['status'] == "OK") {
      
      
      
      await setValue(
              response['resultData']['userToken'],
              response['resultData']['_id'],
              response['resultData']['phone'],
              false)
          .then((value) => Get.find<AuthFactorsController>()
              .savePhone(response['resultData']['phone'], false)
              .then((value) => {
                    Get.offAllNamed('/'),
                    Get.find<ProductCategoryController>().fetchCategoryData(),
                    Get.find<CarSpaController>().getAllCarSpaCategory(),
                    Get.find<MechanicalController>().getMechanicalCategory(),
                    Get.find<QuickHelpController>().getQuickHelpCategoryData(),
                  }));
    }
  }

  Future setValue(String token, String id, String phone, bool isUser) async {
    Get.find<AuthFactorsController>().token.value = token;
    Get.find<AuthFactorsController>().userId.value = id;
    Get.find<AuthFactorsController>().phoneNumber.value = phone;
    Get.find<AuthFactorsController>().isLoggedIn = isUser;
  }

  Future guestLogout(String phoneNumber) async {
    var response = await GuestLoginApi().guestLogout(phoneNumber);
    
    if (response['status'] == "OK") {
      Get.find<AuthFactorsController>()
          .deleteGuestData()
          .then((value) => Get.find<AuthFactorsController>().deleteUserPhone());
    }
  }
}
