import 'package:flutter/material.dart';

ThemeData light = ThemeData(
  fontFamily: 'Poppins',
  primaryColor: Colors.black,
  secondaryHeaderColor: Color(0xFF1ED7AA),
  disabledColor: Color(0xFFBABFC4),
  backgroundColor: Color(0xFFF3F3F3),
  errorColor: Color(0xFFE84D4F),
  brightness: Brightness.light,
  hintColor: Color(0xFF9F9F9F),
  cardColor: Colors.white,
  colorScheme: ColorScheme.light(
    primary: Color(0xFFf9c43c), 
    secondary: Color(0xFFf9c43c),
    ),
  textButtonTheme: TextButtonThemeData(style: TextButton.styleFrom(primary: Color(0xFFffec4e))),
);


class ConstColors{
  static const Color primary = Color(0xFFf9c43c);
  static const amberColor = Color(0xFFffec4e);
}