class Images {
  //static const String placeholder = 'assets/image/placeholder.jpg';
  static const String about_us = 'assets/image/about_us.png';
  static const String log_out = 'assets/image/log_out.png';
  static const String policy = 'assets/image/policy.png';
  static const String support = 'assets/image/support.png';
  static const String terms = 'assets/image/terms.png';
  static const String guest = 'assets/image/guest.png';
  static const String profile_bg = 'assets/image/profile_bg.png';
  static const String spinner = 'assets/carSpa/loading.gif';
  static const String loading = 'assets/carSpa/placeholder.jpeg';
  static const String user = 'assets/image/user.png';
}
