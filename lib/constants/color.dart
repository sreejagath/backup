import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

Color botAppBarColor = HexColor('#ffd800');
Color botAppBarColor2 = HexColor('#f2cc33');
Color blackPrimary = HexColor('#2d2d2d');

MaterialColor primaryColor1 = MaterialColor(0xfffeec4f, const <int, Color>{
  50: const Color(0xfffeec4f),
  100: const Color(0xfffeec4f),
  200: const Color(0xfffeec4f),
  300: const Color(0xfffeec4f),
  400: const Color(0xfffeec4f),
  500: const Color(0xfffeec4f),
  600: const Color(0xfffeec4f),
  700: const Color(0xfffeec4f),
  800: const Color(0xfffeec4f),
  900: const Color(0xfffeec4f),
});
Color white = Colors.white;
Color black45 = Colors.black87;
