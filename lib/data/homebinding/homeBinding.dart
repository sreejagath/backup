import 'package:get/get.dart';
import 'package:pexa_customer/controller/myController/addressController.dart';
import 'package:pexa_customer/controller/myController/appBarColorController.dart';
import 'package:pexa_customer/controller/myController/caegoryController.dart';
import 'package:pexa_customer/controller/myController/carModelController.dart';
import 'package:pexa_customer/controller/myController/carSpaController.dart';
import 'package:pexa_customer/controller/myController/carSpaTimeSlotController.dart';
import 'package:pexa_customer/controller/myController/cartController.dart';
import 'package:pexa_customer/controller/myController/connectivityController.dart';
import 'package:pexa_customer/controller/myController/coupenController.dart';
import 'package:pexa_customer/controller/myController/currentLocationController.dart';
import 'package:pexa_customer/controller/myController/guestController.dart';
import 'package:pexa_customer/controller/myController/homeSearchBoxController.dart';
import 'package:pexa_customer/controller/myController/imageCacheController.dart';
import 'package:pexa_customer/controller/myController/loginController.dart';
import 'package:pexa_customer/controller/myController/mechanicalController.dart';
import 'package:pexa_customer/controller/myController/mechanicalTimeSLotController.dart';
import 'package:pexa_customer/controller/myController/newSearchController.dart';
import 'package:pexa_customer/controller/myController/notificationController.dart';
import 'package:pexa_customer/controller/myController/orderScreenController.dart';
import 'package:pexa_customer/controller/myController/packageOfferController.dart';
import 'package:pexa_customer/controller/myController/productController.dart';
import 'package:pexa_customer/controller/myController/quickHelpController.dart';
import 'package:pexa_customer/controller/myController/serverController.dart';
import 'package:pexa_customer/controller/myController/serviceCheckoutController.dart';
import 'package:pexa_customer/controller/myController/userDetailsController.dart';
import 'package:pexa_customer/controller/tab_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AuthFactorsController());
    Get.put(UserDetailsController());
    Get.put(CarModelController());
    Get.put(ServerTest());
    Get.put(HomeAppBarColorController());
    Get.put(CarSpaController());
    Get.put(ProductCategoryController());
    Get.put(CartControllerFile());
    Get.put(AddressControllerFile());
    Get.put(CarSpaTimeSlotController());
    Get.put(HomeSearchBoxController());
    Get.put(ServiceCheckOutController());
    Get.put(OrderScreenController());
    Get.put(ConnectivityController());
    Get.put(GuestController());
    Get.put(ProductDetailsController());
    Get.put(MechanicalController());
    Get.put(MechanicalTimeSlotController());
    Get.put(QuickHelpController());
    Get.put(NotificationControllerNew());
    Get.put(PackageOfferController());
    Get.put(CurrentLocationController());
    Get.put(PexaSearchController());
    Get.put(ImageCacheController());
    Get.put(CouponController());
    Get.put(TabControllerMethod());
  }
}
